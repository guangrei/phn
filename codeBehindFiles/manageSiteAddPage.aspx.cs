﻿using System;
using System.Configuration;

public partial class manage_manageSiteAddPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();

        if (f.LoginCheck() == true)
        {
            string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
            int id = Functions.ReturnNumeric(Request["subdomainid"]);

            string subdomain = f.GetSubdomainNameById(id);
             subdomainholder.Value = subdomain;
             //get full sitenamedomain
             //fullSiteNameLabel.Text = f.GetFullSubdomainNameById(id);
           

        }
        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;


        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true)
        {

            string _pageTitle = pageTitle.Text;
            //_pageTitle = f.ReplaceLineBreakForTitles(_pageTitle);
            _pageTitle = Server.HtmlEncode(_pageTitle);
            _pageTitle = f.SqlProtect(_pageTitle);
            _pageTitle = Functions.LimitString(_pageTitle, 100);

            string _pageDescription = pageDescription.Text;
            //_pageDescription = f.ReplaceLineBreakForTitles(_pageDescription);
            _pageDescription = Server.HtmlEncode(_pageDescription);
            _pageDescription = f.SqlProtect(_pageDescription);
            _pageDescription = Functions.LimitString(_pageDescription, 100);

            string _pageKeywords = pageKeywords.Text;
            //_pageKeywords = f.ReplaceLineBreakForTitles(_pageKeywords);
            _pageKeywords = Server.HtmlEncode(_pageKeywords);
            _pageKeywords = f.SqlProtect(_pageKeywords);
            _pageKeywords = Functions.LimitString(_pageKeywords, 100);

            string _pageOtherMetaTags = pageOtherMetaTags.Text;
            //_pageOtherMetaTags = f.ReplaceLineBreakForTitles(_pageOtherMetaTags);
            _pageOtherMetaTags = Server.HtmlEncode(_pageOtherMetaTags);
            _pageOtherMetaTags = f.SqlProtect(_pageOtherMetaTags);
            _pageOtherMetaTags = Functions.LimitString(_pageOtherMetaTags, 500);

            string _fontSize = Functions.LimitString(fontSize.SelectedValue.ToString(), 50);
            //_fontSize = f.ReplaceLineBreakForTitles(_fontSize);
            _fontSize = Server.HtmlEncode(_fontSize);
            _fontSize = f.SqlProtect(_fontSize);

            string _pageBgColor = Functions.LimitString(pageBgColor.SelectedValue.ToString(), 50);
            //_pageBgColor = f.ReplaceLineBreakForTitles(_pageBgColor);
            _pageBgColor = Server.HtmlEncode(_pageBgColor);
            _pageBgColor = f.SqlProtect(_pageBgColor);

            string _pageFontColor = Functions.LimitString(pageFontColor.SelectedValue.ToString(), 50);
            //_pageFontColor = f.ReplaceLineBreakForTitles(_pageFontColor);
            _pageFontColor = Server.HtmlEncode(_pageFontColor);
            _pageFontColor = f.SqlProtect(_pageFontColor);

            string _pageFolder = pageFolder.Text.ToString();
            _pageFolder = f.ReplaceLineBreakForTitles(_pageFolder);
            _pageFolder = Functions.ReturnAlphaNumeric(_pageFolder);
            _pageFolder = Functions.LimitString(_pageFolder, 50);
            if (String.IsNullOrEmpty(_pageFolder)==true)
            {
                _pageFolder="Root";
            }

            string _subdomain = Functions.LimitString(subdomainholder.Value,50);


            if (Functions.IsAlphaNumericForSubdomain(_subdomain) == true
                && String.IsNullOrEmpty(_pageTitle)==false
                && String.IsNullOrEmpty(_pageKeywords) == false
                && String.IsNullOrEmpty(_pageDescription) == false)
            {

                if (f.AddSitePage(Functions.un, _subdomain, _pageTitle, _pageDescription, _pageKeywords, _fontSize, _pageBgColor, _pageFontColor, _pageFolder, _pageOtherMetaTags) == true)
                {
                result.Visible = true;
                result.Text = "New page has been created. You now need to edit it. Click below link to go to site management then navigate down to Pages section";
                loggedinDiv.Visible = false;
                okLink.Visible = true;
                }
                else{
                loggedinDiv.Visible = false;
                result.Visible = true;
                result.CssClass = "error";
                result.Text = "An error occured. Can't add page. Page limit per subdomain is " + SiteConfiguration.GetConfig().pageLimitPerSubdomain; 
                
                }

            }
            else
            {
                result.Visible = true;
                result.CssClass = "error";
                result.Text = "All fields are required!";
                
            }

        }
        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;
        }


            
    }
    protected void BackgorundDataSource_setStyle(object sender, EventArgs e)
    {
        for (int i = 0; i < pageBgColor.Items.Count; i++)
        {
            pageBgColor.Items[i].Attributes.Add("style", "background-color: " + pageBgColor.Items[i].Value);
        }
    }
    protected void BackgorundDataSource_setStyleFont(object sender, EventArgs e)
    {
        for (int i = 0; i < pageFontColor.Items.Count; i++)
        {
            pageFontColor.Items[i].Attributes.Add("style", "background-color: " + pageFontColor.Items[i].Value);
        }
    }
    protected void TemplateDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string selectedTheme = TemplateDropDownList.SelectedValue.ToString();


        if (selectedTheme=="Standart")
        {
            pageBgColor.SelectedIndex = pageBgColor.Items.IndexOf(pageBgColor.Items.FindByValue("#FFFFFF"));
            pageFontColor.SelectedIndex = pageFontColor.Items.IndexOf(pageFontColor.Items.FindByValue("#000000"));
        }
        if (selectedTheme == "Black")
        {
            pageBgColor.SelectedIndex = pageBgColor.Items.IndexOf(pageBgColor.Items.FindByValue("#000000"));
            pageFontColor.SelectedIndex = pageFontColor.Items.IndexOf(pageFontColor.Items.FindByValue("#FFFFFF"));
        }
        if (selectedTheme == "Blue")
        {
            pageBgColor.SelectedIndex = pageBgColor.Items.IndexOf(pageBgColor.Items.FindByValue("#0000FF"));
            pageFontColor.SelectedIndex = pageFontColor.Items.IndexOf(pageFontColor.Items.FindByValue("#FFFFFF"));
        }
        if (selectedTheme == "Green")
        {
            pageBgColor.SelectedIndex = pageBgColor.Items.IndexOf(pageBgColor.Items.FindByValue("#008000"));
            pageFontColor.SelectedIndex = pageFontColor.Items.IndexOf(pageFontColor.Items.FindByValue("#FFFFFF"));
        }
        if (selectedTheme == "Gray")
        {
            pageBgColor.SelectedIndex = pageBgColor.Items.IndexOf(pageBgColor.Items.FindByValue("#808080"));
            pageFontColor.SelectedIndex = pageFontColor.Items.IndexOf(pageFontColor.Items.FindByValue("#000000"));
        }
        if (selectedTheme == "Red")
        {
            pageBgColor.SelectedIndex = pageBgColor.Items.IndexOf(pageBgColor.Items.FindByValue("#FF0000"));
            pageFontColor.SelectedIndex = pageFontColor.Items.IndexOf(pageFontColor.Items.FindByValue("#FFFFFF"));
        }
        if (selectedTheme == "Modern")
        {
            pageBgColor.SelectedIndex = pageBgColor.Items.IndexOf(pageBgColor.Items.FindByValue("#DCDCDC"));
            pageFontColor.SelectedIndex = pageFontColor.Items.IndexOf(pageFontColor.Items.FindByValue("#000000"));
        }
    }

    protected void copyButton_Click(object sender, EventArgs e)
    {
        Functions f = new Functions();
        int pageToCopy = Functions.ReturnNumeric(copyPageDropDownList.SelectedValue.ToString());
        string sub = Functions.LimitString(subdomainholder.Value.ToString(), 20);
        sub = Server.HtmlEncode(sub);
        sub = f.SqlProtect(sub);

        if (f.CopySitePage(Functions.un, sub, pageToCopy) == true)
        {
            result.Visible = true;
            result.Text = "New page has been copied. You now need to edit it. Click below link to go to site management then navigate down to Pages section";
            loggedinDiv.Visible = false;
            okLink.Visible = true;
        }
        else
        {
            loggedinDiv.Visible = false;
            result.Visible = true;
            result.CssClass = "error";
            result.Text = "An error occured. Can't copy the page. Page limit per subdomain is " + SiteConfiguration.GetConfig().pageLimitPerSubdomain; 
        }

    }
}
