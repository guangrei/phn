﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI;

public partial class manage_checkIllegalGuestbookPosts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true && Session["IsGlobalAdmin"].ToString() == "yes")
        {

            if (Request.QueryString["cmd"] == "delete")
            {
                f.DeleteAGbPostOwner(Functions.ReturnNumeric(Request.QueryString["postId"]));
                ListOfSitesGrid.Visible = false;
                keyword.Visible = false;
                search.Visible = false;
                resultpanel.Visible = true;
            }
        }
        else
        {
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;
        }

    }
}
