﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class manage_managePoll : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
         Functions f = new Functions();

         if (f.LoginCheck() == true)
         {
             int id = Functions.ReturnNumeric(Request["subdomainid"]);
             string subd = f.GetSubdomainNameById(id);
             subdomainName.Value = subd;
             //get full sitenamedomain
             fullSiteNameLabel.Text = f.GetFullSubdomainNameById(id);

             if (Request["job"] == "delete")
             {
                 ListHolder.Visible = false;
                 deletePoll();
             }
             if (Request["job"] == "reset")
             {
                 ListHolder.Visible = false;
                 resetPoll();
             }

         }
         else
         {
             //login wrong
             loggedinDiv.Visible = false;
             notLoggedInDiv.Visible = true;
         }

    }
    protected void deletePoll()
    {
        Functions f = new Functions();
        resultPanel.Visible = true;

        int id = Functions.ReturnNumeric(Request["subdomainid"]);
        string subdomain = f.GetSubdomainNameById(id);
        int pollTodelete = Functions.ReturnNumeric(Request["id"]);
        if (f.DeletePoll(Functions.un, subdomain, pollTodelete) == true)
        {
            ResultLiteral.Text = "Poll has been deleted<br/>";
        }
        else
        {
            
            ResultLiteral.Text = "Unable to delete the poll</br>";
        }

    }
    protected void resetPoll()
    {
        Functions f = new Functions();
        resultPanel.Visible = true;

        int id = Functions.ReturnNumeric(Request["subdomainid"]);
        string subdomain = f.GetSubdomainNameById(id);
        int pollToReset = Functions.ReturnNumeric(Request["id"]);
        if (f.ResetPoll(Functions.un, subdomain, pollToReset) == true)
        {
            ResultLiteral.Text = "Poll has been reset<br/>";
        }
        else
        {

            ResultLiteral.Text = "Unable to reset the poll</br>";
        }

    }
    protected void addChoiceButton_Click(object sender, EventArgs e)
    {
        addChoiceButton.Visible = false;
        ChoicecountDropDownList.Visible = false;
        ChoiceC.Visible = false;
        HyperLinkReload.Visible = true;
        HyperLinkReload.NavigateUrl = Request.FilePath +"?"+ Request.QueryString;

        int numtextboxes = System.Convert.ToInt32(ChoicecountDropDownList.SelectedItem.Value);
        for (int i = 1; i <= numtextboxes; i++)
        {
            TextBox myTextbox = new TextBox();
            myTextbox.ID = "Choice-" + i.ToString();

            Label myTextboxLabel = new Label();
            myTextboxLabel.Text = "Choice " + i.ToString();
            myTextboxLabel.ID = "Label" + i.ToString();
            ChoicesPlaceHolder.Controls.Add(myTextboxLabel);
            ChoicesPlaceHolder.Controls.Add(new LiteralControl(": "));
            ChoicesPlaceHolder.Controls.Add(myTextbox);
            ChoicesPlaceHolder.Controls.Add(new LiteralControl("<br />"));
        }
    }
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        Functions f = new Functions();
        ListHolder.Visible = false;
        resultPanel.Visible = true;
        string[] ctrls = Request.Form.ToString().Split('&');
        string ctrlPrefix = "Choice";
        int cnt = FindOccurence(ctrlPrefix);
        int maxChoicesInPoll = 9; //9 means 10 Choices
        int queryCount = 0;

        int id = Functions.ReturnNumeric(Request["subdomainid"]);
        string subd = f.GetSubdomainNameById(id);

        string polltitle = PollQuestionText.Text;
        polltitle = Server.HtmlEncode(polltitle);
        polltitle = f.ReplaceLineBreakForTitles(polltitle);
        polltitle = Functions.LimitString(polltitle, 250);
        polltitle = f.SqlProtect(polltitle);
        if (String.IsNullOrEmpty(polltitle))
            polltitle = "New poll";

        int NewPollId = f.addPoll(Functions.un, subd, polltitle, Convert.ToInt32(SiteConfiguration.GetConfig().pollLimitPerSite));


        //Response.Write(cnt);


        if (cnt > 1 && NewPollId > 0)
        {
            ResultLiteral.Text = "New poll created.<br/>";

            for (int k = 1; k <= cnt; k++)
            {

                for (int i = 0; i < ctrls.Length; i++)
                {


                    if (ctrls[i].Contains(ctrlPrefix + "-" + k.ToString()))
                    {

                        string ctrlName = ctrls[i].Split('=')[0];

                        string ctrlValue = ctrls[i].Split('=')[1];

                        ctrlValue = Server.UrlDecode(ctrlValue);
                        ctrlValue = Server.HtmlEncode(ctrlValue.ToString());
                        ctrlValue = f.ReplaceLineBreakForTitles(ctrlValue);
                        ctrlValue = Functions.LimitString(ctrlValue, 250);
                        ctrlValue = f.SqlProtect(ctrlValue);


                        //Decode the Value 
                        if (String.IsNullOrEmpty(ctrlValue) == false && queryCount <= maxChoicesInPoll)
                        {
                            //Response.Write(Server.UrlDecode(ctrlValue));
                            //Response.Write("<br/>");
                            f.addPollChoice(Functions.un, subd, ctrlValue, NewPollId);
                            //ResultLiteral.Text += "Choice " + ctrlValue + " has been added<br/>";
                        }

                        break;
                        queryCount++;
                        //Response.Write(queryCount);
                    }

                }

            }

        }
        else
        {
            resultPanel.CssClass = "error";
            ResultLiteral.Text = "Unable to add poll! Did you try to add empty poll or more polls than allowed?<br/>";
        }




    }
    private int FindOccurence(string substr)
    {

        string reqstr = Request.Form.ToString();

        return ((reqstr.Length - reqstr.Replace(substr, "").Length)

                / substr.Length);

    } 




}
