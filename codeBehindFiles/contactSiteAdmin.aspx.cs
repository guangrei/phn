﻿using System;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Data.SqlClient;


public partial class contactSiteAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int timelimitFroEmailsInMinutes = 15;
        string subdomain = Request["subdomain"];
        string domain = Request["domain"];
        string returnUrl = Request["returnUrl"];
        string fromEmail = Request["fromEmail"];
        string message = Request["message"];
        string captcha = Functions.ReturnAlphaNumeric(Request["captcha"]).ToLower();
        string generatedCaptcha;
        // somehow cache fails to hold captcah text so at those times i don' do check 
        if (Cache["CaptchaImageText"] != null)
        {
            generatedCaptcha = Cache["CaptchaImageText"].ToString().ToLower();
        }
        else
        {
            generatedCaptcha = captcha;
        }
        //-------------------------------------------------------------------------------


        if (String.IsNullOrEmpty(subdomain) == true
            || String.IsNullOrEmpty(domain) == true
                || String.IsNullOrEmpty(returnUrl) == true
                    || String.IsNullOrEmpty(fromEmail) == true
                        || String.IsNullOrEmpty(message) == true
                            || message.Length < 10
                                || String.IsNullOrEmpty(captcha) == true
                                    || Functions.IsValidEmail(fromEmail) == false
                                        || captcha != generatedCaptcha)
        {
            // all fields are required;
            Response.Redirect("http://phn.me/contactSiteAdminError.aspx?err=fields&s=" + subdomain + "&d=" + domain);
        }
        else
        {
            //send email

            string CurrentDomain = SiteConfiguration.GetConfig().defaultSiteDomain;
            string toEmail = "none";

            //get email---------------------------------------------------------------------------------------------------------
             using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("getUserEmailAndLastEmailTime", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                        cmd.Parameters.Add("@limittimeinminutes", SqlDbType.Int).Value = timelimitFroEmailsInMinutes;
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                toEmail = reader["email"].ToString();
                            }
                        }
                        //toEmail = (string)cmd.ExecuteScalar().ToString();
                    }
                }
            //----------------------------------------------------------------------------------------------------------------------


            if (toEmail == "none" || Functions.IsValidEmail(toEmail) == false || String.IsNullOrEmpty(toEmail)==true)
            {
                Response.Redirect("http://phn.me/contactSiteAdminError.aspx?err=time&s=" + subdomain + "&d=" + domain);
            }
            else
            {

                //send email

                // try
                //{
                StringBuilder mailBody = new System.Text.StringBuilder();
                mailBody.Append("Hello from  " + CurrentDomain + ".");
                mailBody.Append(Environment.NewLine);
                mailBody.Append("You have received below email from one of your visitors.");
                mailBody.Append(Environment.NewLine);
                mailBody.Append("---------------");
                mailBody.Append(Environment.NewLine);
                mailBody.Append(message);
                mailBody.Append(Environment.NewLine);
                mailBody.Append("---------------");
                mailBody.Append(Environment.NewLine);
                mailBody.Append("Visitor's email: " + fromEmail);
                mailBody.Append(Environment.NewLine);
                mailBody.Append("Visitor's ip: " + Request.ServerVariables["REMOTE_ADDR"]);
                mailBody.Append(Environment.NewLine);
                mailBody.Append("Visitor's phone: " + Request.ServerVariables["HTTP_USER_AGENT"]);
                mailBody.Append(Environment.NewLine);
                mailBody.Append("---------------");
                mailBody.Append(Environment.NewLine);
                mailBody.Append("http://" + CurrentDomain);

                MailMessage oMsg = new MailMessage();
                oMsg.From = new MailAddress(SiteConfiguration.GetConfig().emailServerDefaultFromMail);
                oMsg.To.Add(new MailAddress(toEmail));
                oMsg.ReplyTo = new MailAddress(fromEmail);
                oMsg.Subject = "Email from " + fromEmail;
                oMsg.IsBodyHtml = false;
                oMsg.Body = mailBody.ToString();
                SmtpClient SmtpMail = new SmtpClient(SiteConfiguration.GetConfig().emailServer);
                SmtpMail.Port = Convert.ToInt32(SiteConfiguration.GetConfig().emailServerPort);
                SmtpMail.Send(oMsg);
                oMsg = null;
                Response.Redirect("http://" + subdomain + "." + domain + "/" + returnUrl);
                // }
                // catch (Exception ee)
                //{
                //  HttpContext.Current.Response.Output.Write("{0} Exception caught.", ee);
                //Response.Write("to email: " +toEmail);
                //Response.Redirect("http://phn.me/contactSiteAdminError.aspx?err=smtp&s=" + subdomain + "&d=" + domain);
                //}
            }
        }
    }
}
