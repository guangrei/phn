﻿using System;

public partial class manage_manageSiteDeleteSite : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true)
        {

            string subd = Request["subdomain"];
            if (Functions.IsAlphaNumericForSubdomain(subd) == false)
            {
                changeResult.Text = "Wrong subdomain id! DO NOT play with querystring!";
            }
            else
            {

                int setmestatus = f.DeleteSite(Functions.un, subd);
                if (setmestatus == 1)
                {
                    changeResult.Text = "Site has been deleted";
                }

                else if (setmestatus == 0)
                {
                    changeResult.Text = "Can't delete the site. Are you sure you are doing the right thing?";
                }

            }

        }
        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;
        }

    }
}
