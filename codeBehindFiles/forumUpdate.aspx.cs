﻿using System;
using System.Web;

public partial class forumUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        string msg;
        if (f.LoginCheck() == true)
        {
            string _isSticky = Functions.LimitString(HttpContext.Current.Request["stick"], 10);
            bool isSticky = false;
            if (_isSticky == "s") { isSticky = true; }

            string topic = HttpContext.Current.Request["topic"];
            topic = HttpContext.Current.Server.HtmlEncode(topic);
            topic = f.ReplaceLineBreakForTitles(topic);
            topic = Functions.LimitString(topic, 100);
            topic = f.SqlProtect(topic);

            string replyBody=HttpContext.Current.Request["replyBody"];
            replyBody = HttpContext.Current.Server.HtmlEncode(replyBody);
            replyBody = f.ReplaceLineBreak(replyBody);
            replyBody = Functions.LimitString(replyBody, 1000);
            replyBody = f.SqlProtect(replyBody);

            if (String.IsNullOrEmpty(topic)==false && String.IsNullOrEmpty(replyBody)==false ) {
                    if (f.AddForumTopic(Functions.un, topic,replyBody, isSticky)==true) {
                        msg = "Your topic has been added";
                    }
                    else{
                        updateResult.CssClass = "error";
                        msg = "Something is not right. Did you try to do double post?";
                    }
            }
            else{
                updateResult.CssClass = "error";
                msg="All fields are required!";
            }

        }
        else
        {
            updateResult.CssClass = "error";
            msg = "You need to be logged in in order to use this page!";
        }

        updateResult.Text = msg;
    }
}
