﻿using System;

public partial class manage_manageSiteStyleSheet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();

        if (f.LoginCheck() == true)
        {


            int id = Functions.ReturnNumeric(Request["subdomainid"]);

            string subd = f.GetSubdomainNameById(id);
            subdomainName.Value = subd;
            //get full sitenamedomain
            fullSiteNameLabel.Text = f.GetFullSubdomainNameById(id);


            if (!IsPostBack)
            {
                styleSheetBox.Text = f.GetCurrentStyle(subd);
            }
           
            

          

        }
            else
            {
                //login wrong
                loggedinDiv.Visible = false;
                notLoggedInDiv.Visible = true;
            }

    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
         Functions f = new Functions();

         string style = Functions.LimitString(Server.HtmlEncode(f.cssUpdateSanitize(styleSheetBox.Text)), 3000);
         string subdomain = subdomainName.Value ;
     

         if (Functions.IsAlphaNumericForSubdomain(subdomain) == true && String.IsNullOrEmpty(style) == false)
            {
                //do db

                if (f.UpdateStyleSheet(Functions.un, subdomain, style) == true)
                {
                    styleSheetBox.Text = styleSheetBox.Text;
                }
                else
                {
                    styleSheetBox.Text = "Error";
                }
            }
            
    }
}
