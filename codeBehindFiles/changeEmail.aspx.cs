﻿using System;
using System.Web;

public partial class manage_changeEmail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
   
        string msg;

        if (f.LoginCheck() == true)
        {
            string newemail = Functions.LimitString(HttpContext.Current.Request["newemail"], 100);
            if (Functions.IsValidEmail(newemail) == true)
            {

                if (f.ChangeEmail(Functions.un, newemail) == true)
                {
                    string verificationCode = f.GetUniqueKey(5);
                    if (f.sendVerificationMail(newemail, verificationCode, Functions.un, Functions.pw)) // sendmail
                        f.deValidateAccount(Functions.un, verificationCode);
                    msg = "Your e-mail has been updated. Please check your inbox at " + newemail + " for your new verification code";
                    
                }
                else
                {
                    changeResult.CssClass = "error";
                    msg = "Something went wrong. We are unable to update your e-mail!";
                    
                }

            }
            else
            {
                changeResult.CssClass = "error";
                msg = "New e-mail is not a valid e-mail address!";
                
            }

        }
        else
        {
            changeResult.CssClass = "error";
            msg = "You need to be logged in in order to use this page!";
           
        }

        changeResult.Text = msg;
        
    }
}
