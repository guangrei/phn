﻿using System;

public partial class manage_manageSiteAdverts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true)
        {
            int id = Functions.ReturnNumeric(Request["subdomainid"]);
            string subdomain = f.GetSubdomainNameById(id);
            fullSiteNameLabel.Text = f.GetFullSubdomainNameById(id);
            subdomainName.Value = f.GetSubdomainNameById(id);

            if (Request["job"]=="delete")
            {
                existingAdsPanel.Visible = false;
                deleteAd();


            }
        }

        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;
        }






    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Functions f = new Functions();
        existingAdsPanel.Visible = false;
        int id = Functions.ReturnNumeric(Request["subdomainid"]);
        string subdomain = f.GetSubdomainNameById(id);
        string advertToAdd = advertCompany.SelectedValue;
        string advertPublisherId=Functions.LimitString(publisherId.Text,50);
        advertPublisherId=Functions.ReturnAlphaNumeric(advertPublisherId);

        if (advertToAdd == "Admob")
        {
            Admob admob = new Admob();
            string testAdResult = admob.ShowAd(true, advertPublisherId);
            addAdResultPanel.Visible = true;
            if (testAdResult.ToLower().IndexOf("test") != -1)
            {

                if (f.addAdvert(Functions.un,subdomain, advertToAdd, advertPublisherId,Convert.ToInt32(SiteConfiguration.GetConfig().advertiserLimit)))
                {
                addAdResultLabel.Text = "Advertiser has been added";
                }
                else{
                    addAdResultLabel.Text = "Cannot add advertiser. Try deleting unused advertisers";
                }
            }
            else
            {
                addAdResultLabel.CssClass = "error";
                addAdResultLabel.Text = "Please check your publisher id. It doesnt seem to be correct";
            }
        }

        if (advertToAdd == "AdmodaText" || advertToAdd == "AdultmodaText" || advertToAdd == "AdmodaBanner" || advertToAdd == "AdultmodaBanner" || advertToAdd == "BuzzcityBanner" || advertToAdd == "BuzzcityText" || advertToAdd == "ZestADZ" ||  advertToAdd == "Mkhoj")
        {
            addAdResultPanel.Visible = true;

                if (f.addAdvert(Functions.un, subdomain, advertToAdd, advertPublisherId, Convert.ToInt32(SiteConfiguration.GetConfig().advertiserLimit)))
                {
                  addAdResultLabel.Text = "Advertiser has been added. Please not that we are unable to test ads for this publisher. So you must make sure you are using correct publisher id!";
                }
               else
                {
                   addAdResultLabel.Text = "Cannot add advertiser. Try deleting unused advertisers";
               }          
        }

      if (advertToAdd == "Decktrade")
      {
          Decktrade decktrade = new Decktrade();
          string testAdResult = decktrade.ShowAd(true, advertPublisherId);
          
          addAdResultPanel.Visible = true;
          //addAdResultLabel.Text = testAdResult;

          if (testAdResult.ToLower().Length>0)
          {

              if (f.addAdvert(Functions.un, subdomain, advertToAdd, advertPublisherId, Convert.ToInt32(SiteConfiguration.GetConfig().advertiserLimit)))
              {
                  addAdResultLabel.Text = "Advertiser has been added";
              }
              else
             {
                 addAdResultLabel.CssClass = "error";
                  addAdResultLabel.Text = "Cannot add advertiser. Try deleting unused advertisers";
              }
          }
          else
          {
             addAdResultLabel.CssClass = "error";
             addAdResultLabel.Text = "Please check your publisher id. It doesnt seem to be correct";
          }
      }


      if (advertToAdd == "Google")
      {

          AdSense adsense = new AdSense();
          string channelIdtoSave = Functions.ReturnAlphaNumeric(channelId.Text);
          string testAdResult = adsense.ShowAd(advertPublisherId, channelIdtoSave);

          addAdResultPanel.Visible = true;
          //addAdResultLabel.Text = testAdResult;

          if (testAdResult.ToLower().IndexOf("google_afm") != -1)
          {

              if (f.addAdvert(Functions.un, subdomain, advertToAdd, advertPublisherId, channelIdtoSave, Convert.ToInt32(SiteConfiguration.GetConfig().advertiserLimit)))
              {
                  addAdResultLabel.Text = "Advertiser has been added. Although we have received response from ad server we are unable to verify it. So you must make sure you are using correct publisher id and channel id!";
              }
              else
              {
                  addAdResultLabel.Text = "Cannot add advertiser. Try deleting unused advertisers";
              }
          }
          else
          {
              addAdResultLabel.CssClass = "error";
              addAdResultLabel.Text = "Please check your publisher id. It doesnt seem to be correct";
          }
      }

      if (advertToAdd == "Mojiva")
      {

          Mojiva mojiva = new Mojiva();
          string zoneIdtoSave = Functions.ReturnAlphaNumeric(zoneId.Text);
          string testAdResult = mojiva.ShowAd(true, advertPublisherId, zoneIdtoSave);

          addAdResultPanel.Visible = true;
          //addAdResultLabel.Text = testAdResult;

          if (testAdResult.ToLower().IndexOf("test") != -1)
          {

              if (f.addAdvert(Functions.un, subdomain, advertToAdd, advertPublisherId, zoneIdtoSave, Convert.ToInt32(SiteConfiguration.GetConfig().advertiserLimit)))
              {
                addAdResultLabel.Text = "Advertiser has been added";
             }
             else
             {
                 addAdResultLabel.Text = "Cannot add advertiser. Try deleting unused advertisers";
              }
          }
          else
          {
             addAdResultLabel.CssClass = "error";
            addAdResultLabel.Text = "Please check your publisher id. It doesnt seem to be correct";
          }
      }

    }


    protected void deleteAd()
    {
        Functions f = new Functions();
        addAdResultPanel.Visible = true;
        int id = Functions.ReturnNumeric(Request["subdomainid"]);
        string subdomain = f.GetSubdomainNameById(id);
        int advertToDelete = Functions.ReturnNumeric(Request["id"]);
        if (f.DeleteAdvert(Functions.un, subdomain, advertToDelete) == true)
        {
            addAdResultLabel.Text = "Advertiser has been deleted";
        }
        else
        {
            addAdResultLabel.CssClass = "error";
            addAdResultLabel.Text = "Unable to delete advertiser";
        }

    }
}
