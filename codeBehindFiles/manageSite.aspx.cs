﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class manage_manageSite : System.Web.UI.Page
{
  

    protected void subDomainInfoListEventHandler(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem dataItem = (ListViewDataItem)e.Item;
        DataRowView drv = (DataRowView)dataItem.DataItem;

        Label isR = (Label)e.Item.FindControl("isR");
        if ((bool)drv["isR"] == true)
        {
            isR.Text = "<img src=\"../images/adult.png\" alt=\"*\" /> Adult";
        }
        else
        {
            isR.Text = "Non adult";
        }


        Label isActiveLabel = (Label)e.Item.FindControl("isActiveLabel");
        if ((bool)drv["isActive"] == true)
        {
            isActiveLabel.Text = "<img src=\"../images/flag_green.gif\" alt=\"*\" /> Online";
        }
        else
        {
            isActiveLabel.Text = "<img src=\"../images/flag_red.gif\" alt=\"*\" /> Offline";
        }


    }


    

    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();

        if (f.LoginCheck() == true)
        {
            
            BindData();

        }
        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;


        }
    }

   

    protected void BindData()
    {


        Functions f = new Functions();
        string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
        int id = Functions.ReturnNumeric(Request["subdomainid"]);
        string subdomain = f.GetSubdomainNameById(id);

        if (String.IsNullOrEmpty(subdomain) == false)
        {
            
            //subdomainName.Value = subdomain;
            //get full sitenamedomain
            string fullsubname = f.GetFullSubdomainNameById(id);
            fullSiteNameLabel.Text = fullsubname + " <a href=\"http://" + fullsubname + "\"><img src=\"../images/url.gif\" alt=\"go\" /></a>";
            //start page list
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();
            SqlCommand cmd = new SqlCommand("listSubdomainPages", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
            cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
            cmd.Parameters.Add("@folder", SqlDbType.VarChar).Value = string.Empty;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            DataSet ds = new DataSet();
            da.Fill(ds);

            subDomainInfoList.DataSource = ds.Tables[0];
            subDomainInfoList.DataBind();
            //pageList.DataSource = ds.Tables[1];
            //pageList.DataBind();


            //paging setup
            //int currentPage = (pageListPaging.StartRowIndex / pageListPaging.PageSize) + 1;
            //int totalPages = pageListPaging.TotalRowCount / pageListPaging.PageSize;

        }
        else
        {
            fullSiteNameLabel.Text = "Error!";
        }

    }

   
}
