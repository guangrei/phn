﻿using System;
using System.Web;

public partial class manage_changePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        string changedpw;
        string msg;
               
        
        if (f.LoginCheck() == true)
        {
          string newpw = Functions.LimitString(HttpContext.Current.Request["newpw"],50);
            if (Functions.IsAlphaNumeric(newpw)==true)
            {

            if (f.ChangePassword(Functions.un, newpw) == true)
            { msg = "Your password has been changed to: "+ newpw;
              changedpw = newpw;
            }
            else
            {
                changeResult.CssClass = "error"; 
                msg = "Something went wrong. We are unable to change your password!";
            changedpw = Functions.pw;
            }

            }
            else{
                changeResult.CssClass = "error";
                msg = "New password must be alpha-numeric (a-z-0-9)!";
                    changedpw = Functions.pw;
            }

        }
        else {
            changeResult.CssClass = "error";
            msg="You need to be logged in in order to use this page!";
            changedpw = Functions.pw;
        }

        changeResult.Text = msg + "<br/><a href=\"login.aspx?un=" + Functions.un + "&amp;pw=" + changedpw + "\">Ok</a>";
        
    }
}
