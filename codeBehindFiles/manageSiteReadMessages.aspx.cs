﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class manage_manageSiteReadMessages : System.Web.UI.Page
{

    protected void AnnouncementListEventHandler(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem dataItem = (ListViewDataItem)e.Item;
        DataRowView drv = (DataRowView)dataItem.DataItem;
        Label isPublic = (Label)e.Item.FindControl("isPublic");
        Label msgDate = (Label)e.Item.FindControl("msgDate");
        Panel control = (Panel)e.Item.FindControl("IsGlobalAdminLabel");

        msgDate.Text = String.Format("<b>{0:ddd, dd MMM yy}</b> ", drv["msgDate"]);

        if ((bool)drv["isPublic"] == false)
        {
            isPublic.Text = " <a onclick=\"return confirm('Delete this message?')\" href=\"manageSiteDeleteMessage.aspx?un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;id=" + drv["id"] + "\"><img src=\"../images/delete.png\" alt=\"-\"/></a> - <b>PRIVATE</b><br/>";
            if (Session["IsGlobalAdmin"].ToString() == "yes")
            {
                control.Visible = true;
            }
        }
        else
        {
            isPublic.Text = " - <b>PUBLIC</b><br/> ";

            if (Session["IsGlobalAdmin"].ToString() == "yes")
            {
                control.Visible = true;
            }
        }




    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();

        if (f.LoginCheck() == true)
        {

            BindData();

            if (Request.QueryString["fromcheck"] == "1")
            {
                tounPanel.Visible = true;
                isPublicFalse.Selected = true;
                _toun.Text = Request.QueryString["toun"];
                _announce.Text = "Warning regarding your site! Page" + Request.QueryString["pageid"] + ".aspx in your site contains illegal content! Please remove it ASAP or your site will be deleted in 24 hrs!";
            }
            if (Request.QueryString["fromcheck"] == "2")
            {
                tounPanel.Visible = true;
                isPublicFalse.Selected = true;
                _toun.Text = Request.QueryString["toun"];
                _announce.Text = "Warning regarding your site! Post " + Request.QueryString["postId"] + " by " + Request.QueryString["poster"] + " in your guestbook contains illegal content! Please remove it ASAP or your site will be deleted in 24 hrs!";
            }
       
        }
        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;
            


        }
    }

    protected void BindData()
    {

        string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
        int id = Functions.ReturnNumeric(Request["id"]);

        //start list
        SqlConnection conn = new SqlConnection(connString);
        conn.Open();
        SqlCommand cmd = new SqlCommand("getAnnouncements", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;

        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;

        DataSet ds = new DataSet();
        da.Fill(ds);

        AnnouncementList.DataSource = ds;
        AnnouncementList.DataBind();
    }
    protected void AnnouncementList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
        this.AnnouncementListPaging.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);

        BindData();
    }
    protected void post_Click(object sender, EventArgs e)
    {
        Functions f = new Functions();
        string msg;
        if (f.LoginCheck() == true && Session["IsGlobalAdmin"].ToString() == "yes")
        {
 
            string toun = Functions.LimitString(_toun.Text, 50);
            if (String.IsNullOrEmpty(toun))
            {
                toun = string.Empty;
            }
            string announce = Functions.LimitString(_announce.Text, 1000);
            announce = f.ReplaceLineBreak(announce);
            string _isPublic = Functions.LimitString(__isPublic.SelectedValue, 10);
            bool isPublic = false;
            if (_isPublic == "Yes") { isPublic = true; }
            //if (Functions.IsAlphaNumeric(announce) == true)
           //{

            if (f.AddAnnounce(toun, announce, isPublic) == true)
                {
                    addResult.Visible = true;
                    AnnouncementList.Visible = false;
                    AnnouncementListPaging.Visible = false;
                    addResult.CssClass = "center";   
                    msg = "Message has been added!";
                    addPanel1.Visible = false;
                    Cache.Remove("anounces");

                }
                else
                {
                    addResult.Visible = true;
                    addResult.CssClass = "error";
                    AnnouncementList.Visible = false;
                    AnnouncementListPaging.Visible = false;
                    msg = "Can't add message!";
                }


           //}
           //else
           //{
             //  AnnouncementList.Visible = false;
               //AnnouncementListPaging.Visible = false;
           //addResult.Visible = true;
           //addResult.CssClass = "error";
           //msg = "All fields are required!";
           //}

        }
        else
        {
            addResult.Visible = true;
            addResult.CssClass = "error";
            AnnouncementList.Visible = false;
            AnnouncementListPaging.Visible = false;
            msg = "You need to be logged in in order to use this page!";
        }

        addResultTwo.Text = msg;
    }




    protected void radioChanged(object sender, EventArgs e)
    {
       
        if (__isPublic.SelectedItem.Text=="No")
        {
            tounPanel.Visible = true;
        }
        else
        {
            tounPanel.Visible = false;
        }
    }
}
