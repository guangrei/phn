﻿using System;

public partial class manage_addSiteForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == false)
        {
            notLoggedInDiv.Visible = true;
            loggedinDiv.Visible = false;
        }

    }
}
