﻿using System;
using System.Web;

public partial class manage_manageSiteSetIndexPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        Functions f = new Functions();
        string msg;
        if (f.LoginCheck() == true)
        {
            int id = Functions.ReturnNumeric(Request["id"]);

            
                string subdomain = Functions.LimitString(HttpContext.Current.Request["subdomain"], 50);
                if (String.IsNullOrEmpty(subdomain) == false)
                {
                    f.setIndexPage(Functions.un, subdomain, id);
                    msg = "Index page has been changed!";
                }
                else
                {
                    changeResult.CssClass = "error";
                    msg = "Subdomain is required. Do not play with query string!";
                }

               
            

        }
        else
        {
            changeResult.CssClass = "error";
            msg = "You need to be logged in in order to use this page!";
        }

        changeResult.Text = msg;
    }

}
