﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class manage_manageSiteDeleteInactiveSites : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        string msg;
        if (f.LoginCheck() == true && Session["IsGlobalAdmin"].ToString() == "yes")
        {
            string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connString;
                conn.Open();

                using (SqlCommand cmd = new SqlCommand("DeleteInactiveSubdomains 30", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 0;
                    cmd.ExecuteScalar();


                    msg = "Cleanup of inactive sites done! If you want to change inactive day limit edit this page.";

                }
            }

        }
        else
        {

            addResult.CssClass = "error";
            msg = "You need to be logged in in order to use this page!";
        }

        addResultTwo.Text = msg;
    }
}
