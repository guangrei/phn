﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using Ionic.Utils.Zip;

public partial class manage_mangeSiteBackupDbFileStoreDb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        string msg;
        if (f.LoginCheck() == true && Session["IsGlobalAdmin"].ToString() == "yes")
        {
            string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
            string BackupDbLocation = Server.MapPath("~/App_Data/PHN_fileStore.bak");
            string fnameForZip = Server.MapPath("~/App_Data/PHN_fileStore.bak.zip");

            FileInfo fi = new FileInfo(fnameForZip);
            if (fi.Exists)
            {
                fi.Delete();
                //Response.Write("DELETEDd");
            }

            FileInfo fii = new FileInfo(BackupDbLocation);
            if (fii.Exists)
            {
                fii.Delete();
                //Response.Write("DELETED");
            }


            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connString;
                conn.Open();

                using (SqlCommand cmd = new SqlCommand("BACKUP DATABASE PHN_fileStore TO DISK = '" + BackupDbLocation + "'", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 0;
                    cmd.ExecuteNonQuery();



                    ZipFile zip = new ZipFile(fnameForZip);
                    zip.AddFile(BackupDbLocation, null);
                    zip.TempFileFolder = Server.MapPath("~/App_Data/");
                    zip.Save(fnameForZip);
                    zip.Dispose();

                    msg = "Backup done!";

                }
            }

        }
        else
        {

            addResult.CssClass = "error";
            msg = "You need to be logged in in order to use this page!";
        }

        addResultTwo.Text = msg;
    }
}
