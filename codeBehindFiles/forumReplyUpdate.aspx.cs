﻿using System;
using System.Web;

public partial class forumUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        string msg;
        if (f.LoginCheck() == true)
        {
            int id = Functions.ReturnNumeric(Request["id"]);

            string replyBody = HttpContext.Current.Request["replyBody"];
            replyBody = Server.HtmlEncode(replyBody);
            replyBody = f.ReplaceLineBreak(replyBody);
            replyBody = Functions.LimitString(replyBody, 1000);
            replyBody = f.SqlProtect(replyBody);

            if (String.IsNullOrEmpty(replyBody)==false ) {
                    if (f.AddReplyToForumTopic(Functions.un, id , replyBody)==true) {
                        msg = "Your reply has been added";
                    }
                    else{
                        updateResult.CssClass = "error";
                        msg = "Something is not right. Did you try to do double post?";
                    }
            }
            else{
                updateResult.CssClass = "error";
                msg="All fields are required!";
            }

        }
        else
        {
            updateResult.CssClass = "error";
            msg = "You need to be logged in in order to use this page!";
        }

        updateResult.Text = msg;
    }
}
