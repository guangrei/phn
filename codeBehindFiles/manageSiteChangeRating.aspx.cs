﻿using System;

public partial class manage_manageSiteChangeRating : System.Web.UI.Page
{
            protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true)
        {
            int id = Functions.ReturnNumeric(Request["subdomainid"]);
            string subd = f.GetSubdomainNameById(id);
            string dom = f.GetDomainNameById(id);

            if (String.IsNullOrEmpty(subd) == true)
            {
                changeResult.Text = "Wrong subdomain id! DO NOT play with querystring!";
            }
            else
            {
                // check for adult keywords
                //Response.Write(dom);
                //Response.Write(f.isAdultKeyword(dom.ToLower()));
                if (f.isAdultKeyword(subd.ToLower()) == true || f.isAdultKeyword(dom.ToLower()) == true)
                {
                    changeResult.Text = "Cannot change site rating. Your subdomain or domain name contains adult keywords!";
                }
                else
                {
                    //change rating
                    int setmestatus = f.ChangeSiteRating(Functions.un, subd);

                    if (setmestatus == 1)
                    {
                        changeResult.Text = "Site rating changed to <b>Adult</b>";
                    }
                    else if (setmestatus == 0)
                    {
                        changeResult.Text = "Site rating changed to <b>Non adult</b>.";
                    }

                    else if (setmestatus == 2)
                    {
                        changeResult.Text = "Can't apply changes. Are you sure you are doing the right thing?";
                    }
                }

            }

        }
            else
            {
                //login wrong
                loggedinDiv.Visible = false;
                notLoggedInDiv.Visible = true;
            }

    }


   
        
        
    }

