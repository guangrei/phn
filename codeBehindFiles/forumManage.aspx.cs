﻿using System;

public partial class forumManage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string deleteWhat = Request.QueryString["del"];
       
        int id = Functions.ReturnNumeric(Request["id"]);

        Functions f = new Functions();

        if (f.LoginCheck() == true && Session["IsGlobalAdmin"].ToString() == "yes")
        {

            if (deleteWhat == "topic")
            {
                f.DeleteTopic(id,1);
            }
            else if (deleteWhat=="reply")
            {
                f.DeleteTopic(id, 2);
            }
            deleteResult.Text = "Done!";
        }
        else
        {
            deleteResult.Text = "You don't have access to this resource!";
        }

    }
}
