﻿using System;
using System.Web;

public partial class manage_manageSiteDeletePage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       Functions f = new Functions();
        string msg;
        if (f.LoginCheck() == true)
        {
            int id = Functions.ReturnNumeric(Request["id"]);
            
                string subdomain = Functions.LimitString(HttpContext.Current.Request["subdomain"], 50);

                if (String.IsNullOrEmpty(subdomain) == false)
                {
                    if (f.DeleteAPage(Functions.un, subdomain, id) == true)
                    {
                        msg = "Page has been deleted!";
                    }
                    else
                    {
                        deleteResult.CssClass = "error";
                        msg = "Can't delete the page. Did you try to delete index page? First set some other page as index page and then delete the page.";
                    }
                }
                else
                {
                    deleteResult.CssClass = "error";
                    msg = "Subdomain is required. Do not play with query string!";
                }


            

        }
        else
        {
            deleteResult.CssClass = "error";
            msg = "You need to be logged in in order to use this page!";
        }

        deleteResult.Text = msg;
    }
   
}
