﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" Title="Untitled Page" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<div class="title">Generic error handler</div>
<ul>
<li>There was an error! Please check</li>
<li>If your email was correct</li>
<li>If you have filled all fields</li>
<li>Your message was longer than 10 letters</li>
<li>Waited at least 15 minutes after sending email</li>
</ul>
<div class="center">
<a href="http://<%=Request["s"] %>.<%=Request["d"] %>">Ok</a>
</div>
</asp:Content>

