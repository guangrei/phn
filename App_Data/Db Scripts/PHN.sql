/****** Object:  UserDefinedFunction [dbo].[SplitME]    Script Date: 07/06/2009 10:10:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitME]
   (  @Delimiter varchar(5), 
      @List      varchar(8000)
   ) 
   RETURNS @TableOfValues table 
      (  RowID   smallint IDENTITY(1,1), 
         [Value] varchar(50) 
      ) 
AS 
   BEGIN
    
      DECLARE @LenString int 
 
      WHILE len( @List ) > 0 
         BEGIN 
         
            SELECT @LenString = 
               (CASE charindex( @Delimiter, @List ) 
                   WHEN 0 THEN len( @List ) 
                   ELSE ( charindex( @Delimiter, @List ) -1 )
                END
               ) 
                                
            INSERT INTO @TableOfValues 
               SELECT substring( @List, 1, @LenString )
                
            SELECT @List = 
               (CASE ( len( @List ) - @LenString ) 
                   WHEN 0 THEN '' 
                   ELSE right( @List, len( @List ) - @LenString - 1 ) 
                END
               ) 
         END
          
      RETURN 
      
   END
GO
/****** Object:  Table [dbo].[sessions]    Script Date: 07/06/2009 10:10:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sessions](
	[sessionid] [varchar](50) NOT NULL,
	[un] [varchar](50) NOT NULL,
	[updatedon] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rss_providers]    Script Date: 07/06/2009 10:10:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rss_providers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[url] [varchar](255) NOT NULL,
	[subdomain] [varchar](50) NOT NULL,
 CONSTRAINT [PK_rss_providers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[online_visitors]    Script Date: 07/06/2009 10:10:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[online_visitors](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[subdomain] [varchar](50) NOT NULL,
	[domain] [varchar](50) NOT NULL,
	[visitorSessionId] [varchar](50) NOT NULL,
	[visitorIp] [varchar](20) NOT NULL,
	[visitorBrowser] [varchar](255) NOT NULL,
	[lastSeen] [datetime] NOT NULL,
	[location] [varchar](30) NOT NULL,
 CONSTRAINT [PK_online_visitors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[messagesToUsers]    Script Date: 07/06/2009 10:10:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[messagesToUsers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[anounce] [varchar](1000) NOT NULL,
	[toun] [varchar](50) NULL,
	[isPublic] [bit] NULL,
	[msgDate] [datetime] NOT NULL,
 CONSTRAINT [PK_messagesToUsers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users]    Script Date: 07/06/2009 10:10:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[un] [varchar](50) NOT NULL,
	[pw] [varchar](50) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[lastLogin] [datetime] NULL,
	[ip] [varchar](15) NULL,
	[isVerified] [bit] NOT NULL,
	[verificationCode] [nchar](10) NOT NULL,
	[isGlobalAdmin] [bit] NOT NULL,
	[regDate] [datetime] NOT NULL,
	[warningCount] [int] NULL,
	[lastEmailReceived] [datetime] NULL,
	[paidUser] [bit] NOT NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[un] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[UploadFile]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UploadFile] 

	@un varchar(50),
	@fileName nvarchar(50),
	@fileSize int,
	@fileType varchar(50),
	@fileData varbinary(max)

AS
BEGIN

if exists (select [fileName] from [PHN_fileStore]..[fileStore] where [fileName]=@fileName and un=@un)
begin
return 0
end
else
begin
INSERT INTO [PHN_fileStore]..[fileStore]
           ([un]
           ,[fileName]
           ,[fileSize]
           ,[fileType]
           ,[fileData]
           ,[uploadTime])
     VALUES
           (@un
           ,@fileName
           ,@fileSize
           ,@fileType
           ,@fileData
           ,GETUTCDATE())
           return 1
  end
 END
GO
/****** Object:  StoredProcedure [dbo].[ReturnFileMimeType]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ReturnFileMimeType] 


	@id int
AS
BEGIN
	   
	   
select [fileType]  from [PHN_fileStore]..[fileStore] where id=@id;

END
GO
/****** Object:  StoredProcedure [dbo].[ListUploadedfiles]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListUploadedfiles] 

	@un varchar(50)

AS
BEGIN

Select * from [PHN_fileStore]..[fileStore] where [un]=@un order by [fileName] asc
END
GO
/****** Object:  StoredProcedure [dbo].[GetFile]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetFile] 



	@id int
AS
BEGIN
	   
	   
select [fileData]  from [PHN_fileStore]..[fileStore] where  id=@id;

END
GO
/****** Object:  StoredProcedure [dbo].[GetCurrentTotalUploads]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCurrentTotalUploads] 

	@un varchar(50)

AS
BEGIN
select ISNULL(SUM(fileSize),0) as totalFileSize from [PHN_fileStore]..[fileStore] where un=@un
END
GO
/****** Object:  Table [dbo].[forum_topics]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[forum_topics](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[un] [varchar](50) NOT NULL,
	[topic] [nvarchar](100) NOT NULL,
	[postDate] [datetime] NOT NULL,
	[updateDate] [datetime] NOT NULL,
	[isSticky] [bit] NOT NULL,
 CONSTRAINT [PK_forum_topics] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[DELETE-ReturnFileName-DELETE]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DELETE-ReturnFileName-DELETE] 



	@id int
AS
BEGIN
	   
	   
select [fileName]  from [PHN_fileStore]..[fileStore] where  id=@id;

END
GO
/****** Object:  Table [dbo].[chat]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chat](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[pageid] [int] NOT NULL,
	[un] [nvarchar](50) NOT NULL,
	[m] [nvarchar](500) NULL,
	[mTime] [datetime] NULL,
	[unIp] [varchar](15) NULL,
	[unBrowser] [nvarchar](250) NULL,
 CONSTRAINT [PK_chat] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[ADMIN_SearchAndReplace_Whole_DB]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ADMIN_SearchAndReplace_Whole_DB]
(
	@SearchStr nvarchar(100),
	@ReplaceStr nvarchar(100)
)
AS
BEGIN
	-- Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.
	-- Purpose: To search all columns of all tables for a given search string and replace it with another string
	-- Written by: Narayana Vyas Kondreddi
	-- Site: http://vyaskn.tripod.com
	-- Tested on: SQL Server 7.0 and SQL Server 2000
	-- Date modified: 2nd November 2002 13:50 GMT

	SET NOCOUNT ON

	DECLARE @TableName nvarchar(256), @ColumnName nvarchar(128), @SearchStr2 nvarchar(110), @SQL nvarchar(4000), @RCTR int
	SET  @TableName = ''
	SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%','''')
	SET @RCTR = 0

	WHILE @TableName IS NOT NULL
	BEGIN
		SET @ColumnName = ''
		SET @TableName = 
		(
			SELECT MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))
			FROM 	INFORMATION_SCHEMA.TABLES
			WHERE 		TABLE_TYPE = 'BASE TABLE'
				AND	QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) > @TableName
				AND	OBJECTPROPERTY(
						OBJECT_ID(
							QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)
							 ), 'IsMSShipped'
						       ) = 0
		)

		WHILE (@TableName IS NOT NULL) AND (@ColumnName IS NOT NULL)
		BEGIN
			SET @ColumnName =
			(
				SELECT MIN(QUOTENAME(COLUMN_NAME))
				FROM 	INFORMATION_SCHEMA.COLUMNS
				WHERE 		TABLE_SCHEMA	= PARSENAME(@TableName, 2)
					AND	TABLE_NAME	= PARSENAME(@TableName, 1)
					AND	DATA_TYPE IN ('char', 'varchar', 'nchar', 'nvarchar')
					AND	QUOTENAME(COLUMN_NAME) > @ColumnName
			)
	
			IF @ColumnName IS NOT NULL
			BEGIN
				SET @SQL=	'UPDATE ' + @TableName + 
						' SET ' + @ColumnName 
						+ ' =  REPLACE(' + @ColumnName + ', ' 
						+ QUOTENAME(@SearchStr, '''') + ', ' + QUOTENAME(@ReplaceStr, '''') + 
						') WHERE ' + @ColumnName + ' LIKE ' + @SearchStr2
				EXEC (@SQL)
				SET @RCTR = @RCTR + @@ROWCOUNT
			END
		END	
	END

	SELECT 'Replaced ' + CAST(@RCTR AS varchar) + ' occurence(s)' AS 'Outcome'
END
GO
/****** Object:  StoredProcedure [dbo].[ADMIN_Search_Whole_DB]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ADMIN_Search_Whole_DB]
(
	@SearchStr nvarchar(100)
)
AS
BEGIN
	-- Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.
	-- Purpose: To search all columns of all tables for a given search string
	-- Written by: Narayana Vyas Kondreddi
	-- Site: http://vyaskn.tripod.com
	-- Tested on: SQL Server 7.0 and SQL Server 2000
	-- Date modified: 28th July 2002 22:50 GMT


	CREATE TABLE #Results (ColumnName nvarchar(370), ColumnValue nvarchar(3630))

	SET NOCOUNT ON

	DECLARE @TableName nvarchar(256), @ColumnName nvarchar(128), @SearchStr2 nvarchar(110)
	SET  @TableName = ''
	SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%','''')

	WHILE @TableName IS NOT NULL
	BEGIN
		SET @ColumnName = ''
		SET @TableName = 
		(
			SELECT MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))
			FROM 	INFORMATION_SCHEMA.TABLES
			WHERE 		TABLE_TYPE = 'BASE TABLE'
				AND	QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) > @TableName
				AND	OBJECTPROPERTY(
						OBJECT_ID(
							QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)
							 ), 'IsMSShipped'
						       ) = 0
		)

		WHILE (@TableName IS NOT NULL) AND (@ColumnName IS NOT NULL)
		BEGIN
			SET @ColumnName =
			(
				SELECT MIN(QUOTENAME(COLUMN_NAME))
				FROM 	INFORMATION_SCHEMA.COLUMNS
				WHERE 		TABLE_SCHEMA	= PARSENAME(@TableName, 2)
					AND	TABLE_NAME	= PARSENAME(@TableName, 1)
					AND	DATA_TYPE IN ('char', 'varchar', 'nchar', 'nvarchar')
					AND	QUOTENAME(COLUMN_NAME) > @ColumnName
			)
	
			IF @ColumnName IS NOT NULL
			BEGIN
				INSERT INTO #Results
				EXEC
				(
					'SELECT ''' + @TableName + '.' + @ColumnName + ''', LEFT(' + @ColumnName + ', 3630) 
					FROM ' + @TableName + ' (NOLOCK) ' +
					' WHERE ' + @ColumnName + ' LIKE ' + @SearchStr2
				)
			END
		END	
	END

	SELECT ColumnName, ColumnValue FROM #Results
END
GO
/****** Object:  StoredProcedure [dbo].[validateAccount]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[validateAccount] 

	@email varchar(100),
	@verif varchar(10)
AS
BEGIN

if exists (select un from users where email=@email and verificationCode=@verif)
	begin
	update users set isVerified=1  where email=@email and verificationCode=@verif
	return 1
	end
else
	begin
	return 0
	end
	
END
GO
/****** Object:  StoredProcedure [dbo].[AddAnnounce]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddAnnounce] 

	@toun varchar(50),
	@anounce varchar(1000),
	@isPublic bit

AS
BEGIN

	INSERT INTO [messagesToUsers]
           ([anounce]
           ,[toun]
           ,[isPublic]
           ,[msgDate])
     VALUES
           (@anounce
           ,@toun
           ,@isPublic
           ,GETUTCDATE())
	return 1
	
END
GO
/****** Object:  StoredProcedure [dbo].[CheckLoginonSession]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckLoginonSession] 

	@un varchar(50),
	@pw varchar(50)
AS
BEGIN
declare @isUserExists bit

	SET NOCOUNT ON;
	
	--clear old sessions older than 20 minutes
	delete from [sessions] where   DATEDIFF(n, updatedon, GETUTCDATE()) >20
	
	if Exists (select un from [sessions] where un=@un and sessionid=@pw)
	begin
	set @isUserExists =1 
		update [sessions] set  updatedon=GETUTCDATE() where un=@un;

	end
	else
	begin
	set @isUserExists =0
	end
	
	return @isUserExists
END
GO
/****** Object:  StoredProcedure [dbo].[CheckLogin]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckLogin] 

	@un varchar(50),
	@pw varchar(50)
AS
BEGIN
declare @isUserExists bit

	SET NOCOUNT ON;
	if Exists (select un from users where un=@un and pw=@pw and isVerified=1)
	begin
	update users set lastLogin=GETUTCDATE() where un=@un
	set @isUserExists =1 
	end
	else
	begin
	set @isUserExists =0
	end
	
	return @isUserExists
END
GO
/****** Object:  StoredProcedure [dbo].[chat_list]    Script Date: 07/06/2009 10:10:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[chat_list] 
@pageid int

AS
BEGIN

Select Top 15 un,m,mTime,unIp,unBrowser from chat where pageid=@pageid order by id desc;
select un from chat where pageid=@pageid group by un order by un asc

END
GO
/****** Object:  StoredProcedure [dbo].[chat_insert]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[chat_insert] 
@pageid int,
@u nvarchar(50),
@m nvarchar(500),
@uIp varchar(15),
@uBrowser nvarchar(250)

AS
BEGIN

INSERT INTO [chat]
           ([pageid]
           ,[un]
           ,[m]
           ,[mTime]
           ,[unIp]
           ,[unBrowser])
     VALUES
           (@pageid 
           ,@u
           ,@m
           ,GETUTCDATE()
           ,@uIp
           ,@uBrowser)
           
           --delete non top 20 posts or posts older than 1 day
delete from chat where id not in (select top 15 id from chat where pageid=@pageid order by id desc)
					or id in (select id from chat where DATEDIFF(day, mTime, GETUTCDATE())>1)
END
GO
/****** Object:  StoredProcedure [dbo].[ChangePassword]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChangePassword] 

	@un varchar(50),
	@pw varchar(50)
AS
BEGIN

update users set pw=@pw where un=@un
return 1
	
END
GO
/****** Object:  StoredProcedure [dbo].[ChangeEmail]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChangeEmail] 

	@un varchar(50),
	@email varchar(100)
AS
BEGIN

update users set email=@email where un=@un
return 1
	
END
GO
/****** Object:  StoredProcedure [dbo].[CountVisitors]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[CountVisitors] 

@subdomain varchar(50)

AS

select COUNT(subdomain) as OnlineVisitorCount from online_visitors where subdomain=@subdomain
GO
/****** Object:  StoredProcedure [dbo].[DeleteAnouncementAdmin]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeleteAnouncementAdmin] 

	@anounceid int

AS
BEGIN

	delete from messagesToUsers where id=@anounceid
	return 1
	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteAnouncement]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteAnouncement] 

	@un varchar(50),
	@anounceid int

AS
BEGIN

	delete from messagesToUsers where toun=@un and id=@anounceid
	return 1
	
END
GO
/****** Object:  Table [dbo].[forum_posts]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[forum_posts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[un] [varchar](50) NOT NULL,
	[topicId] [int] NOT NULL,
	[replyBody] [nvarchar](1000) NOT NULL,
	[postDate] [datetime] NOT NULL,
 CONSTRAINT [PK_forum_replies] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[DevalidateAccount]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DevalidateAccount] 

	@un varchar(50),
	@newVerificationCode varchar (10)
AS
BEGIN

update users set isVerified=0, verificationCode=@newVerificationCode where un=@un

	
END
GO
/****** Object:  StoredProcedure [dbo].[DELETE-UpdateAndCountVisitors-delete]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DELETE-UpdateAndCountVisitors-delete] 

@secondsToKeep int,
@subdomain varchar(50),
@visitorSessionId varchar(50),
@visitorIp varchar(20),
@visitorBrowser varchar(255)


AS

-- delete inactives
DELETE FROM online_visitors WHERE DATEDIFF(second, lastSeen, GETUTCDATE()) > @secondsToKeep;
-- insert user if not exists
if not exists (select visitorSessionId from online_visitors where  visitorSessionId =@visitorSessionId)
		begin
		if not exists (select visitorSessionId from online_visitors where  visitorIp=@visitorIp and visitorBrowser=@visitorBrowser)
		begin
		INSERT INTO online_visitors (subdomain, visitorSessionId, visitorIp, visitorBrowser, lastSeen )
		VALUES (@subdomain , @visitorSessionId, @visitorIp, @visitorBrowser, GETUTCDATE())
		end
		end
-- select count of users
select COUNT(subdomain) as OnlineVisitorCount from online_visitors where subdomain=@subdomain
-- select all users to be listed in next record set
select visitorIp,visitorBrowser from online_visitors where subdomain=@subdomain
GO
/****** Object:  StoredProcedure [dbo].[isuserpaid]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[isuserpaid] 

	@un varchar(50)
AS
BEGIN

if exists (select un from users where un=@un and paidUser=1)
	begin
	return 1
	end
else
	begin
	return 0
	end
	
END
GO
/****** Object:  StoredProcedure [dbo].[IsGlobalAdmin]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[IsGlobalAdmin] 

	@un varchar(50)
AS
BEGIN

if exists (select un from users where un=@un and isGlobalAdmin=1)
	begin
	return 1
	end
else
	begin
	return 0
	end
	
END
GO
/****** Object:  StoredProcedure [dbo].[InsertSession]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[InsertSession] 

	@sessionid varchar(50),
	@un varchar(50)
AS
BEGIN


delete from sessions where un=@un;
INSERT INTO [sessions]
           ([sessionid]
           ,[un]
           ,[updatedon])
     VALUES
           (@sessionid
           ,@un
           ,GETUTCDATE())
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetFeedUrlForBuilder]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFeedUrlForBuilder] 


	@subdomain varchar(50),
	@id int
AS
BEGIN



	select url from rss_providers where subdomain=@subdomain and id=@id



	
END
GO
/****** Object:  StoredProcedure [dbo].[getAnnouncements]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAnnouncements] 
@un varchar(50)

AS
BEGIN
	select * from  messagesToUsers where isPublic=1 or toun=@un order by toun desc, id desc

END
GO
/****** Object:  StoredProcedure [dbo].[GetPostTopic]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetPostTopic] 

	@id int
AS
BEGIN

select topic from forum_topics where id=@id
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetUserPassword]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserPassword] 

	@un varchar(50),
	@email varchar(100)

AS
BEGIN
	select pw from users where un=@un and email=@email
	END
GO
/****** Object:  StoredProcedure [dbo].[Logout]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Logout] 

	@un varchar(50),
	@pw varchar(50)
AS

BEGIN
delete from [sessions] where un=@un and sessionid=@pw
END
GO
/****** Object:  StoredProcedure [dbo].[OnlineUserList]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OnlineUserList] 

@subdomain varchar(50)


AS

if LEN(@subdomain)>0
begin
SELECT [id],[subdomain]
      ,[domain]
      ,[visitorSessionId]
      ,[visitorIp]
      ,[visitorBrowser]
      ,[lastSeen]
      ,[location]
  FROM [online_visitors] where subdomain=@subdomain order by id desc
end
else
begin
SELECT [id],[subdomain]
      ,[domain]
      ,[visitorSessionId]
      ,[visitorIp]
      ,[visitorBrowser]
      ,[lastSeen]
      ,[location]
  FROM [online_visitors]  order by id desc
end
GO
/****** Object:  StoredProcedure [dbo].[RegisterUser]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterUser] 

	@un varchar(50),
	@pw varchar(50),
	@email varchar(100),
	@ip varchar(15),
	@verificationCode varchar(10)
AS
BEGIN
declare @success bit

	SET NOCOUNT ON;
	if Exists (select un from users where un=@un or email=@email)
	begin
	set @success = 0 
	end
	else
	begin
	
	INSERT INTO [users]
           ([un]
           ,[pw]
           ,[email]
           ,[lastLogin]
           ,[ip]
           ,[isVerified]
           ,[verificationCode]
           ,[isGlobalAdmin]
           ,[regDate]
           ,[warningCount]
           ,[lastEmailReceived]
           ,[paidUser])
     VALUES
           (@un
           ,@pw
           ,@email
           ,NULL
           ,@ip
           ,0
           ,@verificationCode
           ,0
           ,GETUTCDATE()
           ,0
           ,GETUTCDATE()
           ,0)
           
	set @success =1
	end
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateVisitorCount]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateVisitorCount] 

@secondsToKeep int,
@subdomain varchar(50),
@domain varchar(50),
@visitorSessionId varchar(50),
@visitorIp varchar(20),
@visitorBrowser varchar(255),
@location varchar(30)


AS

-- delete inactives
DELETE FROM online_visitors WHERE DATEDIFF(second, lastSeen, GETUTCDATE()) > @secondsToKeep;

-- insert user if not exists
if not exists (select visitorSessionId from online_visitors where  visitorIp=@visitorIp and visitorBrowser=@visitorBrowser)
		begin
		INSERT INTO online_visitors (subdomain, domain, visitorSessionId, visitorIp, visitorBrowser, lastSeen, location )
		VALUES (@subdomain , @domain, @visitorSessionId, @visitorIp, @visitorBrowser, GETUTCDATE(), @location)
		end
else
		begin
		UPDATE online_visitors set lastSeen=GETUTCDATE(), location=@location where visitorSessionId=@visitorSessionId
		end
GO
/****** Object:  Table [dbo].[subdomains]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subdomains](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subdomain] [varchar](50) NOT NULL,
	[un] [varchar](50) NOT NULL,
	[domain] [varchar](50) NOT NULL,
	[isActive] [bit] NOT NULL,
	[isR] [bit] NOT NULL,
	[isPaid] [bit] NOT NULL,
 CONSTRAINT [PK_subdomains] PRIMARY KEY CLUSTERED 
(
	[subdomain] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[styles]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[styles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[style] [varchar](3000) NULL,
	[subdomain] [varchar](50) NOT NULL,
 CONSTRAINT [PK_styles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rotatorLinks]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rotatorLinks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NOT NULL,
	[url] [varchar](255) NOT NULL,
	[subdomain] [varchar](50) NOT NULL,
 CONSTRAINT [PK_rotatorLinks] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[ReturnFeedName]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ReturnFeedName] 

	@un varchar(50),
	@subdomain varchar(50),
	@id int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	   
	   
select title  from rss_providers where subdomain=@subdomain and id=@id;
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetFullSubdomainNameById]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetFullSubdomainNameById] 
	@id int
AS
BEGIN

select subdomain+'.'+domain  from subdomains where id=@id;

	
END
GO
/****** Object:  StoredProcedure [dbo].[make_user_unpaid]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[make_user_unpaid] 
@un varchar(50)
AS
BEGIN

	   
	update users set paidUser=0 where un=@un;
	update subdomains set isPaid=0 where un=@un;
	
END
GO
/****** Object:  StoredProcedure [dbo].[make_user_paid]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[make_user_paid] 
@un varchar(50)
AS
BEGIN

	   
	update users set paidUser=1 where un=@un;
	update subdomains set isPaid=1 where un=@un;
	
END
GO
/****** Object:  StoredProcedure [dbo].[ListPaidUsers]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListPaidUsers] 

AS
BEGIN

	   
	Select un,(select COUNT(subdomain) from subdomains where subdomains.un=users.un) as subdomainCount from users where paidUser=1;
	
END
GO
/****** Object:  StoredProcedure [dbo].[getUserEmailAndLastEmailTime]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getUserEmailAndLastEmailTime] 

	@subdomain varchar(50),
	@limittimeinminutes int
AS

declare @un varchar(50)

set nocount on

set @un=(select top 1 un from subdomains where subdomain=@subdomain)
--select @un

if (@un =NULL)
begin
select 'none' as email
end

else

begin
if (DATEDIFF(n,(select lastEmailReceived from users where un=@un), getdate())>@limittimeinminutes) --last email 15 minutes earlies
begin
select email from users where un=@un
update users set lastEmailReceived=GETUTCDATE() where un=@un
end
else
begin
select 'none' as email
end
end
GO
/****** Object:  StoredProcedure [dbo].[GetSubdomainNameById]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSubdomainNameById] 
	@id int
AS
BEGIN

select subdomain from subdomains where id=@id;

	
END
GO
/****** Object:  StoredProcedure [dbo].[GetDomainNameById]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetDomainNameById] 
	@id int
AS
BEGIN

select domain from subdomains where id=@id;

	
END
GO
/****** Object:  Table [dbo].[guestbook]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[guestbook](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subdomain] [varchar](50) NOT NULL,
	[poster] [nvarchar](50) NOT NULL,
	[posterIp] [varchar](20) NOT NULL,
	[posterBrowser] [varchar](255) NOT NULL,
	[postDate] [datetime] NOT NULL,
	[post] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_guestbook_messages] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[ListForumTopics]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListForumTopics] 
@PageNumber int,
@PageSize int
AS

DECLARE	@FirstRow INT,@LastRow INT,@RowCount INT,@PageCount INT

--find recordcount and pages
    SELECT @RowCount = COUNT(*), @PageCount = COUNT(*) / @PageSize 
    FROM forum_topics
---------------------------------------------------------------------
    IF @RowCount % @PageSize != 0 SET @PageCount = @PageCount + 1 
    IF @PageNumber < 1 SET @PageNumber = 1 
    IF @PageNumber > @PageCount SET @PageNumber = @PageCount 
    SELECT 
        CurrentPage = @PageNumber, 
        TotalPages = @PageCount, 
        TotalRows = @RowCount 


SELECT	@FirstRow = ( @PageNumber - 1) * @PageSize + 1,
	@LastRow = (@PageNumber - 1) * @PageSize + @PageSize ;

WITH MyTopics  AS
(
	SELECT	id,un,topic,postDate,updateDate,isSticky,
			ROW_NUMBER() OVER (ORDER BY updateDate DESC) AS RowNumber,
			(Select top 1 forum_posts.un from forum_posts where forum_posts.topicId=forum_topics.id order by id desc) as lastReplyBy
	FROM forum_topics
)
SELECT	RowNumber,id,un,topic,postDate,updateDate,isSticky, lastReplyBy
FROM	MyTopics
WHERE	RowNumber BETWEEN @FirstRow AND @LastRow
ORDER BY isSticky DESC, RowNumber ASC;
GO
/****** Object:  StoredProcedure [dbo].[ListSitesAdults]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListSitesAdults] 
@PageNumber int,
@PageSize int
AS

-- dont include adult sites

DECLARE	@FirstRow INT,@LastRow INT,@RowCount INT,@PageCount INT

--find recordcount and pages
    SELECT @RowCount = COUNT(*), @PageCount = COUNT(*) / @PageSize 
    FROM subdomains where isActive=1 and isR=1
---------------------------------------------------------------------
    IF @RowCount % @PageSize != 0 SET @PageCount = @PageCount + 1 
    IF @PageNumber < 1 SET @PageNumber = 1 
    IF @PageNumber > @PageCount SET @PageNumber = @PageCount 
    SELECT 
        CurrentPage = @PageNumber, 
        TotalPages = @PageCount, 
        TotalRows = @RowCount 


SELECT	@FirstRow = ( @PageNumber - 1) * @PageSize + 1,
	@LastRow = (@PageNumber - 1) * @PageSize + @PageSize ;

WITH subs  AS
(
	SELECT	subdomain,domain,
			ROW_NUMBER() OVER (ORDER BY id DESC) AS RowNumber
	FROM subdomains where isActive=1 and isR=1
)
SELECT	subdomain,domain
FROM	subs
WHERE	RowNumber BETWEEN @FirstRow AND @LastRow
ORDER BY  RowNumber ASC;
GO
/****** Object:  StoredProcedure [dbo].[ListSites]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListSites] 
@PageNumber int,
@PageSize int
AS

-- dont include adult sites

DECLARE	@FirstRow INT,@LastRow INT,@RowCount INT,@PageCount INT

--find recordcount and pages
    SELECT @RowCount = COUNT(*), @PageCount = COUNT(*) / @PageSize 
    FROM subdomains where isActive=1 and isR!=1
---------------------------------------------------------------------
    IF @RowCount % @PageSize != 0 SET @PageCount = @PageCount + 1 
    IF @PageNumber < 1 SET @PageNumber = 1 
    IF @PageNumber > @PageCount SET @PageNumber = @PageCount 
    SELECT 
        CurrentPage = @PageNumber, 
        TotalPages = @PageCount, 
        TotalRows = @RowCount 


SELECT	@FirstRow = ( @PageNumber - 1) * @PageSize + 1,
	@LastRow = (@PageNumber - 1) * @PageSize + @PageSize ;

WITH subs  AS
(
	SELECT	subdomain,domain,
			ROW_NUMBER() OVER (ORDER BY id DESC) AS RowNumber
	FROM subdomains where isActive=1 and isR!=1
)
SELECT	subdomain,domain
FROM	subs
WHERE	RowNumber BETWEEN @FirstRow AND @LastRow
ORDER BY  RowNumber ASC;
GO
/****** Object:  StoredProcedure [dbo].[listRSSLinks]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listRSSLinks] 

	@un varchar(50),
	@subdomain varchar(50)
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	   
	   
select *  from rss_providers where subdomain=@subdomain order by title asc;
end
	
END
GO
/****** Object:  Table [dbo].[poll]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[poll](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subdomain] [varchar](50) NOT NULL,
	[pollTitle] [nvarchar](50) NOT NULL,
	[lastVoteDate] [datetime] NOT NULL,
	[lastVoteIp] [varchar](20) NOT NULL,
 CONSTRAINT [PK_poll] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pages]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pages](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NULL,
	[description] [nvarchar](100) NULL,
	[keywords] [nvarchar](100) NULL,
	[other_meta] [varchar](500) NULL,
	[fontSize] [varchar](50) NULL,
	[pageBgColor] [varchar](50) NULL,
	[pageFontColor] [varchar](50) NULL,
	[pageFolder] [varchar](50) NULL,
	[isStartPage] [bit] NOT NULL,
	[subdomain] [varchar](50) NOT NULL,
	[hits] [int] NOT NULL,
	[lastVisitor] [datetime] NULL,
 CONSTRAINT [PK_pages] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[DeleteTopic]    Script Date: 07/06/2009 10:10:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeleteTopic] 

	@id int,
	@what int
AS
BEGIN

if (@what=1)
begin
Delete from forum_topics where id=@id
end
else
begin
Delete from forum_posts where id=@id
end

	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteSite]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeleteSite] 
	@un varchar(50),
	@subdomain varchar(50)

AS
BEGIN
declare @success int

	SET NOCOUNT ON;
	if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
	begin
	

			delete from subdomains where subdomain=@subdomain
			set @success =1

			

			
	end
	
	else
	begin
	set @success = 0
	end
	
	return @success
END
GO
/****** Object:  Table [dbo].[codeSnippest]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[codeSnippest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subdomain] [varchar](50) NOT NULL,
	[snippetName] [nvarchar](50) NOT NULL,
	[snippetCode] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_codeSnippest] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[CheckSubdomain]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[CheckSubdomain] 

	@subdomain varchar(50),
	@domain varchar(50)
AS
BEGIN
	if Exists (select subdomain from subdomains where subdomain=@subdomain and domain=@domain and isActive=1)
	begin
	return 1
	end
	else
	begin
	return 0
	end
	

END
GO
/****** Object:  StoredProcedure [dbo].[ChangeSiteStatus]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ChangeSiteStatus] 
	@un varchar(50),
	@subdomain varchar(50)

AS
BEGIN
declare @success int

	SET NOCOUNT ON;
	if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
	begin
	

			if (select isActive from subdomains where subdomain=@subdomain)=0
			begin
			update subdomains set isActive=1 where subdomain=@subdomain
			set @success =1
			end
			else
			begin
			update subdomains set isActive=0 where subdomain=@subdomain
			set @success =0
			end
			

			
	end
	
	else
	begin
	set @success = 2
	end
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[ChangeSiteRating]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChangeSiteRating] 
	@un varchar(50),
	@subdomain varchar(50)

AS
BEGIN
declare @success int



	SET NOCOUNT ON;
	if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
	begin
	
			if (select isR from subdomains where subdomain=@subdomain)=0
			begin
			update subdomains set isR=1 where subdomain=@subdomain
			set @success =1
			end
			else
			begin
			update subdomains set isR=0 where subdomain=@subdomain
			set @success =0
			end
			
	end
	
	else
	begin
	set @success = 2
	end
	
	return @success
END
GO
/****** Object:  Table [dbo].[advertisers]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[advertisers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subdomain] [varchar](50) NOT NULL,
	[advertCompany] [varchar](50) NOT NULL,
	[publisherId] [varchar](50) NOT NULL,
	[alternateId] [varchar](50) NULL,
 CONSTRAINT [PK_advertisers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[AddForumTopic]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddForumTopic] 

	@un varchar(50),
	@topic varchar(100),
	@replyBody varchar(1000),
	@isSticky bit
AS
BEGIN
declare @success bit

	SET NOCOUNT ON;
	--if topic already exits. stop double posts
	if Exists (select topic from forum_topics where un=@un and topic=@topic)
	begin
	set @success = 0 
	end
	else
	begin
	--if topic does not exists
	INSERT INTO [forum_topics]
           ([un]
           ,[topic]
           ,[postDate] 
           ,[updateDate] 
           ,[isSticky] )
     VALUES
           (@un
           ,@topic 
           ,GETUTCDATE()
           ,GETUTCDATE()
           ,@isSticky)
    --insert into reply database
    INSERT INTO [forum_posts]    
			(	[un] ,
				[topicId] ,
				[replyBody] ,
				[postDate]	)
		VALUES 
				(@un, 
				 SCOPE_IDENTITY(),
				 @replyBody ,
				 GETUTCDATE())
				 
	set @success =1
	end
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[AddRSSLink]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AddRSSLink] 

	@un varchar(50),
	@subdomain varchar(50),
	@title nvarchar(100),
@url varchar (255)
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin
INSERT INTO [rss_providers]
           ([title]
           ,[url]
           ,[subdomain])
     VALUES
           (@title
           ,@url
           ,@subdomain)
	
	return 1
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[AddReplyToForumTopic]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddReplyToForumTopic] 

	@un varchar(50),
	@id int,
	@replyBody varchar(1000)
AS
BEGIN
declare @success bit

	SET NOCOUNT ON;
	--stop double posts
	if Exists (select id from forum_topics where id=@id)
	begin
	
    --insert into reply database
    --stop double post
    if exists (select id from forum_posts where un=@un and topicId=@id and replyBody=@replyBody)
    	begin
	set @success=0
	end
	else

	    begin
    INSERT INTO [forum_posts]    
			(	[un] ,
				[topicId] ,
				[replyBody] ,
				[postDate]	)
		VALUES 
				(@un, 
				 @id,
				 @replyBody ,
				 GETUTCDATE())
		--update topic update time
		update forum_topics set updateDate=GETUTCDATE()	where id=@id		 
		set @success = 1
	end
	end
	else
	begin	 
	set @success =0
	end
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[ViewForumTopic]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ViewForumTopic] 
@id int,
@PageNumber int,
@PageSize int
AS

DECLARE	@FirstRow INT,@LastRow INT,@RowCount INT,@PageCount INT

--find recordcount and pages
    SELECT @RowCount = COUNT(*), @PageCount = COUNT(*) / @PageSize 
    FROM forum_posts where topicId=@id
---------------------------------------------------------------------
    IF @RowCount % @PageSize != 0 SET @PageCount = @PageCount + 1 
    IF @PageNumber < 1 SET @PageNumber = 1 
    IF @PageNumber > @PageCount SET @PageNumber = @PageCount 
    SELECT 
        CurrentPage = @PageNumber, 
        TotalPages = @PageCount, 
        TotalRows = @RowCount 


SELECT	@FirstRow = ( @PageNumber - 1) * @PageSize + 1,
	@LastRow = (@PageNumber - 1) * @PageSize + @PageSize ;

WITH MyTopics  AS
(
	SELECT	id,un,topicId,replyBody,postDate,
			ROW_NUMBER() OVER (ORDER BY id ASC) AS RowNumber
	FROM forum_posts where topicId=@id
)
SELECT	RowNumber,id,un,topicId,replyBody,postDate
FROM	MyTopics
WHERE	RowNumber BETWEEN @FirstRow AND @LastRow
ORDER BY RowNumber ASC;
GO
/****** Object:  StoredProcedure [dbo].[poll_add]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[poll_add] 
	@un varchar(50),
	@subdomain varchar(50),
	@pollTitle nvarchar(250),
	@limit int
AS
BEGIN

if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

if (select COUNT(*) from poll where subdomain=@subdomain)<@limit --sayfa sayisi limitten az ise
begin

------------------
INSERT INTO [poll]
           ([subdomain]
           ,[pollTitle]
           ,[lastVoteDate]
           ,[lastVoteIp])
     VALUES
           (@subdomain
           ,@pollTitle
           ,GETUTCDATE()
           ,'127.0.0.1')
           return SCOPE_IDENTITY();
------------------
end
	else
	begin
	return 0
	end
end


END
GO
/****** Object:  StoredProcedure [dbo].[AddALink]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AddALink] 

	@un varchar(50),
	@subdomain varchar(50),
	@title nvarchar(100),
@url varchar (255)
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin
INSERT INTO [rotatorLinks]
           ([title]
           ,[url]
           ,[subdomain])
     VALUES
           (@title
           ,@url
           ,@subdomain)
	
	return 1
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[AddAdvertTwo]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AddAdvertTwo] 

	@un varchar(50),
	@subdomain varchar(50),
	@advertCompany varchar(50),
	@advertPublisherId varchar(50),
	@alternatePublisherId varchar(50),
	@limit int

AS
BEGIN
declare @success bit
set nocount on;

if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

if (select COUNT(*) from advertisers where subdomain=@subdomain)<@limit --sayfa sayisi limitten az ise
begin
----------------------------------------------------------
    INSERT INTO [advertisers]
           ([subdomain]
           ,[advertCompany]
           ,[publisherId]
           ,[alternateId])
     VALUES
           (@subdomain,
            @advertCompany,
            @advertPublisherId,
            @alternatePublisherId)
 	set @success =1
-------------------------------------------------------------------------	
	end
	else
	begin
	set @success =0
	end
end

	
	else
	begin
	set @success = 0 
	end
	
	
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[AddAdvert]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AddAdvert] 

	@un varchar(50),
	@subdomain varchar(50),
	@advertCompany varchar(50),
	@advertPublisherId varchar(50),
	@limit int

AS
BEGIN
declare @success bit
set nocount on;

if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

if (select COUNT(*) from advertisers where subdomain=@subdomain)<@limit --sayfa sayisi limitten az ise
begin
----------------------------------------------------------
    INSERT INTO [advertisers]
           ([subdomain]
           ,[advertCompany]
           ,[publisherId]
           ,[alternateId])
     VALUES
           (@subdomain,
            @advertCompany,
            @advertPublisherId,
            Null)
 	set @success =1
-------------------------------------------------------------------------	
	end
	else
	begin
	set @success =0
	end
end

	
	else
	begin
	set @success = 0 
	end
	
	
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[AddGuestBookMessage]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddGuestBookMessage] 

@subdomain varchar(50),
@poster nvarchar(50),
@posterIp varchar(20),
@posterBrowser varchar(255),
@post nvarchar(500),
@limit int

AS
BEGIN
declare @minutesSincelastMsg int
set @minutesSincelastMsg = (select top 1 DATEDIFF(n,postDate,GETUTCDATE())  from guestbook where subdomain=@subdomain order by id desc)

--same user same post same day not accepted!
if Exists (select id from guestbook where posterIp=@posterIp and post=@post and subdomain=@subdomain and DATEDIFF(d,postDate,GETUTCDATE())<2)
begin
return 0
--select '0'
end
else
begin
if (@minutesSincelastMsg>@limit or @minutesSincelastMsg IS NULL)
begin
INSERT INTO [PHN].[dbo].[guestbook]
           ([subdomain]
           ,[poster]
           ,[posterIp]
           ,[posterBrowser]
           ,[postDate]
           ,[post])
     VALUES
           (@subdomain
           ,@poster
           ,@posterIp
           ,@posterBrowser
           ,GETUTCDATE()
           ,@post)
return 1
end
else
begin
return 0
end
End

End
GO
/****** Object:  StoredProcedure [dbo].[AddCodeSnippet]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddCodeSnippet] 

	@un varchar(50),
	@subdomain varchar(50),
	@name nvarchar(50),
	@code nvarchar(1000),
	@limit int
AS
BEGIN
declare @success bit
set nocount on;

if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

if (select COUNT(*) from codeSnippest where subdomain=@subdomain)<@limit --sayfa sayisi limitten az ise
begin
----------------------------------------------------------
    INSERT INTO [codeSnippest]
           ([subdomain]
           ,[snippetName],[snippetCode])
     VALUES
           (@subdomain,@name,@code) 
 	set @success =1
-------------------------------------------------------------------------	
	end
	else
	begin
	set @success =0
	end
end

	
	else
	begin
	set @success = 0 
	end
	
	
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[ADMIN_Illegal_keyword_check_in_gb]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ADMIN_Illegal_keyword_check_in_gb] 
@q nvarchar(50)
AS
BEGIN


SELECT 
(select domain from subdomains where subdomains.subdomain=guestbook.subdomain) as domain,
(select un from subdomains where subdomain=guestbook.subdomain) as un ,
(select lastLogin from users where un=(select un from subdomains where subdomain=guestbook.subdomain)) as LastLogon,
(select email from users where un=(select un from subdomains where subdomain=guestbook.subdomain)) as email,

id,
poster,
subdomain,
post
from guestbook WHERE poster LIKE '%'+@q+'%' or post LIKE '%'+@q+'%'



END
GO
/****** Object:  StoredProcedure [dbo].[DeleteALink]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeleteALink] 

	@un varchar(50),
	@subdomain varchar(50),
	@linkid int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	delete from rotatorLinks where subdomain=@subdomain and id=@linkid
	return 1
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteAGbPostOwner]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeleteAGbPostOwner] 


	@postid int
AS
BEGIN


	delete from guestbook where id=@postid
	return 1
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteAGbPost]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteAGbPost] 

	@un varchar(50),
	@subdomain varchar(50),
	@postid int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin
	
	if @postid>0 --if pageid 0 that means user wants to delete all messages
	begin
	delete from guestbook where subdomain=@subdomain and id=@postid
	end
	else
	begin
	delete from guestbook where subdomain=@subdomain
	end
	return 1
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteAPageOwner]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeleteAPageOwner] 

	@pageid int
AS
BEGIN


	delete from pages where id=@pageid
	return 1

	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteAPage]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteAPage] 

	@un varchar(50),
	@subdomain varchar(50),
	@pageid int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin
if (select isStartPage from pages where subdomain=@subdomain and id=@pageid)=0
begin
	delete from pages where subdomain=@subdomain and id=@pageid
	return 1
end
else
begin
return 0
end	   
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[DELETE-AddHit-DELETE]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DELETE-AddHit-DELETE] 

	@subdomain varchar(50),
	@pageid int

AS
BEGIN

set nocount on

select hits from pages where id=@pageid and subdomain=@subdomain
update pages set hits=hits+1 where id=@pageid and subdomain=@subdomain
END
GO
/****** Object:  StoredProcedure [dbo].[AddSitePage]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddSitePage] 

	@un varchar(50),
	@subdomain varchar(50),
	@title nvarchar(100),
	@description nvarchar(100),
	@keywords nvarchar(100),
	@other_meta varchar(500),
	@fontSize varchar(50),
	@pageBgColor varchar(50),
	@pageFontColor varchar(50),
	@pageFolder varchar(50),
	@limit int
AS
BEGIN
declare @success bit
set nocount on;

if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

if (select COUNT(*) from pages where subdomain=@subdomain)<@limit --sayfa sayisi limitten az ise
begin
----------------------------------------------------------
    INSERT INTO [pages]
           ([title]
           ,[description]
           ,[keywords]
           ,[other_meta]
           ,[fontSize]
           ,[pageBgColor]
           ,[pageFontColor]
           ,[pageFolder]
           ,[isStartPage]
           ,[subdomain]
           ,[hits])
     VALUES
           (@title
           ,@description
           ,@keywords
           ,@other_meta
           ,@fontSize
           ,@pageBgColor
           ,@pageFontColor
           ,@pageFolder
           ,0
           ,@subdomain
           ,0) 
    --if there is one page as default set it as default page
    if (select COUNT(*) from pages where subdomain=@subdomain and isStartPage=1)=0
    begin
    update pages set isStartPage=1 where subdomain=@subdomain and id=@@IDENTITY
    end    
	set @success =1
-------------------------------------------------------------------------	
	end
	else
	begin
	set @success =0
	end
end

	
	else
	begin
	set @success = 0 
	end
	
	
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteInactiveUsers]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeleteInactiveUsers] 


	@days int

AS
BEGIN

delete from users where 
DATEDIFF(DAY, lastLogin, GETDATE()) >@days
and 
DATEDIFF(DAY,(select top 1 lastVisitor from pages where subdomain in (select subdomain from subdomains where users.un=subdomains.un) order by pages.lastVisitor desc), GETDATE()) >@days
and isGlobalAdmin=0

END
GO
/****** Object:  StoredProcedure [dbo].[DeleteInactiveSubdomains]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeleteInactiveSubdomains] 


	@days int

AS
BEGIN

delete from subdomains where 
DATEDIFF(DAY, (select top 1 lastVisitor from pages where pages.subdomain=subdomains.subdomain order by lastVisitor desc), GETDATE()) >@days
and isPaid=0

END
GO
/****** Object:  Table [dbo].[pageBlocks]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pageBlocks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pageId] [int] NOT NULL,
	[tagType] [varchar](20) NOT NULL,
	[tagRelatedId] [int] NULL,
	[tagText] [nvarchar](3000) NULL,
	[tagLinkTitle] [nvarchar](255) NULL,
	[tagLinkUrl] [varchar](255) NULL,
	[tagStyleClass] [varchar](20) NULL,
	[tagFontColor] [varchar](10) NULL,
	[tagFontDecoration] [varchar](10) NULL,
	[tagFontWeight] [varchar](10) NULL,
	[tagFontSize] [varchar](10) NULL,
	[tagBgColor] [varchar](10) NULL,
	[tagBorderSize] [smallint] NULL,
	[tagBorderColor] [varchar](10) NULL,
	[tagAlignment] [varchar](10) NULL,
	[positionInPage] [int] NULL,
 CONSTRAINT [PK_pageBlocks] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [Pk_pageBlocks_pageId] ON [dbo].[pageBlocks] 
(
	[pageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[listRotatorLinks]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[listRotatorLinks] 

	@un varchar(50),
	@subdomain varchar(50)
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	   
	   
select *,(select id from subdomains where subdomain=@subdomain) as subdomainid  from rotatorLinks where subdomain=@subdomain order by id asc;
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[listFolders]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listFolders] 

	@un varchar(50),
	@subdomain varchar(50)
AS
BEGIN

if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	   
	   
select  ISNULL(pageFolder, 'Root') as folderName,ISNULL(pageFolder, 'Root') as folderName2, count(*) as pagesInFolder from pages where subdomain=@subdomain group by pageFolder order by pageFolder asc;

end
	
END
GO
/****** Object:  StoredProcedure [dbo].[listCodeSnippets]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listCodeSnippets] 

	@un varchar(50),
	@subdomain varchar(50)
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	   
	   
select *,(select id from subdomains where subdomain=@subdomain) as subdomainid  from codeSnippest where subdomain=@subdomain order by id asc;
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[ListAdverts]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListAdverts] 

	@un varchar(50),
	@subdomain varchar(50)
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	   
	   
select *,(select id from subdomains where subdomain=@subdomain) as subdomainid  from advertisers where subdomain=@subdomain order by advertCompany asc;
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetDefaultPage]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetDefaultPage] 

	@subdomain varchar(50)


AS
BEGIN
	select top 1 id from pages where subdomain=@subdomain and isStartPage=1
	END
GO
/****** Object:  StoredProcedure [dbo].[GetCurrentStyle]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetCurrentStyle] 

	@subdomain varchar(50)


AS
BEGIN
	select style from styles where subdomain=@subdomain
	END
GO
/****** Object:  StoredProcedure [dbo].[GetAsnipForBuilder]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetAsnipForBuilder] 


	@subdomain varchar(50),
	@id int
AS
BEGIN



	select snippetCode from codeSnippest where subdomain=@subdomain and id=@id



	
END
GO
/****** Object:  StoredProcedure [dbo].[GetAsnip]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetAsnip] 

	@un varchar(50),
	@subdomain varchar(50),
	@id int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	select * from codeSnippest where subdomain=@subdomain and id=@id


end
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetPageHeader]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetPageHeader] 
	@un varchar(50),
	@subdomain varchar(50),
	@id int

AS
BEGIN

	SET NOCOUNT ON;
	if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
	begin
	
	select * from pages where id=@id and subdomain=@subdomain;
			
	end
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetAdIdsForBuilder]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAdIdsForBuilder] 


	@subdomain varchar(50),
	@id int
AS
BEGIN



	select advertCompany, publisherId, alternateId from advertisers where subdomain=@subdomain and id=@id



	
END
GO
/****** Object:  StoredProcedure [dbo].[GetRandomLinkFromLinkRotator]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetRandomLinkFromLinkRotator] 


	@subdomain varchar(50)
	
AS
BEGIN



	select Top 1 title,url from rotatorLinks where subdomain=@subdomain order by NEWID()



	
END
GO
/****** Object:  StoredProcedure [dbo].[listUsersSubdomains]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listUsersSubdomains] 

	@un varchar(50)
AS
BEGIN

select *,ISNULL((select SUM(hits) from pages where pages.subdomain=subdomains.subdomain),0) as totalhits from subdomains where un=@un order by subdomain asc;
	
END
GO
/****** Object:  StoredProcedure [dbo].[listJustSubdomainPages]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listJustSubdomainPages] 

	@un varchar(50),
	@subdomain varchar(50)
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin
	   
	   
select * from pages where subdomain=@subdomain order by pageFolder asc, id asc;
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[ListGuestBookMessages]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListGuestBookMessages] 
@PageNumber int,
@PageSize int,
@subdomain varchar(50)
AS

DECLARE	@FirstRow INT,@LastRow INT,@RowCount INT,@PageCount INT

--find recordcount and pages
    SELECT @RowCount = COUNT(*), @PageCount = COUNT(*) / @PageSize 
    FROM guestbook where subdomain=@subdomain
---------------------------------------------------------------------
    IF @RowCount % @PageSize != 0 SET @PageCount = @PageCount + 1 
    IF @PageNumber < 1 SET @PageNumber = 1 
    IF @PageNumber > @PageCount SET @PageNumber = @PageCount 
    SELECT 
        CurrentPage = @PageNumber, 
        TotalPages = @PageCount, 
        TotalRows = @RowCount 


SELECT	@FirstRow = ( @PageNumber - 1) * @PageSize + 1,
	@LastRow = (@PageNumber - 1) * @PageSize + @PageSize ;

WITH MyTopics  AS
(
	SELECT	* ,ROW_NUMBER() OVER (ORDER BY postDate DESC) AS RowNumber
	FROM guestbook where subdomain=@subdomain
)
SELECT	*
FROM	MyTopics
WHERE	RowNumber BETWEEN @FirstRow AND @LastRow
ORDER BY postDate DESC, RowNumber ASC;
GO
/****** Object:  StoredProcedure [dbo].[listSubdomainPages]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listSubdomainPages] 

	@un varchar(50),
	@subdomain varchar(50),
		@folder varchar(50)
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin
select (select SUM(hits) from pages where subdomain=@subdomain) as totalhits,
	   (select COUNT(id) from pages where subdomain=@subdomain) as totalpages,
	   (select isR from subdomains where subdomain=@subdomain) as isR,
	   ISNULL((select id from pages where subdomain=@subdomain and isStartPage=1),0) as isStartPage,
	   (select id from subdomains where subdomain=@subdomain) as subdomainid,
	   (select isActive from subdomains where subdomain=@subdomain) as isActive


   
if @folder=Null or @folder=''
begin	    
select *,(select id from subdomains where subdomain=@subdomain) as subdomainid from pages where subdomain=@subdomain order by isStartPage desc, id asc;
end
else
begin
select *,(select id from subdomains where subdomain=@subdomain) as subdomainid from pages where subdomain=@subdomain and pageFolder=@folder  order by isStartPage desc, id asc;

end

end
	
END
GO
/****** Object:  StoredProcedure [dbo].[ListSitesPopularAdult]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListSitesPopularAdult] 
@PageNumber int,
@PageSize int
AS
-- dont include adult sites
DECLARE	@FirstRow INT,@LastRow INT,@RowCount INT,@PageCount INT
--find recordcount and pages
SELECT @RowCount = COUNT(*), @PageCount = COUNT(*) / @PageSize 
FROM    
 (  
select x.subdomain,f.domain, x.hits, f.isR
from (
   select subdomain, sum(hits) as hits
   from pages group by subdomain
) as x inner join subdomains as f on f.subdomain= x.subdomain and f.isActive=1 and f.isR=1

) t


---------------------------------------------------------------------
    IF @RowCount % @PageSize != 0 SET @PageCount = @PageCount + 1 
    IF @PageNumber < 1 SET @PageNumber = 1 
    IF @PageNumber > @PageCount SET @PageNumber = @PageCount 
    SELECT 
        CurrentPage = @PageNumber, 
        TotalPages = @PageCount, 
        TotalRows = @RowCount 


SELECT	@FirstRow = ( @PageNumber - 1) * @PageSize + 1,
	@LastRow = (@PageNumber - 1) * @PageSize + @PageSize ;
	
	
WITH subs  AS
(
	SELECT	subdomain,domain,hits,ROW_NUMBER() OVER (ORDER BY hits DESC) AS RowNumber
	FROM (  
			select x.subdomain,f.domain, x.hits, f.isR
			from (
			   select subdomain, sum(hits) as hits
			   from pages group by subdomain
			) as x inner join subdomains as f on f.subdomain= x.subdomain and f.isActive=1 and f.isR=1
		) y
		)
SELECT	subdomain,domain,hits
FROM	subs
WHERE	RowNumber BETWEEN @FirstRow AND @LastRow
ORDER BY  RowNumber ASC;
GO
/****** Object:  StoredProcedure [dbo].[ListSitesPopular]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ListSitesPopular] 
@PageNumber int,
@PageSize int
AS
-- dont include adult sites
DECLARE	@FirstRow INT,@LastRow INT,@RowCount INT,@PageCount INT
--find recordcount and pages
SELECT @RowCount = COUNT(*), @PageCount = COUNT(*) / @PageSize 
FROM    
 (  
select x.subdomain,f.domain, x.hits, f.isR
from (
   select subdomain, sum(hits) as hits
   from pages group by subdomain
) as x inner join subdomains as f on f.subdomain= x.subdomain and f.isActive=1 and f.isR=0

) t


---------------------------------------------------------------------
    IF @RowCount % @PageSize != 0 SET @PageCount = @PageCount + 1 
    IF @PageNumber < 1 SET @PageNumber = 1 
    IF @PageNumber > @PageCount SET @PageNumber = @PageCount 
    SELECT 
        CurrentPage = @PageNumber, 
        TotalPages = @PageCount, 
        TotalRows = @RowCount 


SELECT	@FirstRow = ( @PageNumber - 1) * @PageSize + 1,
	@LastRow = (@PageNumber - 1) * @PageSize + @PageSize ;
	
	
WITH subs  AS
(
	SELECT	subdomain,domain,hits,ROW_NUMBER() OVER (ORDER BY hits DESC) AS RowNumber
	FROM (  
			select x.subdomain,f.domain, x.hits, f.isR
			from (
			   select subdomain, sum(hits) as hits
			   from pages group by subdomain
			) as x inner join subdomains as f on f.subdomain= x.subdomain and f.isActive=1 and f.isR=0
		) y
		)
SELECT	subdomain,domain,hits
FROM	subs
WHERE	RowNumber BETWEEN @FirstRow AND @LastRow
ORDER BY  RowNumber ASC;
GO
/****** Object:  Table [dbo].[poll_answers]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[poll_answers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pollid] [int] NOT NULL,
	[answer] [nvarchar](250) NOT NULL,
	[votes] [int] NOT NULL,
 CONSTRAINT [PK_poll_questions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[poll_list]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[poll_list] 
@un varchar(50),
	@subdomain varchar(50)

AS
BEGIN

if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin
Select id,pollTitle from poll where subdomain=@subdomain order by pollTitle asc;
end
END
GO
/****** Object:  StoredProcedure [dbo].[poll_get_title_by_id]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[poll_get_title_by_id] 
@id int

AS
BEGIN


Select pollTitle from poll  where id=@id

END
GO
/****** Object:  StoredProcedure [dbo].[ReturnCodeSnippetName]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ReturnCodeSnippetName] 

	@un varchar(50),
	@subdomain varchar(50),
	@id int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	   
	   
select snippetName  from codeSnippest where subdomain=@subdomain and id=@id;
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[ReturnAdvertName]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ReturnAdvertName] 

	@un varchar(50),
	@subdomain varchar(50),
	@id int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	   
	   
select advertCompany + '-' +publisherId  from advertisers where subdomain=@subdomain and id=@id;
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[RegisterSubdomain]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RegisterSubdomain] 

	@un varchar(50),
	@subdomain varchar(50),
	@domain varchar(50),
	@limit int,
	@isR bit
AS
BEGIN
declare @success bit

	SET NOCOUNT ON;
	if Exists (select subdomain from subdomains where subdomain=@subdomain)
	begin
	set @success = 0 
	end
	else
	begin
	if (select COUNT(*) from subdomains where un=@un)<@limit
	begin
	INSERT INTO [subdomains]
           ([un]
           ,[subdomain]
           ,[domain]
           ,[isActive]
           ,[isR]
           ,[isPaid])
     VALUES
           (@un
           ,LOWER(@subdomain)
           ,LOWER(@domain)
           ,1
           ,@isR
           ,(SELECT paidUser from users where un=@un))
    --add sample page
    INSERT INTO [pages]
           ([title]
           ,[description]
           ,[pageFolder]
           ,[keywords]
           ,[isStartPage]
           ,[subdomain]
           ,[hits])
     VALUES
           ('Welcome to my site'
           ,'My default page'
           ,'Root'
           ,@subdomain
           ,1
           ,@subdomain
           ,0)  
--add sample style
INSERT INTO [styles]
           ([style]
           ,[subdomain])
     VALUES
           (
N'body {
margin: 0px;
padding:0px;
}'
			,@subdomain)
-- set success 1     
	set @success =1
	end
	
	end
	
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateStyle]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[UpdateStyle] 
	@un varchar(50),
	@subdomain varchar(50),
	@style varchar(3000)
AS
BEGIN
declare @success bit

	SET NOCOUNT ON;
	if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
	begin
	
			if exists (select subdomain from styles where subdomain=@subdomain)
			begin
			--update
			update styles set style=@style where subdomain=@subdomain
			set @success =1
			end
			else
			begin
			--insert
			INSERT INTO [styles]
           ([style]
           ,[subdomain])
			 VALUES
           (@style
           ,@subdomain)
			set @success =1
			end

			
	end
	
	else
	begin
	set @success =0
	end
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateSitePage]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateSitePage] 
	@un varchar(50),
	@subdomain varchar(50),
	@id int,
	@title nvarchar(100),
	@description nvarchar(100),
	@keywords nvarchar(100),
@fontSize varchar(50),
@pageBgColor varchar(50),
@pageFontColor varchar(50),
@pageFolder varchar(50),
@other_meta varchar(500)

AS
BEGIN
declare @success bit


	SET NOCOUNT ON;
	
	if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
	begin
	
	if exists (select id from pages where subdomain=@subdomain and id=@id)
	begin
	update pages set [title]=@title,[description]=@description,[keywords]=@keywords,[fontSize]=@fontSize,[pageBgColor]=@pageBgColor,[pageFontColor]=@pageFontColor, [pageFolder]=@pageFolder, [other_meta]=@other_meta where id=@id
	set @success=1
	end
	else
	begin
	set @success=0
	end
	
		
	end
	else
	begin
		set @success =0
	end
		
		return @success
	
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateCodeSnippet]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateCodeSnippet] 

	@un varchar(50),
	@subdomain varchar(50),
	@name nvarchar(50),
	@code nvarchar(1000),
	@id int
AS
BEGIN
declare @success bit
set nocount on;

if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

if exists (select id from codeSnippest where subdomain=@subdomain and id=@id)
begin
----------------------------------------------------------
    update codeSnippest set snippetName=@name,snippetCode=@code where id=@id and subdomain=@subdomain
 	set @success =1
-------------------------------------------------------------------------	
	end
	else
	begin
	set @success =0
	end
end

	
	else
	begin
	set @success = 0 
	end
	
	
	
	return @success
END
GO
/****** Object:  StoredProcedure [dbo].[TotalHits]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[TotalHits] 
@subdomain varchar(50)
AS
BEGIN

select ISNULL((select SUM(hits) from pages where subdomain=@subdomain),0) as totalhits
	
END
GO
/****** Object:  StoredProcedure [dbo].[SiteMap]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SiteMap] 

	@subdomain varchar(50)
AS
BEGIN
	   
	   
select id,title from pages where subdomain=@subdomain order by title asc;

	
END
GO
/****** Object:  StoredProcedure [dbo].[setIndexPage]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[setIndexPage] 

	@un varchar(50),
	@subdomain varchar(50),
	@pageid int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

update pages set isStartPage=0 where subdomain=@subdomain
update pages set isStartPage=1 where id=@pageid
	   
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateTag]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateTag] 
	
	@subdomain varchar(50),
	@pageid int,
	@tagid int,
	@tagText varchar(3000),
	@tagStyleClass varchar(50),
	@tagFontSize varchar(10),
	@tagFontColor varchar(10),
	@tagFontDecoration varchar(10),
	@tagFontWeight varchar(10),
	@tagBgColor varchar(10),
	@tagBorderSize smallint,
	@tagBorderColor varchar(10),
	@tagAlignment varchar(10)
	
AS
BEGIN


if exists(select subdomain from pages where subdomain=@subdomain)
begin
	if exists (select pageId from pageBlocks where pageId=@pageid and id=@tagid)
	begin
	
		UPDATE [pageBlocks]
		   SET 
			   [tagText] = @tagText
			  ,[tagStyleClass] = @tagStyleClass
			  ,[tagFontColor] = @tagFontColor
			  ,[tagFontDecoration] = @tagFontDecoration
			  ,[tagFontWeight] = @tagFontWeight
			  ,[tagFontSize] = @tagFontSize
			  ,[tagBgColor] = @tagBgColor
			  ,[tagBorderSize] = @tagBorderSize
			  ,[tagBorderColor] = @tagBorderColor
			  ,[tagAlignment] = @tagAlignment
		 WHERE id=@tagId
	return 1
	end
end
else
begin
return 0
end


	
END
GO
/****** Object:  StoredProcedure [dbo].[poll_vote_now]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[poll_vote_now] 

@pollid int,
@answerid int,
@posterIp varchar(20),
@limit int --in SECONDS

AS
BEGIN
declare @secondsSincelastMsg int
set @secondsSincelastMsg = (select DATEDIFF(SECOND,lastVoteDate,GETUTCDATE())  from poll where id=@pollid and lastVoteIp=@posterIp)


if (@secondsSincelastMsg>@limit or @secondsSincelastMsg IS NULL)
begin
update poll_answers set votes=votes+1 where pollid=pollid and id=@answerid
update poll set lastVoteIp=@posterIp,lastVoteDate=GETUTCDATE() where id=@pollid
return 1
--select 1
end
else
begin
return 0
--select 0
End

End
GO
/****** Object:  StoredProcedure [dbo].[poll_show_by_id]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[poll_show_by_id] 

	@id int
AS
BEGIN
declare @totalVotes int

set @totalVotes= (select SUM(votes) from poll_answers where pollid=@id)

if @totalVotes>0
begin
Select pollTitle from poll where id=@id;
select *,convert(decimal(18,1),(100.0 * votes/@totalVotes)) as mypercent
from poll_answers where pollid=@id 
end
else
begin
Select pollTitle from poll where id=@id;
select *,0 as mypercent
from poll_answers where pollid=@id 
end

END
GO
/****** Object:  StoredProcedure [dbo].[poll_show]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[poll_show] 
	@subdomain varchar(50),
	@id int
AS
BEGIN

Select pollTitle from poll where subdomain=@subdomain and id=@id;
select * from poll_answers where pollid=@id order by id asc;


END
GO
/****** Object:  StoredProcedure [dbo].[poll_reset]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[poll_reset] 

	@un varchar(50),
	@subdomain varchar(50),
	@pollid int
AS
BEGIN

SET NOCOUNT ON

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	update  poll_answers set votes=0 where pollid=@pollid
	return 1
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[poll_delete]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[poll_delete] 

	@un varchar(50),
	@subdomain varchar(50),
	@pollid int
AS
BEGIN
SET NOCOUNT ON

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	delete from poll where subdomain=@subdomain and id=@pollid
	-- no need to delete from poll_answers, there is foreign key there
	delete from pageBlocks where tagRelatedId=@pollid and tagType='Poll'
	return 1
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[poll_add_answer]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[poll_add_answer] 
	@un varchar(50),
	@subdomain varchar(50),
	@answer nvarchar(250),
	@pollid int
AS
BEGIN

if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin


------------------
INSERT INTO [poll_answers]
           ([pollid]
           ,[answer]
           ,[votes]
)
     VALUES
           (@pollid
           ,@answer
           ,0
)
           return 1
------------------
end



END
GO
/****** Object:  StoredProcedure [dbo].[MoveTag]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MoveTag]

@un varchar(50), 
@subdomain varchar(50), 
@pageId int,
@tagId int,
@currentPosition int,
@way varchar(10)
As
declare @prevPositiontagid int, @prevtagid int, @nextPositiontagid int, @nexttagid int, @success bit


	SET NOCOUNT ON;
	
	if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
	begin
	
	if exists (select id from pages where subdomain=@subdomain and id=@pageId)
	begin
	
	
		
		if @way='up'
		begin
		set @prevPositiontagid = isnull((select top 1 positionInPage from pageBlocks where pageId=@pageId and positionInPage<@currentPosition order by positionInPage desc),0);
		set @prevtagid = isnull((select top 1 id from pageBlocks where pageId=@pageId and positionInPage<@currentPosition order by positionInPage desc),@tagId);
		update pageBlocks set positionInPage=@prevPositiontagid where pageId=@pageId and id=@tagId;
		update pageBlocks set positionInPage=@currentPosition where pageId=@pageId and id=@prevtagid;
		set @success=1
		end
		
		if @way='down'
		begin
		set @nextPositiontagid = isnull((select top 1 positionInPage from pageBlocks where pageId=@pageId and positionInPage>@currentPosition order by positionInPage asc),0);
		set @nexttagid = isnull((select top 1 id from pageBlocks where pageId=@pageId and positionInPage>@currentPosition order by positionInPage asc),@tagId);
		update pageBlocks set positionInPage=@nextPositiontagid where pageId=@pageId and id=@tagId;
		update pageBlocks set positionInPage=@currentPosition where pageId=@pageId and id=@nexttagid;
		set @success=1
		end

    end
	else
	begin
	set @success=0
	end
	
		
	end
	else
	begin
		set @success =0
	end
		
    return @success
GO
/****** Object:  StoredProcedure [dbo].[GetTagById]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTagById] 
	
	@subdomain varchar(50),
	@pageid int,
	@tagid int
	
AS
BEGIN


if exists(select subdomain from pages where subdomain=@subdomain)
begin
	if exists (select pageId from pageBlocks where pageId=@pageid and id=@tagid)
	begin
	select * from pageBlocks where id=@tagid
	end
end

	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteRSSfeed]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteRSSfeed] 

	@un varchar(50),
	@subdomain varchar(50),
	@feedid int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	delete from rss_providers where subdomain=@subdomain and id=@feedid
	delete from pageBlocks where tagRelatedId=@feedid and tagType='RssFeed'
	return 1
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteItems]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteItems] 

	@un varchar(50),
	@subdomain varchar(50),
	@pageid int,
	@tagids varchar(5000)
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	delete from pageBlocks where pageId=@pageid and id IN (Select Value from [dbo].[SplitME]( ',', @tagids ))
	return 1

end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteItem]    Script Date: 07/06/2009 10:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteItem] 

	@un varchar(50),
	@subdomain varchar(50),
	@pageid int,
	@tagid int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	delete from pageBlocks where pageId=@pageid and id=@tagid
	return 1

end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteAsnip]    Script Date: 07/06/2009 10:10:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteAsnip] 

	@un varchar(50),
	@subdomain varchar(50),
	@id int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	delete from codeSnippest where subdomain=@subdomain and id=@id
--delete form existing pages
	delete from pageBlocks where tagRelatedId=@id and tagType='CodeSnippet'
	return 1
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteAfile]    Script Date: 07/06/2009 10:10:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteAfile] 

	@un varchar(50),
	@id int
AS
BEGIN
if exists(select id from [PHN_fileStore]..[fileStore] where id=@id and un=@un)
begin
delete from [PHN_fileStore]..[fileStore] where [id]=@id and [un]=@un
delete from pageBlocks where tagType='File' and tagRelatedId=@id
return 1
end
else
begin
return 0
end
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteAdvert]    Script Date: 07/06/2009 10:10:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteAdvert] 

	@un varchar(50),
	@subdomain varchar(50),
	@id int
AS
BEGIN

-- check if user owns the subdomain
if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

	delete from advertisers where subdomain=@subdomain and id=@id
	delete from pageBlocks where tagRelatedId=@id and tagType='Advert'
	return 1
end
else
begin
	return 0
end
	
END
GO
/****** Object:  StoredProcedure [dbo].[CopyAPage]    Script Date: 07/06/2009 10:10:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyAPage] 

	@un varchar(50),
	@subdomain varchar(50),
	@pageid int,
	@limit int
AS
BEGIN


if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
begin

if (select COUNT(*) from pages where subdomain=@subdomain)<@limit --sayfa sayisi limitten az ise
begin
----------------------------------------------------------
	 --copy page
INSERT INTO [pages]
           ([title]
           ,[description]
           ,[keywords]
           ,[other_meta]
           ,[fontSize]
           ,[pageBgColor]
           ,[pageFontColor]
           ,[pageFolder]
           ,[isStartPage]
           ,[subdomain]
           ,[hits]
           ,[lastVisitor])
	   SELECT [title]
           ,[description]
           ,[keywords]
           ,[other_meta]
           ,[fontSize]
           ,[pageBgColor]
           ,[pageFontColor]
           ,[pageFolder]
           ,0
           ,[subdomain]
           ,0
           ,[lastVisitor] FROM pages
	   WHERE id = @pageid
	   
	   --copy pageblocks
		INSERT INTO [pageBlocks]
           ([pageId]
           ,[tagType]
           ,[tagRelatedId]
           ,[tagText]
           ,[tagLinkTitle]
           ,[tagLinkUrl]
           ,[tagStyleClass]
           ,[tagFontColor]
           ,[tagFontDecoration]
           ,[tagFontWeight]
           ,[tagFontSize]
           ,[tagBgColor]
           ,[tagBorderSize]
           ,[tagBorderColor]
           ,[tagAlignment]
           ,[positionInPage])
           SELECT SCOPE_IDENTITY()
           ,[tagType]
           ,[tagRelatedId]
           ,[tagText]
           ,[tagLinkTitle]
           ,[tagLinkUrl]
           ,[tagStyleClass]
           ,[tagFontColor]
           ,[tagFontDecoration]
           ,[tagFontWeight]
           ,[tagFontSize]
           ,[tagBgColor]
           ,[tagBorderSize]
           ,[tagBorderColor]
           ,[tagAlignment]
           ,[positionInPage] FROM pageBlocks
		   WHERE pageId = @pageid
           
   
	return 1
-------------------------------------------------------------------------	
	end
	else
	begin
	return 0
	end
end

	
	else
	begin
	return 0
	end
	
	

END
GO
/****** Object:  StoredProcedure [dbo].[BuildPage]    Script Date: 07/06/2009 10:10:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BuildPage] 

	@subdomain varchar(50),
	@pageid int
AS
BEGIN




	if exists (select id from pages where subdomain=@subdomain and id=@pageid)
	begin
	select '200' as PageStatusinHTTP
	select title,[description],keywords,other_meta,fontSize,pageBgColor,pageFontColor,hits from pages where id=@pageid 
	select * from pageBlocks where pageId=@pageid order by positionInPage asc
	--update hits
	update pages set hits=hits+1,lastVisitor=GETUTCDATE() where id=@pageid and subdomain=@subdomain
	end
else
	begin
	select '404' as PageStatusinHTTP
	end



END
GO
/****** Object:  StoredProcedure [dbo].[ADMIN_Illegal_keyword_check]    Script Date: 07/06/2009 10:10:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[ADMIN_Illegal_keyword_check] 
@q nvarchar(50)
AS
BEGIN


SELECT t.pageId ,
(select subdomain from pages where id=t.pageId) as subdomain, 
(select domain from subdomains where subdomain=(select subdomain from pages where id=t.pageId)) as domain,
(select un from subdomains where subdomain=(select subdomain from pages where id=t.pageId)) as un ,
(select lastLogin from users where un=(select un from subdomains where subdomain=(select subdomain from pages where id=t.pageId))) as LastLogon,
(select email from users where un=(select un from subdomains where subdomain=(select subdomain from pages where id=t.pageId))) as email


from (
SELECT pageId FROM pageBlocks
WHERE (tagText LIKE '%'+@q+'%' or tagLinkTitle LIKE '%'+@q+'%' or tagLinkUrl LIKE '%'+@q+'%')
GROUP BY pageId ) t


END
GO
/****** Object:  StoredProcedure [dbo].[AddTag]    Script Date: 07/06/2009 10:10:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddTag]

@pageWidelimit int, @limit int, @un varchar(50), @subdomain varchar(50), @pageId int, @tagType varchar(20),
@tagRelatedId int,  @tagText nvarchar(3000),	@tagLinkTitle nvarchar(255),@tagLinkUrl varchar(255),
@tagStyleClass varchar(20), @tagFontColor varchar(10),@tagFontDecoration varchar(10),
@tagFontWeight varchar(10), @tagFontSize varchar(10), @tagBgColor varchar(10), @tagBorderSize smallint,
@tagBorderColor varchar(10),  @prevPositiontagid int, @tagAlignment varchar(10)

AS
BEGIN
declare @success bit

	SET NOCOUNT ON;
	
	if exists (select subdomain from subdomains where un=@un and subdomain=@subdomain)
	begin
	
	if exists (select id from pages where subdomain=@subdomain and id=@pageId)
	begin
			-- check page wide and item limits
			if (select COUNT(*) from pageBlocks where pageId=@pageId)< @pageWidelimit and
				(select COUNT(*) from pageBlocks where tagType=@tagType and pageId=@pageId) < @limit
			begin
			INSERT INTO [pageBlocks]
           ([pageId],[tagType],[tagRelatedId],[tagText],[tagLinkTitle],[tagLinkUrl],[tagStyleClass]
           ,[tagFontColor],[tagFontDecoration],[tagFontWeight],[tagFontSize],[tagBgColor],[tagBorderSize]
           ,[tagBorderColor],[positionInPage],[tagAlignment])
			VALUES
           (@pageId,@tagType,@tagRelatedId,@tagText,@tagLinkTitle,@tagLinkUrl,@tagStyleClass,
            @tagFontColor,@tagFontDecoration,@tagFontWeight,@tagFontSize,@tagBgColor,@tagBorderSize,
            @tagBorderColor,@prevPositiontagid+1,@tagAlignment )
			update pageBlocks set positionInPage=positionInPage+1 where positionInPage>@prevPositiontagid and  pageId=@pageId and id != SCOPE_IDENTITY()
			set @success=1
			end
			
			else
			begin
			set @success=0
			end
			
	end
	else
	begin
	set @success=0
	end
	
		
	end
	else
	begin
		set @success =0
	end
		
		return @success
	
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[AddAutoBrTag]    Script Date: 07/06/2009 10:10:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddAutoBrTag]

@pageId int, @tagType varchar(20),
@tagText nvarchar(3000)

AS
BEGIN

declare @prevPositiontagid int
set @prevPositiontagid = (select  top 1 positionInPage from pageBlocks where pageid=@pageId order by id desc)


			INSERT INTO [pageBlocks]  ([pageId],[tagType],[tagText],[positionInPage])
			VALUES    (@pageId,@tagType,@tagText,@prevPositiontagid+1)


	
	
END
GO
/****** Object:  ForeignKey [FK_forum_posts_forum_topics]    Script Date: 07/06/2009 10:10:39 ******/
ALTER TABLE [dbo].[forum_posts]  WITH CHECK ADD  CONSTRAINT [FK_forum_posts_forum_topics] FOREIGN KEY([topicId])
REFERENCES [dbo].[forum_topics] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[forum_posts] CHECK CONSTRAINT [FK_forum_posts_forum_topics]
GO
/****** Object:  ForeignKey [FK_users_un]    Script Date: 07/06/2009 10:10:39 ******/
ALTER TABLE [dbo].[subdomains]  WITH CHECK ADD  CONSTRAINT [FK_users_un] FOREIGN KEY([un])
REFERENCES [dbo].[users] ([un])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[subdomains] CHECK CONSTRAINT [FK_users_un]
GO
/****** Object:  ForeignKey [FK_subdomains_subdomain_for_styles]    Script Date: 07/06/2009 10:10:39 ******/
ALTER TABLE [dbo].[styles]  WITH CHECK ADD  CONSTRAINT [FK_subdomains_subdomain_for_styles] FOREIGN KEY([subdomain])
REFERENCES [dbo].[subdomains] ([subdomain])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[styles] CHECK CONSTRAINT [FK_subdomains_subdomain_for_styles]
GO
/****** Object:  ForeignKey [FK_subdomains_subdomain_for_rotatorLinks]    Script Date: 07/06/2009 10:10:39 ******/
ALTER TABLE [dbo].[rotatorLinks]  WITH CHECK ADD  CONSTRAINT [FK_subdomains_subdomain_for_rotatorLinks] FOREIGN KEY([subdomain])
REFERENCES [dbo].[subdomains] ([subdomain])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[rotatorLinks] CHECK CONSTRAINT [FK_subdomains_subdomain_for_rotatorLinks]
GO
/****** Object:  ForeignKey [FK_subdomains_subdomain_for_guestbook]    Script Date: 07/06/2009 10:10:39 ******/
ALTER TABLE [dbo].[guestbook]  WITH CHECK ADD  CONSTRAINT [FK_subdomains_subdomain_for_guestbook] FOREIGN KEY([subdomain])
REFERENCES [dbo].[subdomains] ([subdomain])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[guestbook] CHECK CONSTRAINT [FK_subdomains_subdomain_for_guestbook]
GO
/****** Object:  ForeignKey [FK_subdomains_subdomain_for_poll]    Script Date: 07/06/2009 10:10:39 ******/
ALTER TABLE [dbo].[poll]  WITH CHECK ADD  CONSTRAINT [FK_subdomains_subdomain_for_poll] FOREIGN KEY([subdomain])
REFERENCES [dbo].[subdomains] ([subdomain])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[poll] CHECK CONSTRAINT [FK_subdomains_subdomain_for_poll]
GO
/****** Object:  ForeignKey [FK_subdomains_subdomain_for_pages]    Script Date: 07/06/2009 10:10:39 ******/
ALTER TABLE [dbo].[pages]  WITH CHECK ADD  CONSTRAINT [FK_subdomains_subdomain_for_pages] FOREIGN KEY([subdomain])
REFERENCES [dbo].[subdomains] ([subdomain])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[pages] CHECK CONSTRAINT [FK_subdomains_subdomain_for_pages]
GO
/****** Object:  ForeignKey [FK_subdomains_subdomain_for_codeSnippest]    Script Date: 07/06/2009 10:10:40 ******/
ALTER TABLE [dbo].[codeSnippest]  WITH CHECK ADD  CONSTRAINT [FK_subdomains_subdomain_for_codeSnippest] FOREIGN KEY([subdomain])
REFERENCES [dbo].[subdomains] ([subdomain])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[codeSnippest] CHECK CONSTRAINT [FK_subdomains_subdomain_for_codeSnippest]
GO
/****** Object:  ForeignKey [FK_subdomains_subdomain_for_advertisers]    Script Date: 07/06/2009 10:10:40 ******/
ALTER TABLE [dbo].[advertisers]  WITH CHECK ADD  CONSTRAINT [FK_subdomains_subdomain_for_advertisers] FOREIGN KEY([subdomain])
REFERENCES [dbo].[subdomains] ([subdomain])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[advertisers] CHECK CONSTRAINT [FK_subdomains_subdomain_for_advertisers]
GO
/****** Object:  ForeignKey [FK_pageBlocks_pages]    Script Date: 07/06/2009 10:10:40 ******/
ALTER TABLE [dbo].[pageBlocks]  WITH CHECK ADD  CONSTRAINT [FK_pageBlocks_pages] FOREIGN KEY([pageId])
REFERENCES [dbo].[pages] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[pageBlocks] CHECK CONSTRAINT [FK_pageBlocks_pages]
GO
/****** Object:  ForeignKey [FK_pollid_fk]    Script Date: 07/06/2009 10:10:40 ******/
ALTER TABLE [dbo].[poll_answers]  WITH CHECK ADD  CONSTRAINT [FK_pollid_fk] FOREIGN KEY([pollid])
REFERENCES [dbo].[poll] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[poll_answers] CHECK CONSTRAINT [FK_pollid_fk]
GO
