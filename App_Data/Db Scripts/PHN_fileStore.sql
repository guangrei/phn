/****** Object:  Table [dbo].[fileStore]    Script Date: 07/06/2009 10:11:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fileStore](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[un] [varchar](50) NOT NULL,
	[fileName] [nvarchar](50) NOT NULL,
	[fileSize] [int] NOT NULL,
	[fileType] [varchar](50) NOT NULL,
	[fileData] [varbinary](max) NOT NULL,
	[uploadTime] [datetime] NOT NULL,
 CONSTRAINT [PK_fileStore] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
