﻿<%@ WebHandler Language="C#" Class="DisplayImg" %>

using System;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;


public class DisplayImg : IHttpHandler
{

        public void ProcessRequest (HttpContext context)
        {
            // hot link protection 
            // see PageHandler.ashx
            // needs to be developed.
            bool HttpReferrerChecked = false;
            // some mobiles does not http ref so if there isint any ref set checked to true
            if (context.Request.UrlReferrer == null || context.Request.UrlReferrer.Host.Length == 0)
            {
                HttpReferrerChecked = true;
            }
            else
            {
               string HttpReferrer = context.Request.UrlReferrer.Host.ToString().ToLower();

                
               foreach (SiteConfigurationDomain domain in SiteConfiguration.GetConfig().AvailableDomains)
                   if (HttpReferrer.IndexOf(domain.Name.ToString().ToLower()) > -1)
                   //if (domain.Name.ToString().ToLower() == HttpReferrer)
                   {
                       HttpReferrerChecked = true;
                       break;
                   }               

            }
            
            if (context.Cache["HotLinkProtection"] == null || HttpReferrerChecked == false)
            {
                //context.Response.Write("Hot linking is not allowed! http://phn.me");
                context.Response.Redirect("http://phn.me/images/logo.png");
            }
            else
            {
                //img id
                int theID = Functions.ReturnNumeric(context.Request.QueryString["id"]);               
                
                if (theID > 0)
                {
                    context.Response.StatusCode = 200;
                    context.Response.Cache.SetExpires(DateTime.Now.AddMinutes(10));
                    context.Response.Cache.SetCacheability(HttpCacheability.Public);
                    context.Response.AddHeader("Cache-Control", "no-transform");
                    Stream strm = DisplayImage(theID);
                    
                    //if thumbnail
                    if (context.Request.QueryString["m"] == "t")
                    {
                        System.Drawing.Image Resized = System.Drawing.Image.FromStream(strm, true);
                        strm = new MemoryStream(ResizeImage.Resize(Resized, 30, 30));                        
                    }
                    
                    if (strm != null)
                    {
                        // mime type
                        context.Response.ContentType = Functions.ReturnFileMimeType(theID);

                        byte[] buffer = new byte[2048];
                        int byteSeq = strm.Read(buffer, 0, 2048);

                        while (byteSeq > 0)
                        {
                            context.Response.OutputStream.Write(buffer, 0, byteSeq);
                            byteSeq = strm.Read(buffer, 0, 2048);
                        }
                    }
                }
            }
        }
   
public Stream DisplayImage(int theID)
{
       string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

       using (SqlConnection conn = new SqlConnection())
       {
           conn.ConnectionString = connString;
           conn.Open();

           using (SqlCommand cmd = new SqlCommand("GetFile", conn))
           {
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.AddWithValue("@ID", theID);

               object theImg = cmd.ExecuteScalar();
               try
               {
                   return new MemoryStream((byte[])theImg);
               }
               catch
               {
                   return null;
               }
           }

       }
}

    
public bool IsReusable
{ 
get
{ 
return true;
}
}
}