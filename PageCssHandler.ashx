﻿<%@ WebHandler Language="C#" Class="PageCssHandler" %>

using System;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text;

public class PageCssHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/css";
        context.Response.Cache.SetExpires(DateTime.Now.AddMinutes(5));
        context.Response.Cache.SetCacheability(HttpCacheability.Public);
        context.Response.ContentEncoding = System.Text.Encoding.UTF8;

        Functions f = new Functions();
        string subd = GetSubdomainParam();
        //remove .css
        if(String.IsNullOrEmpty(subd)==false){
            try
            {
                subd = subd.Remove(subd.Length - 4, 4);
            }
            catch
            {
                subd = string.Empty;
            }
        }
        if (Functions.IsAlphaNumericForSubdomain(subd) == false)
        {
            subd = string.Empty;
        }



        context.Response.Write(f.GetCurrentStyle(subd).ToString());
         
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
    
public string GetSubdomainParam(){
        if (HttpContext.Current.Request.PathInfo.Length == 0) 
            {
            return "";
                
            }
               
        else 
        {
            return HttpContext.Current.Request.PathInfo.Substring(1);
        }

        }

}