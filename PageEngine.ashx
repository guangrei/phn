﻿<%@ WebHandler Language="C#" Class="PageEngine"%>
using System;
using System.Web;
using System.Web.SessionState;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public class PageEngine : IHttpHandler, IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {

        // check cache for file hotlinking protection
        // 20 seconds = 20000 miliseconds
        // see FileHandler.ashx
        if (context.Cache["HotLinkProtection"] == null)
        {
            context.Cache.Insert(
            "HotLinkProtection",
            "1",
            null,
            DateTime.Now.AddMinutes(6), System.Web.Caching.Cache.NoSlidingExpiration);
        }
        
        // http headers starts-------------------------------------------
        context.Response.ContentType = "text/html";
        context.Response.Cache.SetExpires(DateTime.Now.AddMinutes(5));
        context.Response.Cache.SetCacheability(HttpCacheability.Public);
        context.Response.AddHeader("Cache-Control", "no-transform");
        context.Response.ContentEncoding = System.Text.Encoding.UTF8;
        // http headers ends--------------------------------------------

        myPageBuilder build = new myPageBuilder();
        // get queryString values starts--------------------------------
        string subdomainname = Functions.LimitString(context.Request.QueryString["subdomain"], 50);
        string domainname = Functions.LimitString(context.Request.QueryString["domain"], 50);
        // check against attacks
        if (Functions.IsAlphaNumericForSubdomain(subdomainname) == false || String.IsNullOrEmpty(subdomainname)==true){
            subdomainname="wwww";
        }
        if (Functions.IsAlphaNumeric(domainname) == false || String.IsNullOrEmpty(domainname)==true){
            subdomainname = SiteConfiguration.GetConfig().defaultSiteDomain;
        }
                
            
        
        string defaultpage = Functions.LimitString(context.Request.QueryString["defaultpage"], 50);
        int pageid = Functions.ReturnNumeric(context.Request.QueryString["pageid"]);
        if (pageid == 0 && defaultpage!="yes")
        {
            // if page id is 0 means page id is not numeric give 404
            //context.Response.Redirect("http://" + domainname + "/404.aspx");
            context.Response.StatusCode = 404;
            context.Server.Transfer("~/404.aspx");
        }
        else
        {

            // check if subdomain exists start
            if (build.CheckSubdomain(subdomainname, domainname) == false)
            {
                // if sub domain does not exists
                //context.Response.Redirect("http://"+domainname+"/404.aspx");
                context.Response.StatusCode = 404;
                context.Server.Transfer("~/404.aspx");
            }
            else
            {
                if (defaultpage == "yes")
                {
                    pageid = build.GetDefaultPage(subdomainname);
                }

                DataSet ds = build.BuildPage(subdomainname,pageid);
                
                if (ds.Tables[0].Rows[0]["PageStatusinHTTP"].ToString() == "404")
                {
                    context.Response.StatusCode = 404;
                    context.Server.Transfer("~/404.aspx");
                }
                else
                {
                    //get and add hit count of page
                    string myHits = ds.Tables[1].Rows[0]["hits"].ToString();
                    
                    context.Response.Write(@"<?xml version=""1.0"" encoding=""utf-8""?><!DOCTYPE html PUBLIC ""-//WAPFORUM//DTD XHTML Mobile 1.0//EN"" ""http://www.wapforum.org/DTD/xhtml-mobile10.dtd""><html xmlns=""http://www.w3.org/1999/xhtml""><head><meta http-equiv=""content-type"" content=""application/vnd.wap.xhtml+xml; charset=utf-8"" /><meta name=""Robots"" content=""all, index, follow""/>");
                    context.Response.Write("<meta name=\"description\" content=\"" + ds.Tables[1].Rows[0]["description"].ToString() + "\" />");
                    context.Response.Write("<meta name=\"keywords\" content=\"" + ds.Tables[1].Rows[0]["keywords"].ToString() + "\" />");
                    context.Response.Write("<title>" + ds.Tables[1].Rows[0]["title"].ToString() + "</title>");
                    context.Response.Write("<link href=\"/PageCssHandler.ashx/" + subdomainname + ".css\" rel=\"stylesheet\" type=\"text/css\"></link>");
                    context.Response.Write(context.Server.HtmlDecode(ds.Tables[1].Rows[0]["other_meta"].ToString())); //user meta tags
                    context.Response.Write("</head>");
                    context.Response.Write("<body style=\"background-color:" + ds.Tables[1].Rows[0]["pageBgColor"].ToString() + "; font-size:" + ds.Tables[1].Rows[0]["fontSize"].ToString() + "; color:" + ds.Tables[1].Rows[0]["pageFontColor"].ToString() + "\">");
                    if (build.IsMobile()==false)
                    {
                        //insert phn.me toolbar
                        context.Response.Write(@"<div style=""background: #ffffff url('http://phn.me/images/logobg.png'); margin-bottom:4px; border-bottom:solid 1px #969696; height:26px;"">
                            <table cellpadding=""0"" cellspacing=""0"" style=""width: 100%; height: 25px; border-width: 0px;"">
	                            <tr>
		                            <td style=""text-align: left;"">
		                            <a href=""http://phn.me"" title=""Go to PHN.ME Home""><img alt=""Phn.Me"" height=""25"" src=""http://phn.me/images/logo.png"" width=""115""/></a></td>
		                            <td style=""text-align: right;"">
                                    <a style=""text-decoration: none; font-size: medium;"" title=""Login and edit your site"" href=""http://phn.me/manage/""><img alt=""Register"" src=""http://phn.me/images/weblogin.png""/> Login</a>&nbsp;&nbsp;&nbsp;<a style=""text-decoration: none; font-size: medium;"" title=""Create your site in seconds!"" href=""http://phn.me/manage/register.aspx""><img alt=""Register"" src=""http://phn.me/images/webnewuser.png""/> Create site</a>
		                            </td>
	                            </tr>
                            </table>
                            </div>");
                    }
                    context.Response.Write("<div class=\"wrapper\">");
                    
                    // check if there is content
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        // get page content and loop
                        foreach (DataRow row in ds.Tables[2].Rows)
                        {
                            string TagTypeToShow = row["tagType"].ToString();
                            int tagRelatedId = Functions.ReturnNumeric(row["tagRelatedId"].ToString());
                            string tagText = row["tagText"].ToString();
                            string tagLinkTitle = row["tagLinkTitle"].ToString();
                            string tagLinkUrl = row["tagLinkUrl"].ToString();
                            string tagStyleClass = row["tagStyleClass"].ToString();
                            string tagFontColor = row["tagFontColor"].ToString();
                            string tagFontDecoration = row["tagFontDecoration"].ToString();
                            string tagFontWeight = row["tagFontWeight"].ToString();
                            string tagFontSize = row["tagFontSize"].ToString();
                            string tagBgColor = row["tagBgColor"].ToString();
                            string tagBorderSize = row["tagBorderSize"].ToString();
                            string tagBorderColor = row["tagBorderColor"].ToString();
                            string tagAlignment = row["tagAlignment"].ToString();
                            string builtStyle = "font-weight:" + tagFontWeight + "; font-size: " + tagFontSize + "; text-align:" + tagAlignment + " ; color: " + tagFontColor + "; text-decoration: " + tagFontDecoration + "; border: " + tagBorderSize + "px solid " + tagBorderColor + "; background-color: " + tagBgColor + "";


                            switch (TagTypeToShow)
                            {
                                case "BR":
                                    context.Response.Write("<br/>");
                                    break;
                                case "HR":
                                    context.Response.Write("<hr/>");
                                    break;
                                case "ULo":
                                    context.Response.Write("<ul>");
                                    break;
                                case "ULc":
                                    context.Response.Write("</ul>");
                                    break;
                                case "LIo":
                                    context.Response.Write("<li>");
                                    break;
                                case "LIc":
                                    context.Response.Write("</li>");
                                    break;
                                case "DIVo":
                                    context.Response.Write(build.TagBuildTagDiv(tagStyleClass, builtStyle));
                                    break;
                                case "DIVc":
                                    context.Response.Write("</div>");
                                    break;
                                case "ParagraphO":
                                    context.Response.Write(build.TagBuildTagParagraph(tagStyleClass, builtStyle));
                                    break;
                                case "ParagraphC":
                                    context.Response.Write("</p>");
                                    break;
                                case "VisitorIP":
                                    context.Response.Write(build.TagBuildTagVisitorIP(tagStyleClass, builtStyle, context.Request.ServerVariables["REMOTE_ADDR"].ToString()));
                                    break;
                                case "VisitorCountry":
                                    
                                    string uIp = context.Request.ServerVariables["REMOTE_ADDR"].ToString();

                                    
                                    //if (context.Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains("mini"))
                                    //{
                                    //    string forwardedip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                    //    if (forwardedip != null)
                                    //    {
                                    //        string[] forwardedipArray = forwardedip.Split(new char[] { ',' });
                                    //        //last ip in the array is the real ip
                                    //        //http://dev.opera.com/articles/view/opera-mini-request-headers/
                                    //        string extIp = forwardedipArray[forwardedipArray.Length - 1].ToString().Trim();
                                    //        if (Functions.IsValidIP(extIp)==true && Functions.IsIpLocalIP(extIp) == false)
                                    //        {
                                    //            uIp = extIp;
                                    //        }
                                    //    }
                                    //
                                    //}
                                    context.Response.Write(build.TagBuildTagVisitorCountry(tagStyleClass, builtStyle, uIp));
                                    break;
                                case "SiteMap":
                                    context.Response.Write(build.TagBuildTagSitemap(subdomainname));
                                    break;
                                case "Text":
                                    context.Response.Write(build.TagBuildTagText(tagStyleClass, builtStyle, tagText));
                                    break;
                                case "bText":
                                    context.Response.Write(build.TagBuildTagBText(tagStyleClass, builtStyle, tagText));
                                    break;
                                case "ServerTime":
                                    context.Response.Write(build.TagBuildTagServerTime(tagStyleClass, builtStyle, Functions.ReturnFloat(tagText)));
                                    break;
                                case "TotalHits":
                                    context.Response.Write(build.TagBuildTagTotalHits(subdomainname));
                                    break;
                                case "ServerDate":
                                    context.Response.Write(build.TagBuildTagServerDate(tagStyleClass, builtStyle, Functions.ReturnFloat(tagText)));
                                    break;
                                case "TextLink":
                                case "iTextLink":
                                    context.Response.Write(build.TagBuildTagTextLink(tagStyleClass, builtStyle, tagLinkUrl, tagLinkTitle));
                                    break;
                                case "Image":
                                    context.Response.Write(build.TagBuildTagImage(tagStyleClass, builtStyle, tagLinkUrl));
                                    break;
                                case "ImageLink":
                                    context.Response.Write(build.TagBuildTagImageLink(tagStyleClass, builtStyle, tagLinkTitle, tagLinkUrl));
                                    break;
                                case "HtmlCode":
                                    context.Response.Write(context.Server.HtmlDecode(tagText));
                                    break;
                                case "CodeSnippet":
                                    context.Response.Write(context.Server.HtmlDecode(build.TagBuildTagCodeSnippet(subdomainname, tagRelatedId)));
                                    break;
                                case "RssFeed":
                                    string RssUrl = build.TagBuildTagRssFeedA(subdomainname, tagRelatedId);
                                    //tagText holds number of articles to pull from rss feed
                                    if (String.IsNullOrEmpty(RssUrl)==false)
                                    context.Response.Write(build.TagBuildTagRssFeedB(RssUrl, tagText));
                                    break;
                                case "File":
                                    context.Response.Write(build.TagBuildTagFile(subdomainname, tagRelatedId, tagText, tagLinkTitle));
                                    break;
                                case "LinkRotator":
                                    context.Response.Write(build.TagBuildTagLinkRotator(subdomainname));
                                    break;
                                case "Advert":
                                    string returnedAd = build.TagBuildTagAdvert(subdomainname, tagRelatedId);
                                    if (String.IsNullOrEmpty(returnedAd))
                                        context.Response.Write(DefaultAdverts.BuzzNonAdult());
                                    else
                                        context.Response.Write(returnedAd);
                                    break;
                                case "OnlineCount":
                                    context.Response.Write(build.GetVisitorcount(subdomainname));
                                    break;
                                case "OnlineList":
                                    DataTable dtss = new DataTable();
                                    dtss = build.TagBuildTagVisitorsList(subdomainname).Tables[0];
                                    foreach (DataRow dr in dtss.Rows)
                                    {
                                        context.Response.Output.Write(Functions.GetCountryFlag(dr["visitorIp"].ToString()) + " <b><a href=\"page{2}.aspx\">Visitor-{0}</a></b><br/><small>{1}</small><br/>", dr["id"], dr["visitorBrowser"], dr["location"]);
                                    }
                                    break;
                                case "ContactAdminForm":
                                    context.Response.Write(build.TagBuildTagContactAdminForm(subdomainname, tagText, tagLinkTitle, tagLinkUrl));
                                    break;
                                    
                                // guestbook start
                                case "Guestbook":
                                    int perPage= 6;
                                    int page = Functions.ReturnNumeric(context.Request["p"]);
                                    DataTable PagingTable = new DataTable("paging");
                                    PagingTable = build.TagBuildGuestbook(subdomainname, page, perPage).Tables[0];
                                    int currentPage = Functions.ReturnNumeric(PagingTable.Rows[0]["CurrentPage"].ToString());
                                    int totalPages = Functions.ReturnNumeric(PagingTable.Rows[0]["TotalPages"].ToString());
                                    int totalRecords = Functions.ReturnNumeric(PagingTable.Rows[0]["TotalRows"].ToString());
                                    DataTable MessagesTable = new DataTable("messages");
                                    MessagesTable=build.TagBuildGuestbook(subdomainname, page, perPage).Tables[1];
                                    
                                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                    sb.Append("<ul style=\"list-style-type:none; padding:0; margin:0;\">");

                                    foreach (DataRow dr in MessagesTable.Rows)
                                    {
                                        sb.AppendFormat("<li style=\"margin-bottom:5px;\"><b>{1}</b> " + Functions.GetCountryFlag(dr["posterIp"].ToString()) + " <small>{4:dd MMM yyyy}</small><br/>{5}</li>", dr["id"], dr["poster"], dr["posterIp"], dr["posterBrowser"], dr["postDate"], build.Smile(dr["post"].ToString()));
                                    }
                                    //pages
                                    if (totalPages > 1)
                                    {
                                        sb.Append("<li style=\"margin-bottom:5px;\"><div style=\"text-align: center;\">");
                                        sb.AppendFormat("{0}/{1}<br/>",currentPage,totalPages);
                                    }
                                    
                                    //first page
                                    if (currentPage > 1)
                                    {
                                        sb.AppendFormat("<a href=\"page{0}.aspx?p=1\"><img src=\"/images/rewind.png\" alt=\"First page\"/></a> ", pageid);
                                    }
                                    //next page
                                    if (currentPage < totalPages && totalPages>1)
                                    {
                                        sb.AppendFormat("<a href=\"page{0}.aspx?p={1}\"><img src=\"/images/forward.png\" alt=\"Next page\"/></a> ", pageid, currentPage + 1);
                                    }
                                    //prev page
                                    if (currentPage>1)
                                    {
                                        sb.AppendFormat("<a href=\"page{0}.aspx?p={1}\"><img src=\"/images/back.png\" alt=\"Previous page\"/></a> ", pageid, currentPage - 1);
                                    }
                                    //last page
                                    if (totalPages > 1 && currentPage<totalPages)
                                    {
                                        sb.AppendFormat("<a href=\"page{0}.aspx?p={1}\"><img src=\"/images/fast_forward.png\" alt=\"Last page\"/></a> ", pageid, totalPages);
                                    }
                                    //finish pages
                                    if (totalPages > 1)
                                    {
                                        sb.Append("</div></li>");
                                    }

                                    //error msg
                                    if (context.Request.QueryString["err"] == "1")
                                    {
                                        sb.Append("<li style=\"margin-bottom:5px;\"><a id=\"res\"></a><b>There was an error. Please check your entry and try again later</b></li>");
                                    }
                                    // ad new form
                                    sb.Append("<li style=\"margin-bottom:5px;\"><form method=\"post\" action=\"/postToGuestBook.aspx\"><p><input type=\"hidden\" name=\"returnUrl\" value=\"page" + pageid + ".aspx\"/>Nickname:<br/><input type=\"text\" name=\"nick\"/><br/>Message:<br/><textarea name=\"message\" rows=\"3\" cols=\"20\"></textarea><br/><a href=\"/captchaImage.aspx/dummy.jpg\"><img alt=\"Securityimage\" src=\"/captchaImage.aspx/dummy.jpg\"/></a> : <input name=\"captcha\" type=\"text\" size=\"4\"/><br/><input type=\"submit\" value=\"Post\"/></p></form></li>");

                                    sb.Append("</ul>Smiles: :) , :D , :@ , :H , :( , :c , :o , ;) , :p");
                                    context.Response.Write(sb.ToString());
                                    break;
                                    //guestbook end
                                    
                                    
                                    
                                    case "Poll":
                                    string viewVotes = context.Request.QueryString["v"];
                                    context.Response.Write(build.TagBuildTagPoll(tagRelatedId, viewVotes, pageid));
                                    break;
                                    
                                    case "Chat":
                                    Functions f = new Functions();
                                    string u = context.Request["u"];
                                    if (String.IsNullOrEmpty(u))
                                    {
                                        u = "User-" + CaptchaDLL.CaptchaImage.GenerateRandomCode(CaptchaDLL.CaptchaType.AlphaNumeric, 4);
                                    }
                                    u = context.Server.HtmlEncode(u);
                                    u = f.ReplaceLineBreakForTitles(u);
                                    u = Functions.LimitString(u, 50);
                                    u = f.SqlProtect(u);
                                    
                                    context.Response.Write("<script type=\"text/javascript\">");
                                    context.Response.Write("//<![CDATA[");
                                    context.Response.Write(Environment.NewLine);
                                    context.Response.Write("function ChangeNick(u){var r=prompt(\"Enter a new nick\", u);if (typeof r === \'string\'){document.forms[0].u.value=r;}}");
                                    context.Response.Write("function timedRefresh(){setTimeout(\"location.href=\'page"+ pageid +".aspx?u="+ u +"&rnd="+DateTime.Now.GetHashCode().ToString()+"\';\",30000);} window.onload=timedRefresh;");
                                    context.Response.Write(Environment.NewLine);
                                    context.Response.Write("//]]>");
                                    context.Response.Write("</script>");
                                    
                                    
                                    string viewBrowser = context.Request.QueryString["b"];
                                    string m = context.Request["m"];
                                    m = context.Server.HtmlEncode(m);
                                    m = f.ReplaceLineBreak(m);
                                    m = Functions.LimitString(m, 500);
                                    m = f.SqlProtect(m);

                                    
                                    string unBrowser = context.Request.ServerVariables["HTTP_USER_AGENT"];
                                    unBrowser = context.Server.HtmlEncode(unBrowser);
                                    unBrowser = f.SqlProtect(unBrowser);
                                    if (String.IsNullOrEmpty(unBrowser))
                                    {
                                        unBrowser = "Unknown browser";
                                    }
                                    

                                    string Ip = context.Request.ServerVariables["REMOTE_ADDR"];
                                    if (unBrowser.ToLower().Contains("mini"))
                                    {
                                        string forwardedip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                        if (forwardedip != null)
                                        {
                                            string[] forwardedipArray = forwardedip.Split(new char[] { ',' });
                                            //last ip in the array is the real ip
                                            //http://dev.opera.com/articles/view/opera-mini-request-headers/
                                            string extIp = forwardedipArray[forwardedipArray.Length - 1].ToString().Trim();
                                            if (Functions.IsValidIP(extIp)==true && Functions.IsIpLocalIP(extIp)==false)
                                            {
                                                Ip = extIp;
                                            }
                                        }

                                    }
                                    
                                               
                                    DataTable ChatTable = new DataTable("chatTable");
                                    ChatTable = build.TagBuildTagChatMessageList(pageid).Tables[0];
                                    DataTable UsersTable = new DataTable("usersTable");
                                    UsersTable = build.TagBuildTagChatMessageList(pageid).Tables[1];
                                    string postt = context.Request["p"];
                                    if (postt == "p")
                                    {
                                        //insert row todata table then post to db
                                        DataRow newMsg;
                                        newMsg = ChatTable.NewRow();
                                        //newMsg["pageid"] = pageid;
                                        newMsg["un"] = u;
                                        newMsg["m"] = m;
                                        newMsg["mTime"] = DateTime.UtcNow;
                                        newMsg["unIp"] = Ip;
                                        newMsg["unBrowser"] = unBrowser;
                                        ChatTable.Rows.InsertAt(newMsg,0);
                                        //add to db
                                        build.TagBuildTagChatMessageInsert(pageid, u, m, Ip, unBrowser);
                                    }
                                    System.Text.StringBuilder sc = new System.Text.StringBuilder();
                                    sc.Append("<ul style=\"list-style-type:none; padding:0; margin:0;\">");
                                    sc.Append("<li style=\"list-style-type:none;\"><a id=\"top\"></a><div style=\"text-align:center;\">[2]<a accesskey=\"2\" href=\"page" + pageid + ".aspx?u=" + u + "&amp;rnd=" + DateTime.Now.GetHashCode().ToString() + "\"><img src=\"/images/refresh.gif\" alt=\"refresh\"/></a> [3]<a accesskey=\"3\" href=\"#post\"><img src=\"/images/write.gif\" alt=\"write\"/></a></div></li>");

                                    //list messages
                                    string brow ;
                                    foreach (DataRow cdr in ChatTable.Rows)
                                    {
                                        //show browser?
                                        if (viewBrowser == "b") { brow = "<small>"+cdr["unBrowser"].ToString() +"</small><br/>"; } else { brow = string.Empty; }
                                        sc.AppendFormat("<li style=\"list-style-type:none; border:0px; \"><b>{0}</b> " + Functions.GetCountryFlag(cdr["unIP"].ToString()) + " <small>{2:dd MMM hh:mm}</small><br/>{3}{1}</li>", cdr["un"], build.Smile(cdr["m"].ToString()), cdr["mTime"], brow);
                                    }
                                    sc.Append("<li style=\"list-style-type:none;\"><a id=\"post\"></a><form method=\"post\" action=\"/page" + pageid + ".aspx\"><p><input type=\"hidden\" name=\"p\" value=\"p\"/><input type=\"hidden\" name=\"u\" value=\"" + u + "\"/><input type=\"text\" name=\"m\"/> <input type=\"submit\"/><br/>[4]<a accesskey=\"4\" href=\"#top\"><img src=\"/images/top.gif\" alt=\"top\"/></a> [5]<a accesskey=\"5\" href=\"javascript:ChangeNick('" + u + "')\"><img src=\"/images/changenick.gif\" alt=\"change nick\"/></a> [6]<a accesskey=\"6\" href=\"page" + pageid + ".aspx?u=" + u + "&amp;b=b\"><img src=\"/images/browser.gif\" alt=\"info\"/></a></p></form></li>");
                                    // list users
                                    sc.Append("<li style=\"list-style-type:none;\">");
                                    foreach (DataRow udr in UsersTable.Rows)
                                    {
                                        sc.Append(udr["un"] + " ");    
                                    }
                                    sc.Append("</li>");

                                    sc.Append("</ul>Smiles: :) , :D , :@ , :H , :( , :c , :o , ;) , :p");
                                    
                                    context.Response.Write(sc.ToString());
                                    break;
                                    
                                    
                                    
                                    
                                    
                                    
                            }

                        }
                        }
                    else
                    {
                        // nothin in the page
                        context.Response.Write(build.UnderCunstruction());
                    }

                    
                    //phn.me spesific div start
                    context.Response.Write("<div class=\"phnme\">");
                    //advert
                    Buzzcity b = new Buzzcity();
                    context.Response.Write(b.ShowTextAd("14635"));
                    // powered by to main page
                    if (defaultpage == "yes")
                    {
                        context.Response.Write("<br/><a href=\"http://phn.me\">Powered by Phn.me</a>");
                    }
                    //phn.me spesific div end
                    context.Response.Write("</div>");
                    
                    
                    //finish html tags
                    context.Response.Write("</div></body></html>");

                    
                    // add user to online users
                    build.TagBuildTagUpdateVisitors(subdomainname, domainname, Convert.ToString(pageid));
                    // end add user to onlines

                }

            }
        }
       
    }
 
    
    
    
    
    public bool IsReusable {
        get {
            return false;
        }
    }


}