﻿<%@ WebHandler Language="C#" Class="SiteMapHandler" %>

using System;
using System.Web;



public class SiteMapHandler : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/xml";
        context.Response.Cache.SetExpires(DateTime.Now.AddMinutes(10));
        context.Response.Cache.SetCacheability(HttpCacheability.Public);
        context.Response.ContentEncoding = System.Text.Encoding.UTF8;

        Functions f = new Functions();
        string subd = context.Request["subdomain"];
        string domain = context.Request["domain"];
        if (Functions.IsAlphaNumericForSubdomain(subd) == false)
        {
            subd = string.Empty;
        }

        context.Response.Write(@"<?xml version=""1.0"" encoding=""UTF-8""?><urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:mobile=""http://www.google.com/schemas/sitemap-mobile/1.0"">");
        context.Response.Write(f.SitemapInXml(subd,domain).ToString());
        context.Response.Write("</urlset>");
    }
 
    public bool IsReusable {
        get {
            return true;
        }
    }
    

}