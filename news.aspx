﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" Title="Untitled Page" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
            BindData();
    }
    
    protected void BindData()
    {

        string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
        int id = Functions.ReturnNumeric(Request["id"]);

        //start list
        SqlConnection conn = new SqlConnection(connString);
        conn.Open();
        SqlCommand cmd = new SqlCommand("getAnnouncements", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = string.Empty;

        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;

        DataSet ds = new DataSet();
        da.Fill(ds);

        AnnouncementList.DataSource = ds;
        AnnouncementList.DataBind();
    }
    protected void AnnouncementList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
        this.AnnouncementListPaging.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);

        BindData();
    }
    protected void AnnouncementListEventHandler(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem dataItem = (ListViewDataItem)e.Item;
        DataRowView drv = (DataRowView)dataItem.DataItem;
        Label isPublic = (Label)e.Item.FindControl("isPublic");
        Label msgDate = (Label)e.Item.FindControl("msgDate");
        Panel control = (Panel)e.Item.FindControl("IsGlobalAdminLabel");

        msgDate.Text = String.Format("<b>{0:ddd, dd MMM yy}</b> ", drv["msgDate"]);

        if ((bool)drv["isPublic"] == false)
        {
            
        }
        else
        {
            isPublic.Text = " - <b>PUBLIC</b><br/> ";

        }




    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<div class="title"><a href="/"><img src="/images/home.gif" alt=""/> Home</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Announcements</div>

<asp:ListView ID="AnnouncementList" runat="server" 
        OnItemDataBound="AnnouncementListEventHandler" 
        onpagepropertieschanging="AnnouncementList_PagePropertiesChanging">
<LayoutTemplate>
    <ul>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
        <b> <asp:Label ID="msgDate" runat="server" Text=""></asp:Label></b>
            <asp:Label ID="isPublic" runat="server" Text=""></asp:Label> <%#Eval("anounce")%>
            <asp:Panel ID="IsGlobalAdminLabel" Visible="false" runat="server">
            <a class="faint" href="manageSiteIsGlobalAdminDeleteMessage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;id=<%#Eval("id")%>">Delete</a></asp:Panel>
            
        </li>
    </ItemTemplate>
    <EmptyDataTemplate>
    There is no information
    </EmptyDataTemplate>
</asp:ListView>

 
<ul>
<li>
<div class="center"> 
 <asp:DataPager ID="AnnouncementListPaging" runat="server" PagedControlID="AnnouncementList" 
    QueryStringField="p">                      
        <Fields>
           <asp:NumericPagerField ButtonType="Link" ButtonCount="10" NextPageText="Next" 
                PreviousPageText="Previous" RenderNonBreakingSpacesBetweenControls="true" />
        </Fields>
    </asp:DataPager>
    </div>
</li>
</ul>
</asp:Content>