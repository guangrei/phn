Mobile site creator is a .net 3.5 c# project and it is mobile enabled .
You can host subdomain based mobile sites that manageable via any mobile phone. 


Some of it�s features are:

One of the most advanced mobile web designer
Earn money from adverts with the help of companies like Admob, Mojiva, Buzzcity etc.
HTML allowed to design your site manually
Use style sheets to design your site
Code snippets
Link rotator add-on
Re-publish RSS feeds add-on
Guestbook add-on
Poll and Quiz add-on
Chat add-on
Email to admin add-on
Online visitor list add-on
Show visitors' country and flag add-on
Insert images from Flickr and videos from Youtube to your site
Forums to get help from fellow wapmasters
And many more... 


Setup is very simple:

Open web.config file remove default domains and add your own domains . Rest of the settings pretty self explanatory such as allowed file types for upload and allowed file limit.

You need to setup wild card dns setup on your domain(s) in order to get sub domain system working. 

You also need to setup a web site on iis without host headers if you will use more than one domain to serve sites. If you are using iis7 app needs to be on classic app pool due to limitation of URLRewriter

For caching rss feeds and flicker search results you need to have write access to apps root folder.

App uses 2 sql server databases, one is for content and user info the other is to host binary files. I have used sql server to store binary uploads for easy file management. It doesn't affect the performance as current setup gives only 5 mb space to each user.

I have provided full database script in /AppData/Db scripts folder. If you change name of PHNfileStore.mdf database make sure you alter trigger in users table of PHN.mdf . Trigger there deletes files of user when user removed from database

Once site up and running create to create your global admin account just register a user and set its isGlobalAdmin status to true from your sql database. This is required to manage global settings such as database cleanup,backup,setting up paid user status,checking/removing illegal sites

Please keep in mind that i am beginner to .net and there could be place for many improvements in the code

If you want to see working version go to http://phn.me