﻿using System;
using System.Text;
using System.Net;
using System.IO;
using System.Web;

public class Admoda
{
    public string ShowAd(string service, string publisherId)
    {
        StringBuilder admoda_get = new StringBuilder();
        admoda_get.Append("z=").Append(publisherId);
        admoda_get.Append("&a=").Append(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
        admoda_get.Append("&ua=").Append(HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"]));
        string serviceUrl;
        if (service == "AdmodaBanner")
        {
            serviceUrl = "http://www.admoda.com/ads/fetch.php?" + admoda_get.ToString();
        }
        else if (service == "AdultmodaBanner")
        {
            serviceUrl = "http://www.adultmoda.com/ads/fetch.php?" + admoda_get.ToString();
        }
        else
        {
            serviceUrl = "";
        }


        StringBuilder admoda_contents = new StringBuilder();
        try
        {
            HttpWebRequest request = WebRequest.Create(serviceUrl) as HttpWebRequest;
            request.Method = "GET";
            request.ContentType = "text/html";
            request.Timeout = 1000;
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                admoda_contents.Append(reader.ReadToEnd());
            }
        }
        catch (Exception e)
        {
            //admoda_contents.Append("Mobvision Error!");
            //admoda_contents.Append(e.ToString());
        }

        string ResponseResult = admoda_contents.ToString();
        //return ResponseResult;
        string[] advert = ResponseResult.Split('|');
        string bannerid = advert[0].ToString();

        if (Functions.ReturnNumeric(bannerid) > 0)
        {
            string text = advert[3];
            text = text.Replace("&amp;", "&");
            text = text.Replace("&", "&amp;");
            if (text.Length == 0)
                text = "Click here";

            string image_url = advert[1];
            image_url = image_url.Replace("&amp;", "&");
            image_url = image_url.Replace("&", "&amp;");
            string click_url = advert[2];
            click_url = click_url.Replace("&amp;", "&");
            click_url = click_url.Replace("&", "&amp;");

            string myreturn = string.Empty;
            if (image_url != "")
                myreturn = "<img src=\"" + image_url +"\" alt=\"Advert\" /><br />";

            return myreturn+="<a href=\"" + click_url + "\">" + text + "</a>";
        }
        else
        {
            return string.Empty;
        }





    }
    public string ShowTextAd(string service, string publisherId)
	{
        StringBuilder admoda_get = new StringBuilder();
        admoda_get.Append("z=").Append(publisherId);
        admoda_get.Append("&a=").Append(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
        admoda_get.Append("&ua=").Append(HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"]));
        string serviceUrl;
        if (service == "AdmodaText")
        {
            serviceUrl = "http://www.admoda.com/ads/textfetch.php?" + admoda_get.ToString();
        }
        else if (service == "AdultmodaText")
        {
            serviceUrl = "http://www.adultmoda.com/ads/textfetch.php?" + admoda_get.ToString();
        }
        else
        {
            serviceUrl = "";
        }
        

        StringBuilder admoda_contents = new StringBuilder();
        try
        {
            HttpWebRequest request = WebRequest.Create(serviceUrl) as HttpWebRequest;
            request.Method = "GET";
            request.ContentType = "text/html";
            request.Timeout = 1000;
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                admoda_contents.Append(reader.ReadToEnd());
            }
        }
        catch (Exception e)
        {
            //admoda_contents.Append("Mobvision Error!");
            //admoda_contents.Append(e.ToString());
        }

        string ResponseResult = admoda_contents.ToString();
        //return ResponseResult;
        string [] advert = ResponseResult.Split('|');
        string bannerid = advert[0].ToString();
        

        if (Functions.ReturnNumeric(bannerid) > 0)
        {
            string text = advert[3];
            text = text.Replace( "&amp;", "&" );
            text = text.Replace( "&", "&amp;" );
            string click_url = advert[2];
            click_url= click_url.Replace( "&amp;", "&"  );
            click_url= click_url.Replace( "&", "&amp;" );
            return "<a href=\"" + click_url + "\">" + text + "</a>";
        }
        else
        {
           return string.Empty;
       }





	}

}
