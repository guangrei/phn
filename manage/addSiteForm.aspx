﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/addSiteForm.aspx.cs" Inherits="manage_addSiteForm" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <asp:Panel ID="loggedinDiv" runat="server">
    <div class="title">
    <a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="/images/home.gif" alt=""/> Home</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Add new site
    </div>
           <ul>
           <li>
            <form method="post" id="subdomainFrm" action="addSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">
              <div>
             Please choose a subdomain for your site.
             </div>
                 <table style="width: 100%">
                     <tr>
                         <td style="width: 110px">
            Subdomain:</td>
                         <td>
                             <input id="subdomaininput" name="subdomain" type="text" />
                             <br/>
                             a-z,0-9. Max 50 chars</td>
                     </tr>
                                 <tr>
                         <td style="width: 110px">
            Domain:</td>
                         <td>
                             <%Functions f = new Functions(); f.listAvailableDoaminsFromWEbConfig(); %>
                    </td>
                     </tr>
                     <tr>
                         <td style="width: 110px">
            Adult content:</td>
                         <td>
                             <select name="isR"><option value="1">Yes</option><option value="0">No</option></select></td>
                     </tr>
                     <tr>
                         <td style="width: 110px">
                             &nbsp;</td>
                         <td>
            <input type="submit" value="Add" /></td>
                     </tr>
                 </table>
               </form>
            
            You can create up to <%=SiteConfiguration.GetConfig().subdomainLimitPerUser%> sites. Our service is based on uniqueness of subdomains, not full domain name of your site. Due to that you cannot have same subdomain on more than one domain
            </li></ul>
</asp:Panel>

<asp:Panel Visible="false" ID="notLoggedInDiv" CssClass="center" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
    </asp:Panel>
</asp:Content>

