﻿<%@ Page Title=""  validateRequest="false" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteSnippetsEdit.aspx.cs" Inherits="manage_manageSiteSnippetsEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<form id="From1" runat="server">

 <asp:Panel ID="LoggedInDiv" runat="server">
 <div class="title">
<a href="manageSiteSnippets.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Code snippets</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Edit snippet
</div>
    
 <asp:Panel ID="PanelGetDetails" runat="server">
        <ul><li>
<div>
<asp:HiddenField ID="subdomainholder" runat="server" />
</div>

        Snippet's friendly name:<br/>
    Max 50 chars<br/>
            <asp:TextBox ID="snippetName" runat="server"></asp:TextBox>
            <br/>
        Snippet's code:<br/>
    Html only. Max 1000 chars<br/> 
            <asp:TextBox ID="txtsnippetCode" CssClass="fullscreen" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Update" />
        </li></ul>
        </asp:Panel>
    
           <asp:Panel CssClass="error" ID="Panelallfields" Visible="false" runat="server">
           All fields are required!
       </asp:Panel>
    
       <asp:Panel ID="PanelUpdateDetails" CssClass="center"  Visible="false" runat="server">
       Snippet updated<br/>
       <a href="manageSiteSnippets.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
       </asp:Panel>
       
       <asp:Panel CssClass="error" ID="PanelUpdateDetailsError" Visible="false" runat="server">
              Can't find the snippet!
       </asp:Panel>
    
    
    
    
    
    
    
    </asp:Panel>
    
    
    
    <asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>
</form>
</asp:Content>

