﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/flickrSearch.aspx.cs" Inherits="manage_flickrSearch" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<form id="f" runat="server">
<asp:Panel ID="loggedinDiv" runat="server">
    <div class="title">
    <a href="manageSiteEditPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>">Edit Page<%=Request.QueryString["pageid"] %>.aspx</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Add Flickr image
    </div>

<div class="center">
    <asp:Label CssClass="center" ID="pageCountLabel" runat="server"></asp:Label>
    <asp:Panel ID="resultListPanel" runat="server">
        <asp:ListView ID="ListView1" runat="server">
        <LayoutTemplate>
        <ul>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </ul>
        </LayoutTemplate>
         <ItemTemplate>
         <li>
         <a href="<%#Eval("MediumUrl") %>"><img src="<%#Eval ("SquareThumbnailUrl") %>" alt="..." /></a>
         <br />
         <a href="manageSiteEditPageAddNewItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>&amp;used=flickr&amp;MediumUrl=<%#Eval("MediumUrl") %>&amp;SquareThumbnailUrl=<%#Eval ("SquareThumbnailUrl") %>&amp;prevPositiontagid=<%=Request.QueryString["prevPositiontagid"] %>">Use</a>
                          
        </li>
    </ItemTemplate>
    <EmptyDataTemplate>
    No photos found.
    </EmptyDataTemplate>
    </asp:ListView>
        <asp:Label ID="LabelPagesLabel" runat="server" Visible="false"></asp:Label> <br />
      
              <asp:Button ID="nextButton" runat="server" Text="Next" Visible="false" 
            onclick="nextButton_Click"/> <asp:Button ID="prevButton" runat="server" Text="Previous" Visible="false" onclick="prevButton_Click"/>
           
        <asp:Panel ID="goToPanel" runat="server" Visible="false">
            Go to: <asp:TextBox ID="cPage" runat="server" Visible="false" Columns="3"></asp:TextBox> 
            <asp:Button ID="goToButton" runat="server" Text="Ok" 
            onclick="goToButton_Click" />
            </asp:Panel>
            
    </asp:Panel>
    Search flickr: 
    <asp:TextBox ID="searchTextBox" runat="server"></asp:TextBox> 
    <asp:Button ID="Button1"
        runat="server" Text="Search" onclick="Button1_Click" />
		<br/>
		Only creativecommons.org licensed images searched.
        </div>

</asp:Panel>
<asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>
</form>
</asp:Content>


