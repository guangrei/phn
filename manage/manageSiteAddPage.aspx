﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteAddPage.aspx.cs" Inherits="manage_manageSiteAddPage" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<form id="addpageForm" runat="server">


<asp:ObjectDataSource ID="ListPagesDataSource" runat="server" SelectMethod="ListPages" TypeName="myPageBuilder">
<SelectParameters>
<asp:ControlParameter ControlID="subdomainholder" Name="subdomain" PropertyName="Value" Type="String" />
    <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>



    <div class="title"><a href="managePages.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Edit pages</a> <img src="/images/bluearrow.gif" alt="&gt;"/> 
        Add new page
</div>

<div class="center">
<asp:Label ID="result" runat="server" visible="false" Text=""></asp:Label>
</div>

<asp:Panel ID="loggedinDiv" runat="server">
<script type="text/javascript">
//<![CDATA[


function addTagIn (code)
{

    var inputtoupdate = document.forms[0].<%=this.pageOtherMetaTags.ClientID.ToString() %>;
   
        if (code=="g")
	    {
        inputtoupdate.value= inputtoupdate.value + ' <meta name="verify-v1" content="GOOGLE VERIFICATION CODE HERE" />';
	    }
	    else if (code=="y")
	    {
        inputtoupdate.value= inputtoupdate.value + ' <meta name="y_key" content="YAHOO VERIFICATION CODE HERE" >';
	    }    
}



//]]>
</script>
       <table style="width: 100%">
       
             <tr>
            <td style="width: 110px">
                Put in folder:</td>
            <td>
                <asp:TextBox ID="pageFolder" runat="server" Text="Root"></asp:TextBox>
            </td>
        </tr>
        
      <tr>
            <td style="width: 110px">
                Color themes</td>
            <td>
        <asp:DropDownList ID="TemplateDropDownList" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="TemplateDropDownList_SelectedIndexChanged">
        <asp:ListItem Value="" Text="Not configured"></asp:ListItem>
        <asp:ListItem Value="Standart" Text="Standart"></asp:ListItem>
        <asp:ListItem Value="Black" Text="Black"></asp:ListItem>
        <asp:ListItem Value="Blue" Text="Blue"></asp:ListItem>
        <asp:ListItem Value="Green" Text="Green"></asp:ListItem>
        <asp:ListItem Value="Gray" Text="Gray"></asp:ListItem>
        <asp:ListItem Value="Modern" Text="Modern"></asp:ListItem>
        <asp:ListItem Value="Red" Text="Red"></asp:ListItem>
        </asp:DropDownList> <asp:Button ID="applyTheme" runat="server" Text="Go" 
                    onclick="TemplateDropDownList_SelectedIndexChanged" />
            </td>
        </tr>
        
                <tr>
            <td style="width: 110px">
                <b>&lt;head&gt;</b></td>
            <td>
            </td>
        </tr>
        
        <tr>
            <td style="width: 110px">
                Title:</td>
            <td>
                <asp:TextBox ID="pageTitle" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 110px">
                Keywords:</td>
            <td>
                <asp:TextBox ID="pageKeywords" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 110px">
                Description:</td>
            <td>
                <asp:TextBox ID="pageDescription" runat="server"></asp:TextBox>
            </td>
        </tr>
        
          <tr>
            <td style="width: 110px">
                Other tags:</td>
            <td>
                <asp:TextBox ID="pageOtherMetaTags" runat="server" TextMode="MultiLine" Width="100%" Rows="2"></asp:TextBox>
                <br />Verification tag: <a href="#" onclick="addTagIn('g')">Google</a> <a href="#" onclick="addTagIn('y')">Yahoo</a>
            </td>
        </tr> 
               <tr>
            <td style="width: 110px">
                <b>&lt;/head&gt;</b></td>
            <td>
            </td>
        </tr>
                
                       <tr>
            <td style="width: 110px">
                <b>&lt;body&gt;</b></td>
            <td>
            </td>
        </tr>
        
        
         <tr>
            <td style="width: 110px">
                Bg color:</td>
            <td>
            <asp:ObjectDataSource ID="BackgorundDataSource" runat="server" 
            SelectMethod="colourList" TypeName="myPageBuilder"></asp:ObjectDataSource>
            <asp:DropDownList ID="pageBgColor" runat="server" 
            AppendDataBoundItems="true"
                    DataSourceID="BackgorundDataSource" DataTextField="ColorName" 
                    DataValueField="ColorHex" OnPreRender="BackgorundDataSource_setStyle">
                    <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
                 </asp:DropDownList>
            </td>
        </tr>
    
    <tr>
            <td style="width: 110px">
                Font color:
                </td>
        
            <td>
            <asp:DropDownList ID="pageFontColor" runat="server" 
            AppendDataBoundItems="true"
                    DataSourceID="BackgorundDataSource" DataTextField="ColorName" 
                    DataValueField="ColorHex" OnPreRender="BackgorundDataSource_setStyleFont">
                    <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
                 </asp:DropDownList>
            </td>
        </tr>
        

         
         <tr>
   
            <td style="width: 110px">
                Font size :</td>
            <td>
                <asp:DropDownList ID="fontSize" runat="server">
<asp:ListItem Value="" Text="Not configured"></asp:ListItem>
<asp:ListItem Value="xx-small" Text="Xx-small"></asp:ListItem>
<asp:ListItem Value="x-small" Text="X-small"></asp:ListItem>
<asp:ListItem Value="small" Text="Small"></asp:ListItem>
<asp:ListItem Value="medium" Text="Medium"></asp:ListItem>
<asp:ListItem Value="large" Text="Large"></asp:ListItem>
<asp:ListItem Value="x-large" Text="X-large"></asp:ListItem>
<asp:ListItem Value="xx-large" Text="Xx-large"></asp:ListItem>
     </asp:DropDownList>
     
            </td>
        </tr>
    
    
    
        
          
        
        <tr>
            <td style="width: 110px">
                &nbsp;</td>
            <td>
                <asp:HiddenField ID="subdomainholder" runat="server" />
                Other meta tags max 500, rest max 100 chars<br/>
                <asp:Button ID="Button1" runat="server" Text="Add" onclick="Button1_Click" /> <a href="managePages.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">
                Cancel</a>
                
            </td>
        </tr>
        
        
    </table>
    
    
<div class="title">Copy from anotherpage</div>
<ul><li>
    You can also create a new page by copying other page. Select the page you want 
    to copy from the list and then click Copy
<br />
<asp:DropDownList ID="copyPageDropDownList" runat="server" 
        DataSourceID="ListPagesDataSource" DataTextField="title" 
        DataValueField="id"></asp:DropDownList><br />
    <asp:Button ID="copyButton" runat="server" Text="Copy" 
        onclick="copyButton_Click" /></li></ul>
    
    
                You can add up to <%=SiteConfiguration.GetConfig().pageLimitPerSubdomain%> 
       page per site.
 </asp:Panel>
 
   <asp:Panel Visible="false" ID="okLink" runat="server" CssClass="center">
    <a href="managePages.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">
       Ok</a>
    </asp:Panel>

  
  <asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>


 </form>
</asp:Content>