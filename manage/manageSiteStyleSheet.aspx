﻿<%@ Page validateRequest="false" Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteStyleSheet.aspx.cs" Inherits="manage_manageSiteStyleSheet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <form id="form1" runat="server">
    
    <asp:Panel ID="loggedinDiv" runat="server">
    <div class="title"><a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>"><asp:Label ID="fullSiteNameLabel" runat="server"></asp:Label></a> <img src="/images/bluearrow.gif" alt="&gt;"/> Style sheet
</div>

    <div class="error">Make sure you know what you are doing! If you have no idea what it is a style sheet, please use our designer to set style of your page.</div>
    
    <b>&lt;style type=&quot;text/css&quot;&gt;</b>     
    <asp:TextBox ID="styleSheetBox" runat="server" CssClass="fullscreen" Rows="10" TextMode="MultiLine"></asp:TextBox>
    <br/>
    <b>&lt;/style&gt;</b>
     <br/>
   Max 3000 chars. Only Css tags allowed<br/>
     
    <asp:HiddenField ID="subdomainName" runat="server" />
    <asp:Button ID="updateButton" runat="server" Text="Update" 
            onclick="updateButton_Click" /> <a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Finished</a>
       </asp:Panel>
   

    <asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
    </asp:Panel>
    
    </form>

</asp:Content>
