<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteReadMessages.aspx.cs" Inherits="manage_manageSiteReadMessages" ValidateRequest="false" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<form id="form1" runat="server">
<asp:Panel ID="loggedinDiv" runat="server">
<div class="title"><a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="/images/home.gif" alt=""/> Home</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Announcements
</div>
    <asp:Panel ID="addResult" Visible="false" runat="server">
        <asp:Label ID="addResultTwo" runat="server" Text=""></asp:Label>
        <br/><a href="manageSiteReadMessages.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Ok</a>
    </asp:Panel>
    
<asp:ListView ID="AnnouncementList" runat="server" 
        OnItemDataBound="AnnouncementListEventHandler" 
        onpagepropertieschanging="AnnouncementList_PagePropertiesChanging">
<LayoutTemplate>
    <ul>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
        <b> <asp:Label ID="msgDate" runat="server" Text=""></asp:Label></b>
            <asp:Label ID="isPublic" runat="server" Text=""></asp:Label> <%#Eval("anounce")%>
            <asp:Panel ID="IsGlobalAdminLabel" Visible="false" runat="server">
            <a class="faint" onclick="return confirm('DELETE this message?')" href="manageSiteIsGlobalAdminDeleteMessage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;id=<%#Eval("id")%>">Delete</a></asp:Panel>
            
        </li>
    </ItemTemplate>
    <EmptyDataTemplate>
    There is no information
    </EmptyDataTemplate>
</asp:ListView>

 
<ul>
<li>
<div class="center"> 
 <asp:DataPager ID="AnnouncementListPaging" runat="server" PagedControlID="AnnouncementList" 
    QueryStringField="p">                      
        <Fields>
           <asp:NumericPagerField ButtonType="Link" ButtonCount="10" NextPageText="Next" 
                PreviousPageText="Previous" RenderNonBreakingSpacesBetweenControls="true" />
        </Fields>
    </asp:DataPager>
    </div>
</li>
</ul>

<%if (Session["IsGlobalAdmin"].ToString() == "yes")
  {%>
 <asp:Panel ID="addPanel1" runat="server">
<div class="title">Add announce</div>
    <b>Public announcement:</b> <asp:radiobuttonlist id="__isPublic" runat="server" 
        RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" 
        onselectedindexchanged="radioChanged" >
    <asp:listitem id="isPublicTrue" runat="server" value="Yes" Selected="True" />
    <asp:listitem id="isPublicFalse" runat="server" value="No" />
    </asp:radiobuttonlist>
    <br/>
        <asp:Panel ID="tounPanel" Visible="false" runat="server">
    <b>To un:</b> <asp:TextBox ID="_toun" runat="server" ></asp:TextBox>
    <br />
    </asp:Panel>
    <b>Announcement:</b>
    <asp:TextBox ID="_announce" runat="server" Rows="5" TextMode="MultiLine" CssClass="fullscreen"></asp:TextBox>

    <asp:Button ID="post" runat="server" Text="Post" onclick="post_Click" />
</asp:Panel>
<%} %>  

</asp:Panel>

<asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>
</form>
</asp:Content>

