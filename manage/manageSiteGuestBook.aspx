﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteGuestBook.aspx.cs" Inherits="manageSiteGuestBook" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">

<form id="form1" runat="server">
<asp:Panel ID="LoggedInDiv" runat="server">
<div class="title">
<a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>"><asp:Label ID="fullSiteNameLabel" runat="server"></asp:Label></a> <img src="/images/bluearrow.gif" alt="&gt;"/> 
    Manage guestbook
</div>
<%
    string job = Request.QueryString["job"];
    if (job=="delete")
        {
            Functions f = new Functions();
            int id = Functions.ReturnNumeric(Request["subdomainid"]);
            int postid = Functions.ReturnNumeric(Request["id"]);
            string subdomain = f.GetSubdomainNameById(id);
            if (
            f.DeleteAGbPost(Functions.un, subdomain, postid) == true)
            {%>
            <div class="center">Message has been deleted<br/>
            <a href="manageSiteGuestBook.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">
                Ok</a>
            </div>
            <%}
            else
            {%><div class="error">Unable to delete the message<br/>
                        <a href="manageSiteGuestBook.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">
        Ok</a>
		</div><%}}else{%>
		
		
    <asp:Literal ID="dumbGb" runat="server"></asp:Literal>


<%}%>
        


</asp:Panel>



    <asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>

    </form>

</asp:Content>

