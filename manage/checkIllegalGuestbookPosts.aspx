﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/checkIllegalGuestbookPosts.aspx.cs" Inherits="manage_checkIllegalGuestbookPosts" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">

    <form id="form1" runat="server">

<asp:Panel ID="loggedinDiv" runat="server">
<div class="title"><a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="/images/home.gif" alt=""/> 
    Home</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Check illegal guestbooks
</div>

    <asp:Panel ID="resultpanel" runat="server" CssClass="center" Visible="false">
        <a href="?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Post deleted!</a>
    </asp:Panel>

    <asp:TextBox ID="keyword" runat="server"></asp:TextBox> <asp:Button ID="search" 
        runat="server" Text="Search" />
    <br />


    
       <asp:ObjectDataSource ID="getIllegalSitesGB" runat="server" 
        SelectMethod="ListIllegalGuestBooks" TypeName="Functions">
        <SelectParameters>
            <asp:ControlParameter ControlID="keyword" DefaultValue="child" Name="keyword" 
                PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:GridView  CssClass="inftbl" ID="ListOfSitesGrid" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" DataSourceID="getIllegalSitesGB" 
        >     
        <Columns>
        
            <asp:TemplateField HeaderText="Username">
            <HeaderStyle CssClass="title" />
            <ItemStyle CssClass="cell" />
            <ItemTemplate><a href="mailto:<%# Eval("email") %>"><%# Eval("un") %></a></ItemTemplate>
        </asp:TemplateField>
        
              
            <asp:TemplateField HeaderText="Last logon">
            <HeaderStyle CssClass="title" />
            <ItemStyle CssClass="cell" />
            <ItemTemplate><%# String.Format("{0:dd MMM yyyy}",Eval("LastLogon")) %></ItemTemplate>
        </asp:TemplateField>


        
                    <asp:TemplateField HeaderText="Post">
            <HeaderStyle CssClass="title" />
            <ItemStyle CssClass="cell" />
            <ItemTemplate>
            <a href="http://<%# Eval("subdomain").ToString().ToLower() %>.<%# Eval("domain") %>">
                http://<%# Eval("subdomain").ToString().ToLower() %>.<%# Eval("domain") %></a><br />
                
                <b><%# Eval("poster") %> :</b> <br /> <%# Eval("post") %>
            </ItemTemplate>
        </asp:TemplateField>
        
                    <asp:TemplateField HeaderText="Action">
            <HeaderStyle CssClass="title" />
            <ItemStyle CssClass="cell" />
            <ItemTemplate>
                <a onclick="return confirm('Delete this post?')" href="?cmd=delete&amp;postId=<%# Eval("id") %>&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Delete</a> | <a href="manageSiteReadMessages.aspx?fromcheck=2&amp;toun=<%# Eval("un") %>&amp;postId=<%# Eval("id") %>&amp;poster=<%# Eval("poster") %>&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Warn</a>
            </ItemTemplate>
        </asp:TemplateField>
        
        </Columns>
        
        <pagerstyle  CssClass="cell"
          horizontalalign="Center"/>
        <PagerTemplate>
            Page <%= ListOfSitesGrid.PageIndex+1 %> of <%=ListOfSitesGrid.PageCount%><br />
           <asp:Button ID="Button1" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev"  Text="Prev"/>
            <asp:Button ID="Button2" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" Text="Next"/>
        </PagerTemplate>
    </asp:GridView>
 
    
 
 
    
 </asp:Panel>   
    
    
    
    
    <asp:Panel Visible="false" CssClass="error" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
    </asp:Panel>

    </form>

</asp:Content>