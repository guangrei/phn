﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteEditPage.aspx.cs" Inherits="manage_manageSiteEditPage" %>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="System.Configuration"%>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
  
<asp:Panel ID="loggedinDiv" runat="server">

    <div class="title">
    <a href="managePages.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Edit pages</a> <img src="/images/bluearrow.gif" alt="&gt;"/> <asp:Label ID="editPageLabel" runat="server" Text=""></asp:Label>
    </div>
       
<div>
<ul>
<li>
<%=listPages()%>
</li>
</ul>


<b>&lt;body&gt;</b>
<br/>
<b>&lt;div class="wrapper"&gt;</b>
<div class="gap"></div>
<div class="center">
<a class="bold faint" href="manageSiteEditPageAddNewItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>&amp;pageid=<%=Request.QueryString["pageid"]%>&amp;prevPositiontagid=0"><img src="../images/add.png" alt="+"/> Add new item</a>
</div>

<form id="dele" method="post" action="manageSiteEditPageDeleteItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>&amp;pageid=<%=Request.QueryString["pageid"]%>&amp;delcol=1"><div>
<ul>

<%
    string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
    Functions f = new Functions();
    int subdomainid = Functions.ReturnNumeric(Request["subdomainid"]);
    string subdomain = f.GetSubdomainNameById(subdomainid);
    int pageid = Functions.ReturnNumeric(Request["pageid"]);

    myPageBuilder build = new myPageBuilder();
    DataSet ds = build.BuildPage(subdomain, pageid);
    int itemCountInpage = Functions.ReturnNumeric(ds.Tables[2].Rows.Count.ToString());
    if (itemCountInpage>0)
    {


        //Page build

        
foreach (DataRow dataRow in ds.Tables[2].Rows)
{
        %>
        <li>
        <input name="DeleteCheckbox" value="<%=dataRow["id"].ToString()%>" type="checkbox" /> 
        <span class="bold"><%=dataRow["tagType"].ToString()%></span> 
        <%if (dataRow["tagType"].ToString() == "Text" || dataRow["tagType"].ToString() == "bText")
          {
              Response.Write(Server.HtmlEncode(myPageBuilder.Truncate(dataRow["tagText"].ToString(), 10)));
               %><a class="bold faint" href="manageSiteEditPageEditItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=subdomainid%>&amp;pageid=<%=dataRow["pageId"].ToString() %>&amp;tagid=<%=dataRow["id"].ToString() %>"><img src="../images/edit.gif" alt="e"/></a>
                <%
                 }
         if (dataRow["tagType"].ToString() == "HtmlCode")
          {
              Response.Write(myPageBuilder.Truncate(dataRow["tagText"].ToString(), 10));
               %><a class="bold faint" href="manageSiteEditPageEditItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=subdomainid%>&amp;pageid=<%=dataRow["pageId"].ToString() %>&amp;tagid=<%=dataRow["id"].ToString() %>"><img src="../images/edit.gif" alt="e"/></a>
                <% 
                }
         if (dataRow["tagType"].ToString() == "iTextLink" || dataRow["tagType"].ToString() == "TextLink")
          {
              Response.Write(myPageBuilder.Truncate(dataRow["tagLinkTitle"].ToString(), 10));
          }
         if (dataRow["tagType"].ToString() == "Image")
          {
              Response.Write("<a href=\"" + dataRow["tagLinkUrl"].ToString() + "\">Image</a>");
          }
         if (dataRow["tagType"].ToString() == "ImageLink")
          {
              Response.Write("<img src=\"" + dataRow["tagLinkTitle"].ToString() + "\" alt=\"\"/> " + Server.HtmlEncode(myPageBuilder.Truncate(dataRow["tagLinkUrl"].ToString(), 10)));
          }
         if (dataRow["tagType"].ToString() == "CodeSnippet")
          {
              Response.Write(Functions.ReturnCodeSnippetName(Functions.un, subdomain, Functions.ReturnNumeric(dataRow["tagRelatedId"].ToString())));
          }
         if (dataRow["tagType"].ToString() == "RssFeed")
          {
              Response.Write(Functions.ReturnFeedName(Functions.un, subdomain, Functions.ReturnNumeric(dataRow["tagRelatedId"].ToString())) + " (" + dataRow["tagText"].ToString() + ")");
          }
         if (dataRow["tagType"].ToString() == "Advert")
          {
              Response.Write(Functions.ReturnAdvertName(Functions.un, subdomain, Functions.ReturnNumeric(dataRow["tagRelatedId"].ToString()))); ;
          }
         if (dataRow["tagType"].ToString() == "File")
          {
              Response.Write(myPageBuilder.Truncate(dataRow["tagLinkTitle"].ToString(), 10) + " as " + dataRow["tagText"].ToString());
          }
         if (dataRow["tagType"].ToString() == "ContactAdminForm")
          {
              Response.Write("Returns to " + dataRow["tagLinkUrl"].ToString());
          }
         if (dataRow["tagType"].ToString() == "Poll")
         {
             Response.Write(Server.HtmlEncode(myPageBuilder.Truncate(f.GetPollTitleById(Convert.ToInt32(dataRow["tagRelatedId"].ToString())), 10)));
         }
         %>
        <a href="manageSiteEditPageAddNewItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=subdomainid%>&amp;pageid=<%=dataRow["pageId"].ToString() %>&amp;prevPositiontagid=<%=dataRow["positionInPage"].ToString()%>"><img src="../images/add.png" alt="+"/></a> 
        <a onclick="return confirm('Delete this item?');" class="bold faint" href="manageSiteEditPageDeleteItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=subdomainid%>&amp;pageid=<%=dataRow["pageId"].ToString() %>&amp;tagid=<%=dataRow["id"].ToString() %>"><img src="../images/delete.png" alt="-"/></a>
        <a href="manageSiteEditPageMoveItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=subdomainid%>&amp;pageid=<%=dataRow["pageId"].ToString() %>&amp;tagid=<%=dataRow["id"].ToString() %>&amp;cp=<%=dataRow["positionInPage"].ToString()%>&amp;way=up"><img src="../images/arrow_up.gif" alt="up"/></a> 
        <a href="manageSiteEditPageMoveItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=subdomainid%>&amp;pageid=<%=dataRow["pageId"].ToString() %>&amp;tagid=<%=dataRow["id"].ToString() %>&amp;cp=<%=dataRow["positionInPage"].ToString()%>&amp;way=down"><img src="../images/arrow_down.gif" alt="down"/></a> 
        </li><%  
    }
        
    %><li><input onclick="return confirm('Delete selected item(s)?');" id="Submit1" type="submit" value="Delete selected items" /></li><%  
        // end build
        



    }
    else
    {
        Response.Write("<li>There are no items. Click Add new item to start building your page</li>");
    }
%>

 </ul>
    
</div></form>
      
<div class="gap"></div>
<b>&lt;/div&gt;</b>
<br/>
<b>&lt;/body&gt;</b>
</div>





</asp:Panel>








<asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>

</asp:Content>

