﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteDeleteInactiveSites.aspx.cs" Inherits="manage_manageSiteDeleteInactiveSites" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">

<asp:Panel ID="loggedinDiv" runat="server">
<div class="title"><a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="/images/home.gif" alt=""/> Home</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Delete unverified users
</div>
    <asp:Panel ID="addResult" CssClass="center" runat="server">
        <asp:Label ID="addResultTwo" runat="server" Text=""></asp:Label>
        <br/><a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Ok</a>
    </asp:Panel>
 </asp:Panel>   
    
    <asp:Panel Visible="false" CssClass="error" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>

</asp:Content>
