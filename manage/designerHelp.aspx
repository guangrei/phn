﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" Title="Untitled Page" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<div>
    <div class="title">
    <a href="manageSiteEditPageAddNewItem.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>">
        Edit Page<%=Request.QueryString["pageid"] %>.aspx</a> <img src="/images/bluearrow.gif" alt="&gt;"/> 
        Help
    </div>
<ul>
<li>
<b>How can i align an item to center of the page :</b> Insert <b>Div open</b> item 
    and set it's <b>Alignment</b> to center; then insert your item that you want to 
    align to center; finally insert <b>Div close</b> item. Same think can be done 
    with <b>Paragraph open/close</b> item too.
</li>

<li>
<b>How can i add link to other pages :</b> Use <b>PHN: Link</b> item to ad link to other pages in your site.
</li>

<li>
    <b>What is the difference between Block and Inline text :</b> <b>Block text</b> 
    is a text that covers all of the line and any other item you add will be on the next line. We 
    achive this by putting it in div tag.<b> Inline text</b> doesn't cover all of 
    the line. So you can for example, have a image next to Inline tex and both will be on same line. We achive this by using span tag.</li>
</ul>


</div>
</asp:Content>

