﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"%>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">

<div class="title"><a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="/images/home.gif" alt=""/> Home</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Account limits</div>

<ul>
<li><b>You have following limits on your account on per page basis</b></li>
<li>
<b>Items:</b> <%=SiteConfiguration.GetConfig().TotalItemsLimitPerPage %>
</li>
<li>
<b>Text/html/snippet/rotator:</b> <%=SiteConfiguration.GetConfig().TextInputLimitPerPage %>
</li>
<li>
<b>Adverts:</b> <%=SiteConfiguration.GetConfig().AdvertiserLimitPerPage %>
</li>
<li>
<b>Online count/list:</b> <%=SiteConfiguration.GetConfig().OnlyOneItemPerPage%>
</li>
<li>
<b>Total hits:</b> <%=SiteConfiguration.GetConfig().OnlyOneItemPerPage%>
</li>
<li>
<b>Sitemap:</b> <%=SiteConfiguration.GetConfig().SiteMapLimitPerPage%>
</li>
<li>
<b>RSS feed:</b> <%=SiteConfiguration.GetConfig().RssFeedLimitPerPage%>
</li>
<li>
<b>Contact admin form:</b> <%=SiteConfiguration.GetConfig().OnlyOneItemPerPage%>
</li>
<li>
<b>Poll:</b> <%=SiteConfiguration.GetConfig().OnlyOneItemPerPage%>
</li>
<li>
<b>Chat:</b> <%=SiteConfiguration.GetConfig().OnlyOneItemPerPage%>
</li>
<li>
<b>Any other item:</b> <%=SiteConfiguration.GetConfig().DefaultItemLimitPerPage %>
</li>
</ul>
    
    
    
</asp:Content>
