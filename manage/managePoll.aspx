﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/managePoll.aspx.cs" Inherits="manage_managePoll" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <form id="f1" runat="server">
<asp:Panel ID="loggedinDiv" runat="server">
<asp:HiddenField ID="subdomainName" runat="server" />
    <div class="title">
    <a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>"><asp:Label ID="fullSiteNameLabel" runat="server"></asp:Label></a> <img src="/images/bluearrow.gif" alt="&gt;"/> 
        Manage polls
    </div>
    
<asp:Panel ID="ListHolder" runat="server">
    
        <asp:ListView ID="pollList" runat="server" 
        DataSourceID="listPollsDatasource">
        <LayoutTemplate>
        <ul>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </ul>
        </LayoutTemplate>
         <ItemTemplate>
         <li>
         <b><%# Eval("pollTitle") %> <a onclick="return confirm('DELETE this poll? It will be deleted from all pages too!')" href="managePoll.aspx?job=delete&amp;id=<%# Eval("id") %>&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>"><img
    src="../images/delete.png" alt="del"/></a> <a onclick="return confirm('RESET this poll? All votes for this poll will be cleared!')" href="managePoll.aspx?job=reset&amp;id=<%# Eval("id") %>&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>"><img
    src="../images/reset_poll.gif" alt="reset"/></a></b>
    <br />
    <%# Functions.GetPollChoices(Convert.ToInt32(Eval("id")))%>
        </li>
    </ItemTemplate>
    <EmptyDataTemplate>
        There are no polls.
    </EmptyDataTemplate>
    </asp:ListView>
   
    
    <asp:ObjectDataSource ID="listPollsDatasource" runat="server" 
        SelectMethod="ListPolls" TypeName="Functions">
        <SelectParameters>
            <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
            <asp:ControlParameter ControlID="subdomainName" Name="subdomain" 
                PropertyName="Value" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <div class="title">Add new poll</div>
    <ul>
    <li>
        Poll question: <asp:TextBox ID="PollQuestionText" runat="server"></asp:TextBox>
        <asp:Literal ID="ChoiceC" runat="server"><br />Choice count: </asp:Literal> 
        <asp:DropDownList ID="ChoicecountDropDownList" runat="server">
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
            <asp:ListItem>7</asp:ListItem>
            <asp:ListItem>8</asp:ListItem>
            <asp:ListItem>9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
        </asp:DropDownList>
    <br />
        <asp:PlaceHolder ID="ChoicesPlaceHolder" runat="server"></asp:PlaceHolder>
    
    <asp:Button ID="addChoiceButton" runat="server" Text="Add choices" 
            onclick="addChoiceButton_Click" />
    <asp:Button ID="SaveButton" runat="server" Text="Save" onclick="SaveButton_Click" /> <asp:HyperLink ID="HyperLinkReload" Visible="false" runat="server">Reset</asp:HyperLink>
    </li>
       
     <li>
    Did you know that you can create a quiz site with poll add-on? You can create a poll and page for each question and than link those pages together. Finally you can have a result page where you publish correct answers!
    </li> 
    <li>
        You can create up to <%=SiteConfiguration.GetConfig().pollLimitPerSite%> polls per site. Poll edit is not available.
    </li>
    </ul>
 </asp:Panel>
    
    
    
    <asp:Panel ID="resultPanel" visible="false" CssClass="center" runat="server">
        <asp:Literal ID="ResultLiteral" runat="server"></asp:Literal>
        <a href="managePoll.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Ok</a></asp:Panel>
    
     
 </asp:Panel>
 
 
 
 
 
 
 
 <asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>
</form>
</asp:Content>

