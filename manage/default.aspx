﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"%>
<%@ Register src="controls/logon.ascx" tagname="logonForm" tagprefix="nLL" %>
<asp:Content ID="pageBody" ContentPlaceHolderID="body" Runat="Server">

<% Functions f = new Functions();if (f.LoginCheck() == true){// login success----------------------------------%>
<%//////////////////////////////PAGE CONTENT////////////////////////////////////////////////////////////////// %>
<%    
 
    string _showDeleteMode = Request.QueryString["view"];
    int showDeleteMode;
    if (_showDeleteMode == "deletemode") { showDeleteMode = 1; } else { showDeleteMode = 0; }  
       
 %>
 
 
Welcome to your control panel. If you need help, check our Anouncements and Support forum below.




<%if (showDeleteMode == 1)
  { %>
<div class="error">You are on DELETE MODE. If you click 
    <img src="../images/delete.png" alt="del"/> icon, site next to it together with all its pages will be PERMANENTLY DELETED!</div>
<%} %>  
 
<div class="title"><img src="../images/mysites.gif" alt=""/> My sites</div>
<% 
    f.listUsersSubdomains(Functions.un,showDeleteMode); 
       
 %>
 <ul><li><a href="addSiteForm.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Add new site</a></li></ul>


 <img src="../images/flag_green.gif" alt="*"/> online <img src="../images/flag_red.gif" alt="-"/> offline <img src="../images/adult.png" alt="R"/> adult


<div class="title"><img src="../images/filestore.gif" alt=""/> My File store</div>
 <ul><li><img src="/images/bluearrow.gif" alt="&gt;"/> <a href="uploadFile.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Upload &amp; Manage files</a></li></ul>


<%= f.getAnnouncements(Functions.un) %>


<div class="title"><img src="../images/help.png" alt=""/> Help &amp; support</div>
<ul><li><a href="/forums.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="/images/forum.gif" alt=""/> Support forum</a></li></ul>



<div class="title"><img src="../images/account.gif" alt=""/> My account</div>
<ul>
<li>
<% if (Request.QueryString["view"]=="cpw") { %>
        <div class="bold">Change password</div>
        <a id="cpw"></a>
            <form id="changepwFrm" method="post" action="changePassword.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">
             <p> 
            New password:<br /><input id="newpwinput" name="newpw" type="text" />
            <br/>
            <input type="submit" value="Change" /> <a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Cancel</a>
            </p>
        </form>
<%}else{ %>
<a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;view=cpw#cpw"><img src="../images/changepw.gif" alt=""/> Change password</a>
<%} %>
</li>

<li>
<% if (Request.QueryString["view"]=="cem") { %>
        <div class="bold">Change e-mail</div>
        <a id="cem"></a>
        <div class="error">Please note that you will have to re-verify your account and you will not be able to login untill you finish re-verification. So make sure you are updating to a valid e-mail address.</div>
            <form id="emailcFrm" method="post" action="changeEmail.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">
             <p> 
            New e-mail:<br /><input id="newemailinput" name="newemail" type="text" />
            <br/>
            
            <input type="submit" value="Change" /> <a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Cancel</a>
            </p>
        </form>
 <%}else{ %>
<a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;view=cem#cem"><img src="../images/email.gif" alt=""/> Change e-mail</a>
<%} %>
</li>
<li>
<img src="../images/limits.gif" alt=""/> <a href="limits.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Account limits</a>
</li>

<li>
<img src="../images/terms.gif" alt=""/> <a href="../terms.htm">Service terms</a>
</li>
<li>
<%if (showDeleteMode == 1)
  { %>
<a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="../images/normal.gif" alt=""/> Switch to normal mode</a>
<%}
  else
  {%>
<img src="../images/delete.png" alt=""/> <a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;view=deletemode">Switch to Delete mode</a>
<%} %>
</li>
<li>
<img src="../images/logoff.gif" alt=""/> <a href="logout.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Logout</a>
</li>
</ul>

<%if (Session["IsGlobalAdmin"].ToString() == "yes")
  {%>
 <div class="title"><img src="../images/icon_sticky.gif" alt=""/> GlobalAdmin</div>
<ul>
  <li>
<a class="bold" href="manageSiteReadMessages.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Add announcement</a>
</li>
<li>
<a class="bold" href="checkIllegalSites.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Check illegal sites</a>
</li>
<li>
<a class="bold" href="checkIllegalGuestbookPosts.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Check illegal guestbooks</a>
</li>
  <li>
<a class="bold" href="manageSiteDeleteUnverified.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Delete unverified users</a>
</li>
  <li>
<a class="bold" href="manageSiteDeleteGBposts.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Delete old guestbook posts</a>
</li>
  <li>
<a class="bold" href="manageSiteDeleteInactiveSites.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Delete inactive sites</a>
</li>
  <li>
<a class="bold" href="manageSiteDeleteInactiveUsers.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Delete inactive users</a>
</li>
  <li>
<a class="bold" href="managePaidUsers.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Manage paid users</a>
</li>
  <li>
<a class="bold" href="manageSiteBackupDb.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Backup Db(s)</a>
</li>
</ul>
<%}%>  




<%//////////////////////////////PAGE CONTENT////////////////////////////////////////////////////////////////// %>
<%}else{%><nLL:logonForm ID="LogonForm1" runat="server" /><%}%>      
</asp:Content>