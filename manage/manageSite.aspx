﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSite.aspx.cs" Inherits="manage_manageSite" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<form id="form1" runat="server">
<asp:Panel ID="loggedinDiv" runat="server">
<div class="title"><a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="/images/home.gif" alt=""/> Home</a> <img src="/images/bluearrow.gif" alt="&gt;"/> <asp:Label ID="fullSiteNameLabel" runat="server"></asp:Label>
</div>
<asp:ListView ID="subDomainInfoList" runat="server" OnItemDataBound="subDomainInfoListEventHandler">
<LayoutTemplate>
    <ul>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </ul>
    </LayoutTemplate>
    <ItemTemplate>
    <li>This is your site control panel. Please navigate down in the page to create and edit pages or add/remove features to your site</li>
    
    <li>
    <table class="inftbl" id="inf">
	<tr>
		<td class="cell"><a href="manageSiteChangeStatus.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>" onclick="return confirm('Change site status?')"><asp:Label ID="isActiveLabel" runat="server" Text=""></asp:Label></a></td>
    	<td class="cell"><a href="manageSiteChangeRating.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>" onclick="return confirm('Change site rating?')"><asp:Label ID="isR" runat="server" Text=""></asp:Label></a></td>
		<td class="cell"><%#Eval("totalpages")%> pages</td>
		<td class="cell"><%#Eval("totalhits")%> hits</td>
	</tr>
    </table>
    </li>

     <li>
    <a href="managePages.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>"><img src="/images/edit.gif" alt=""/> Edit pages</a>
    </li>
    <li>
       <a href="manageSiteStyleSheet.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>"><img src="/images/css.gif" alt=""/> Style sheet</a>
    </li>


    <li>
    <a href="manageSiteSnippets.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>"><img src="/images/newsnippet.gif" alt=""/> Code snippets</a>
    </li>
    
    <li>
    <a href="manageSiteLinks.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>"><img src="/images/rotator.gif" alt=""/> Link rotator</a>
    </li>
    
    <li>
    <a href="manageSiteRSS.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>"><img src="/images/rss.png" alt=""/> Manage RSS feeds</a>
    </li>
    
    <li>
    <a href="manageSiteAdverts.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>"><img src="/images/money.png" alt=""/> Manage adverts</a>
    </li>
    
    <li>
    <a href="manageSiteGuestBook.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>"><img src="/images/bookmanage.png" alt=""/> Manage guestbook</a>
    </li>
    
    <li>
    <a href="managePoll.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%#Eval("subdomainid")%>"><img src="/images/poll.gif" alt=""/> Manage polls</a>
    </li>
    
    <li>
       <b>SEO Sitemap: /sitemap.aspx</b>
    </li>  
    
    </ItemTemplate>
    <EmptyDataTemplate>
    There is no information!
    </EmptyDataTemplate>
</asp:ListView>

</asp:Panel>
<asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>
</form>
</asp:Content>

