﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"%>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
         //hide footer
        ((HtmlControl)this.Master.FindControl("footer")).Visible = false;
  
    
        Functions f = new Functions();
        if (f.LoginCheckOnLogin() == true)
        {
            if (f.IsGlobalAdmin(Functions.un) == true)
            {
                Session["IsGlobalAdmin"] = "yes";
            }
            loginResult.Text = "Login successful!";

            string sessionid = f.GetUniqueKey(25);

            f.InsertSession(Functions.un, sessionid);
            ContinueHyperLink.NavigateUrl = "default.aspx?un="+Functions.un+"&pw="+sessionid;
        }
        else
        {
            loginResult.CssClass = "error";
            loginResult.Text = "Check your username and password!";
            ContinueHyperLink.NavigateUrl = "default.aspx";
        }

    }
 </script>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">


    <div class="center">
    <asp:Label ID="loginResult" runat="server" Text=""></asp:Label>
    <br/>
        <asp:HyperLink ID="ContinueHyperLink" runat="server">Continue</asp:HyperLink> 
    </div>
    
    
    
</asp:Content>

